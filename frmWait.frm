VERSION 5.00
Begin VB.Form frmWait 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Checking for Available Datasources"
   ClientHeight    =   705
   ClientLeft      =   2760
   ClientTop       =   3750
   ClientWidth     =   4425
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   705
   ScaleWidth      =   4425
   ShowInTaskbar   =   0   'False
   Begin VB.Timer Timer1 
      Interval        =   2000
      Left            =   3840
      Top             =   120
   End
   Begin VB.Label Label1 
      Caption         =   "Checking"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   120
      TabIndex        =   0
      Top             =   130
      Width           =   4215
   End
End
Attribute VB_Name = "frmWait"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Private dotCount As Integer

Private Sub Timer1_Timer()
    Dim str1 As String, str2 As String, x As Integer
    
    DoEvents

    str1 = "Checking"
    
    For x = 0 To dotCount
        str2 = str2 + "."
    Next x
    
    If dotCount < 20 Then
        dotCount = dotCount + 1
    Else
        dotCount = 1
    End If
    
    Label1.Caption = str1 & str2
    DoEvents
    
End Sub

