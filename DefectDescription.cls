VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "DefectDescription"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Member0" ,"DefectDescription"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Private mvarDefectTypeID As Integer 'local copy
Private mvarDefectDescriptionID As Integer 'local copy
Private mvarDefectDescription As String 'local copy
Private mvarDefectInfo As String 'local copy

Public Property Let DefectInfo(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.DefectInfo = 5
    mvarDefectInfo = vData
End Property

Public Property Get DefectInfo() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.DefectInfo
    DefectInfo = mvarDefectInfo
End Property

Public Property Let DefectDescription(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.DefectDescription = 5
    mvarDefectDescription = vData
End Property

Public Property Get DefectDescription() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.DefectDescription
    DefectDescription = mvarDefectDescription
End Property

Public Property Let DefectDescriptionID(ByVal vData As Integer)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.DefectDescriptionID = 5
    mvarDefectDescriptionID = vData
End Property

Public Property Get DefectDescriptionID() As Integer
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.DefectDescriptionID
    DefectDescriptionID = mvarDefectDescriptionID
End Property

Public Property Let DefectTypeID(ByVal vData As Integer)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.DefectTypeID = 5
    mvarDefectTypeID = vData
End Property

Public Property Get DefectTypeID() As Integer
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.DefectTypeID
    DefectTypeID = mvarDefectTypeID
End Property



