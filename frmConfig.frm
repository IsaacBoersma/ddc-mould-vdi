VERSION 5.00
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Begin VB.Form frmConfig 
   Caption         =   "Configuration"
   ClientHeight    =   9825
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   5670
   LinkTopic       =   "Form1"
   ScaleHeight     =   9825
   ScaleWidth      =   5670
   StartUpPosition =   3  'Windows Default
   Begin VB.Frame Frame3 
      Caption         =   "Application Configuration"
      Height          =   5055
      Left            =   120
      TabIndex        =   16
      Top             =   2040
      Width           =   5415
      Begin VB.CheckBox ctlSendToOracle 
         Caption         =   "Send Data To Oracle"
         Height          =   375
         Left            =   3360
         TabIndex        =   32
         Top             =   4200
         Width           =   1815
      End
      Begin VB.ComboBox ctlDefectProfile 
         Height          =   315
         Left            =   1440
         TabIndex        =   31
         Top             =   1440
         Width           =   3735
      End
      Begin VB.TextBox ctlImagePath 
         Height          =   285
         Left            =   1800
         TabIndex        =   25
         Top             =   2880
         Width           =   3375
      End
      Begin VB.TextBox ctlDataMask 
         Height          =   285
         Left            =   1320
         TabIndex        =   24
         Top             =   3600
         Width           =   735
      End
      Begin VB.CommandButton Command1 
         Caption         =   "..."
         Height          =   285
         Left            =   2160
         TabIndex        =   23
         Top             =   3600
         Width           =   375
      End
      Begin VB.ComboBox ctlInsp 
         Height          =   315
         Left            =   2160
         TabIndex        =   22
         Top             =   360
         Width           =   3015
      End
      Begin VB.CheckBox ctlLockEnabled 
         Caption         =   "Allow Inspection Point Changes"
         Height          =   255
         Left            =   1080
         TabIndex        =   21
         Top             =   840
         Width           =   2535
      End
      Begin VB.CheckBox ctlOverrideEnabled 
         Caption         =   "Allow Disposition Override"
         Height          =   255
         Left            =   240
         TabIndex        =   20
         Top             =   2160
         Width           =   2655
      End
      Begin VB.CheckBox ctlSendScan 
         Caption         =   "Send Scan Data Over Comm Port"
         Height          =   375
         Left            =   240
         TabIndex        =   19
         Top             =   4200
         Width           =   2895
      End
      Begin VB.TextBox ctlPortNumber 
         Height          =   375
         Left            =   2400
         TabIndex        =   18
         Text            =   "1"
         Top             =   4560
         Width           =   255
      End
      Begin MSComCtl2.UpDown UpDown1 
         Height          =   375
         Left            =   2640
         TabIndex        =   17
         Top             =   4560
         Width           =   255
         _ExtentX        =   450
         _ExtentY        =   661
         _Version        =   393216
         Value           =   1
         BuddyControl    =   "UpDown1"
         BuddyDispid     =   196641
         OrigLeft        =   360
         OrigTop         =   5040
         OrigRight       =   615
         OrigBottom      =   5415
         Max             =   4
         Min             =   1
         SyncBuddy       =   -1  'True
         BuddyProperty   =   65547
         Enabled         =   -1  'True
      End
      Begin VB.Label Label2 
         Caption         =   "Defect Profile:"
         Height          =   255
         Left            =   240
         TabIndex        =   30
         Top             =   1490
         Width           =   1095
      End
      Begin VB.Label Label4 
         Caption         =   "Default Inspection Point:"
         Height          =   255
         Left            =   240
         TabIndex        =   29
         Top             =   390
         Width           =   1815
      End
      Begin VB.Label Label7 
         Caption         =   "Part(s) Image Path:"
         Height          =   255
         Left            =   240
         TabIndex        =   28
         Top             =   2895
         Width           =   1455
      End
      Begin VB.Label Label8 
         Caption         =   "Data Mask:"
         Height          =   255
         Left            =   240
         TabIndex        =   27
         Top             =   3615
         Width           =   975
      End
      Begin VB.Label Label9 
         Caption         =   "Use Comm Port:"
         Height          =   255
         Left            =   1200
         TabIndex        =   26
         Top             =   4620
         Width           =   1215
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "Remote Operation"
      Height          =   1815
      Left            =   120
      TabIndex        =   2
      Top             =   7200
      Width           =   5415
      Begin VB.TextBox ctlRemoteDatabase 
         Height          =   285
         Left            =   1560
         TabIndex        =   6
         Top             =   1440
         Width           =   3615
      End
      Begin VB.TextBox ctlRemoteServer 
         Height          =   285
         Left            =   1320
         TabIndex        =   4
         Top             =   960
         Width           =   3855
      End
      Begin VB.CheckBox ctlClientIsRemote 
         Caption         =   "Client Is Remote"
         Height          =   375
         Left            =   240
         TabIndex        =   3
         Top             =   360
         Width           =   1815
      End
      Begin VB.Label Label11 
         Caption         =   "Remote Database"
         Height          =   255
         Left            =   120
         TabIndex        =   7
         Top             =   1440
         Width           =   1335
      End
      Begin VB.Label Label10 
         Caption         =   "Remote Server"
         Height          =   255
         Left            =   120
         TabIndex        =   5
         Top             =   960
         Width           =   1095
      End
   End
   Begin VB.CommandButton cmdCancel 
      Caption         =   "Cancel"
      Height          =   495
      Left            =   3000
      TabIndex        =   1
      Top             =   9240
      Width           =   1095
   End
   Begin VB.CommandButton cmdOk 
      Caption         =   "Ok"
      Height          =   495
      Left            =   1560
      TabIndex        =   0
      Top             =   9240
      Width           =   1095
   End
   Begin VB.Frame Frame2 
      Caption         =   "Data Storage"
      Height          =   1815
      Left            =   120
      TabIndex        =   8
      Top             =   120
      Width           =   5415
      Begin VB.TextBox ctlTmOut 
         Height          =   285
         Left            =   1920
         TabIndex        =   15
         Top             =   1305
         Width           =   735
      End
      Begin VB.TextBox ctlServerName 
         Height          =   285
         Left            =   1440
         TabIndex        =   13
         Top             =   345
         Width           =   3735
      End
      Begin VB.TextBox ctlDbName 
         Height          =   285
         Left            =   1560
         TabIndex        =   11
         Top             =   840
         Width           =   3615
      End
      Begin VB.Label Label1 
         Caption         =   "sec."
         Height          =   255
         Left            =   2880
         TabIndex        =   14
         Top             =   1320
         Width           =   375
      End
      Begin VB.Label Label3 
         Caption         =   "Server Name:"
         Height          =   255
         Left            =   240
         TabIndex        =   12
         Top             =   360
         Width           =   1095
      End
      Begin VB.Label Label13 
         Caption         =   "Connection Timeout:"
         Height          =   255
         Left            =   240
         TabIndex        =   10
         Top             =   1320
         Width           =   1575
      End
      Begin VB.Label Label12 
         Caption         =   "Database Name:"
         Height          =   255
         Left            =   240
         TabIndex        =   9
         Top             =   855
         Width           =   1335
      End
   End
End
Attribute VB_Name = "frmConfig"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub cmdCancel_Click()
    On Error GoTo Err_Handler

    Unload frmConfig
    End
    
    Exit Sub
Err_Handler:
    logGeneralError "frmConfig.cmdCancel_Click", CStr(Err.Number), Err.Description
End Sub

Private Sub cmdOk_Click()
    Dim intTemp As Integer
    
    On Error GoTo Err_Handler
    
    intTemp = intTemp + IIf(ctlServerName = "", 0, 1)
    intTemp = intTemp + IIf(ctlDbName = "", 0, 1)
    intTemp = intTemp + IIf(ctlInsp = "", 0, 1)
    If intTemp = 3 Then
        Main
        Unload frmConfig
    End If
    
    Exit Sub
Err_Handler:
    logGeneralError "frmConfig.cmdOk_Click", CStr(Err.Number), Err.Description
End Sub

Private Sub Command1_Click()
    On Error GoTo Err_Handler

    frmDataMask.Show
    
    Exit Sub
Err_Handler:
    logGeneralError "frmConfig.Command1_Click", CStr(Err.Number), Err.Description
End Sub

Private Sub ctlDefectProfile_DropDown()
    Dim connTemp As New ADODB.Connection, rstTemp As New ADODB.Recordset, strSQL As String
    
    On Error GoTo Err_Connect
    
    ctlDefectProfile.Clear
    connTemp.ConnectionString = "Provider=SQLOLEDB.1;Persist Security Info=False;User " _
                & "ID=DDCApp;PWD=keRrTRN3;Initial Catalog=" & ctlDbName & ";Data Source=" _
                & ctlServerName
    strSQL = "SELECT * FROM tblDefectProfiles ORDER BY DefectProfileID"
    connTemp.Open
    rstTemp.Open strSQL, connTemp
    With ctlDefectProfile
        Do Until rstTemp.EOF
            .AddItem rstTemp("ProfileDescription")
            '.DataField = rsttemp("DefectProfileID")
            rstTemp.MoveNext
        Loop
    End With
    rstTemp.Close
    connTemp.Close
    
    Exit Sub
Err_Connect:
    logGeneralError "frmConfig.ctlDefectProfile_DropDown", CStr(Err.Number), Err.Description
    MsgBox "SQL Connection Error: " & Err.Description & ":" & Err.Number, vbOKOnly, "Connection Error"
    If connTemp.State = adStateOpen Then connTemp.Close
End Sub

Private Sub ctlInsp_DropDown()
    Dim connTemp As New ADODB.Connection, rstTemp As New ADODB.Recordset, strSQL As String
    
    On Error GoTo Err_Connect
    
    ctlInsp.Clear
    connTemp.ConnectionString = "Provider=SQLOLEDB.1;Persist Security Info=False;User " _
                & "ID=DDCApp;PWD=keRrTRN3;Initial Catalog=" & ctlDbName & ";Data Source=" _
                & ctlServerName
    strSQL = "SELECT * FROM tblInspectionPoints ORDER BY IPID"
    connTemp.Open
    rstTemp.Open strSQL, connTemp
    With ctlInsp
        Do Until rstTemp.EOF
            If Not rstTemp("IPID") = 0 Then
                .AddItem rstTemp("InspectionPoint")
            End If
            rstTemp.MoveNext
        Loop
    End With
    rstTemp.Close
    connTemp.Close
    
    Exit Sub
Err_Connect:
    logGeneralError "frmConfig.ctlInsp_DropDown", CStr(Err.Number), Err.Description
    MsgBox "SQL Connection Error: " & Err.Description & ":" & Err.Number, vbOKOnly, "Connection Error"
    If connTemp.State = adStateOpen Then connTemp.Close
End Sub

Private Sub Form_Load()
    On Error GoTo Err_Handler
    
    Main
    Unload frmConfig
    
    Exit Sub
Err_Handler:
    logGeneralError "frmConfig.Form_Load", CStr(Err.Number), Err.Description
End Sub
