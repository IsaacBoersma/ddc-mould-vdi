VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "DefectDefinition"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
'local variable(s) to hold property value(s)
Private mvarDefectType As String 'local copy
Private mvarDefectDescription As String 'local copy
Private mvarDefectInfo As String 'local copy
Private mvarDefectDefIDX As Integer 'local copy
Public Property Let DefectDefIDX(ByVal vData As Integer)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.DefectDefIDX = 5
    mvarDefectDefIDX = vData
End Property


Public Property Get DefectDefIDX() As Integer
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.DefectDefIDX
    DefectDefIDX = mvarDefectDefIDX
End Property


Public Property Let DefectInfo(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.DefectInfo = 5
    mvarDefectInfo = vData
End Property


Public Property Get DefectInfo() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.DefectInfo
    DefectInfo = mvarDefectInfo
End Property



Public Property Let DefectDescription(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.DefectDescription = 5
    mvarDefectDescription = vData
End Property


Public Property Get DefectDescription() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.DefectDescription
    DefectDescription = mvarDefectDescription
End Property



Public Property Let DefectType(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.DefectType = 5
    mvarDefectType = vData
End Property


Public Property Get DefectType() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.DefectType
    DefectType = mvarDefectType
End Property



