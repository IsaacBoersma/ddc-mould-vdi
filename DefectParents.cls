VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "DefectParents"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Collection" ,"DefectParent"
Attribute VB_Ext_KEY = "Member0" ,"DefectParent"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
'local variable to hold collection
Private mCol As Collection

Public Function Add(Optional EventID As String, Optional EventDateTime As Variant, _
                    Optional PartSN As String, Optional ItemClass As String, _
                    Optional PartNumber As String, Optional Comment As String, _
                    Optional LotNum As String, Optional LabelCount As Integer, _
                    Optional pressID As Integer, Optional pressDesc As String, Optional Flag As Integer, _
                    Optional sKey As String) As DefectParent
    'create a new object
    Dim objNewMember As DefectParent
    Set objNewMember = New DefectParent

    'set the properties passed into the method
    objNewMember.EventID = EventID
    objNewMember.PartSN = PartSN
    objNewMember.ItemClass = ItemClass
    objNewMember.PartNumber = PartNumber
    objNewMember.Comment = Comment
    objNewMember.LotNum = LotNum
    objNewMember.LabelCount = LabelCount
    objNewMember.pressID = pressID
    objNewMember.pressDesc = pressDesc
    objNewMember.Flag = Flag
    If IsObject(EventDateTime) Then
        Set objNewMember.EventDateTime = EventDateTime
    Else
        objNewMember.EventDateTime = EventDateTime
    End If
    
    If Len(sKey) = 0 Then
        mCol.Add objNewMember
    Else
        mCol.Add objNewMember, sKey
    End If

    'return the object created
    Set Add = objNewMember
    Set objNewMember = Nothing


End Function

Public Property Get Item(vntIndexKey As Variant) As DefectParent
Attribute Item.VB_UserMemId = 0
    'used when referencing an element in the collection
    'vntIndexKey contains either the Index or Key to the collection,
    'this is why it is declared as a Variant
    'Syntax: Set foo = x.Item(xyz) or Set foo = x.Item(5)
  Set Item = mCol(vntIndexKey)
End Property

Public Property Get Count() As Long
    'used when retrieving the number of elements in the
    'collection. Syntax: Debug.Print x.Count
    Count = mCol.Count
End Property

Public Sub Remove(vntIndexKey As Variant)
    'used when removing an element from the collection
    'vntIndexKey contains either the Index or Key, which is why
    'it is declared as a Variant
    'Syntax: x.Remove(xyz)
    mCol.Remove vntIndexKey
End Sub

Public Property Get NewEnum() As IUnknown
Attribute NewEnum.VB_UserMemId = -4
Attribute NewEnum.VB_MemberFlags = "40"
    'this property allows you to enumerate
    'this collection with the For...Each syntax
    Set NewEnum = mCol.[_NewEnum]
End Property

Private Sub Class_Initialize()
    'creates the collection when this class is created
    Set mCol = New Collection
End Sub

Private Sub Class_Terminate()
    'destroys collection when this class is terminated
    Set mCol = Nothing
End Sub

