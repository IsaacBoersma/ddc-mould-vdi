VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.OCX"
Begin VB.Form frmRemoteServerConnected 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Update Manager"
   ClientHeight    =   3405
   ClientLeft      =   2760
   ClientTop       =   3750
   ClientWidth     =   7335
   ClipControls    =   0   'False
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   Moveable        =   0   'False
   ScaleHeight     =   3405
   ScaleWidth      =   7335
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton Command1 
      Caption         =   "Close"
      Enabled         =   0   'False
      Height          =   375
      Left            =   6240
      TabIndex        =   3
      Top             =   2880
      Width           =   975
   End
   Begin VB.Timer Timer1 
      Interval        =   5
      Left            =   6720
      Top             =   2280
   End
   Begin MSComctlLib.ProgressBar ProgressBar1 
      Height          =   375
      Left            =   120
      TabIndex        =   2
      Top             =   2880
      Width           =   4815
      _ExtentX        =   8493
      _ExtentY        =   661
      _Version        =   393216
      Appearance      =   0
      Scrolling       =   1
   End
   Begin VB.ListBox ctlOperations 
      Height          =   2400
      Left            =   120
      TabIndex        =   1
      Top             =   360
      Width           =   7095
   End
   Begin VB.CommandButton ctlClose 
      Caption         =   "Open DDC"
      Enabled         =   0   'False
      Height          =   375
      Left            =   5040
      TabIndex        =   0
      Top             =   2880
      Width           =   975
   End
End
Attribute VB_Name = "frmRemoteServerConnected"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Option Explicit

Private strConnStr As String
Private oRemConn As New ADODB.Connection
Private ConnectionFailed As Boolean
Private strLocConnStr As String
Private oLocConn As New ADODB.Connection
Private Enum ConnectionLocation
    LocalConnection = 0
    RemoteConnection = 1
End Enum
Private PartSNs As New Collection
Private EventIDs As New Collection

Private Sub Command1_Click()
    End
End Sub

Private Sub ctlClose_Click()
    Unload frmRemoteServerConnected
End Sub

Private Sub InitiateConnection(eConnectionLocation As ConnectionLocation)
    Dim strServer As String, strDatabase As String
    
    On Error GoTo err_Handler
    
    Select Case eConnectionLocation
        Case LocalConnection
'            strServer = GetSettingString(HKEY_LOCAL_MACHINE, SV_KEY_ROOT & "\" & SV_KEY_PRFDATA, SV_VAL_SVRNAME, "")
'            strDatabase = GetSettingString(HKEY_LOCAL_MACHINE, SV_KEY_ROOT & "\" & SV_KEY_PRFDATA, SV_VAL_DBSNAME, "")
            strServer = frmDefectEntry.ServerName
            strDatabase = frmDefectEntry.DatabaseName
        Case RemoteConnection
'            strServer = GetSettingString(HKEY_LOCAL_MACHINE, SV_KEY_ROOT & "\" & SV_KEY_PRFDATA, SV_VAL_REMSERV, "")
'            strDatabase = GetSettingString(HKEY_LOCAL_MACHINE, SV_KEY_ROOT & "\" & SV_KEY_PRFDATA, SV_VAL_REMDBNM, "")
            strServer = "172.18.26.213"
            strDatabase = "DDC"
    End Select
    
    
    strConnStr = "Provider=SQLNCLI.1;Persist Security Info=True;User " _
                & "ID=DDCApp;PWD=keRrTRN3;Initial Catalog=" & strDatabase & ";Data Source=" _
                & strServer
                
    Select Case eConnectionLocation
        Case LocalConnection
        
            oLocConn.ConnectionString = strConnStr
            ctlOperations.AddItem ("Opening Local Connection...")
            ctlOperations.ListIndex = ctlOperations.ListCount - 1
            DoEvents
            oLocConn.Open
            
        Case RemoteConnection
        
            oRemConn.ConnectionString = strConnStr
            ctlOperations.AddItem ("Opening Remote Connection...")
            ctlOperations.ListIndex = ctlOperations.ListCount - 1
            DoEvents
            oRemConn.Open
    
    End Select
    
    
    Exit Sub
    
err_Handler:

    If CheckConnection(eConnectionLocation) = False Then
        ctlOperations.AddItem ("Connection Failed... Operation Cancelled")
        DoEvents
        ctlClose.Enabled = True
        ConnectionFailed = True
    Else
        logGeneralError "frmRemoteServerConnected.General.InitiateConnection", CStr(Err.Number), Err.Description
    End If
    
End Sub

Private Function CheckConnection(eConnectionLocation As ConnectionLocation) As Boolean
    Dim oADOErrors As ADODB.Errors, oADOError As ADODB.Error
    
    Select Case eConnectionLocation
        Case LocalConnection
            Set oADOErrors = oLocConn.Errors
        Case RemoteConnection
            Set oADOErrors = oRemConn.Errors
    End Select
    
    If oADOErrors.Count > 0 Then
        For Each oADOError In oADOErrors
            ctlOperations.AddItem (oADOError.Description)
            DoEvents
        Next oADOError
        ConnectionFailed = True
        CheckConnection = False
    Else
        CheckConnection = True
        Select Case eConnectionLocation
            Case LocalConnection
            
                ctlOperations.AddItem ("Local Connection Test Successful.")
                ctlOperations.ListIndex = ctlOperations.ListCount - 1
                DoEvents
                If oLocConn.State = adStateOpen Then
                    oLocConn.Close
                    ctlOperations.AddItem ("Local Connection Closed.")
                    ctlOperations.ListIndex = ctlOperations.ListCount - 1
                    DoEvents
                End If
            
            Case RemoteConnection
            
                ctlOperations.AddItem ("Remote Connection Test Successful.")
                ctlOperations.ListIndex = ctlOperations.ListCount - 1
                DoEvents
                If oRemConn.State = adStateOpen Then
                    oRemConn.Close
                    ctlOperations.AddItem ("Remote Connection Closed.")
                    ctlOperations.ListIndex = ctlOperations.ListCount - 1
                    DoEvents
                End If
            
        End Select
        
    End If
    
End Function

Private Sub GetItemClasses()
    Dim cmdRemote As New ADODB.Command, rstRemote As New ADODB.Recordset, paramRemote As New ADODB.Parameter
    Dim cmdLocal As New ADODB.Command, rstLocal As New ADODB.Recordset, paramLocal As New ADODB.Parameter
    Dim intRecCount As Integer
    
    On Error GoTo err_Handler
    
    oRemConn.Open
    oLocConn.Open
    
    cmdRemote.CommandText = "usp_RemoteDDC_GetItemClasses"
    cmdRemote.ActiveConnection = oRemConn
    cmdRemote.CommandType = adCmdStoredProc
    Set paramRemote = cmdRemote.CreateParameter
    paramRemote.Name = "@Count"
    paramRemote.Direction = adParamOutput
    paramRemote.Type = adInteger
    cmdRemote.Parameters.Append paramRemote
    
    Set rstRemote = cmdRemote.Execute
    
    cmdRemote.Execute
    intRecCount = cmdRemote.Parameters("@Count").Value
    
    If intRecCount > 0 Then
        
        cmdLocal.CommandText = "usp_DeleteItemClasses"
        cmdLocal.ActiveConnection = oLocConn
        cmdLocal.CommandType = adCmdStoredProc
        cmdLocal.Execute
        
        ctlOperations.AddItem ("Loading ItemClasses...")
        ctlOperations.ListIndex = ctlOperations.ListCount - 1
        DoEvents
        
        cmdLocal.CommandText = "usp_InsertItemClass"
        Set paramLocal = cmdLocal.CreateParameter
        paramLocal.Name = "@ItemClass"
        paramLocal.Direction = adParamInput
        paramLocal.Type = adVarChar
        paramLocal.Size = 10
        cmdLocal.Parameters.Append paramLocal
    
        Set paramLocal = cmdLocal.CreateParameter
        paramLocal.Name = "@ItemDescription"
        paramLocal.Direction = adParamInput
        paramLocal.Type = adVarChar
        paramLocal.Size = 50
        cmdLocal.Parameters.Append paramLocal
        
        ProgressBar1.Max = IIf(intRecCount > 0, intRecCount, 1)
        ProgressBar1.Value = 0
        
        With rstRemote
            Do While Not (rstRemote.EOF)
                With cmdLocal
                .Parameters(0).Value = rstRemote(0).Value
                .Parameters(1).Value = rstRemote(1).Value
                .Execute
                End With
                rstRemote.MoveNext
                ProgressBar1.Value = ProgressBar1.Value + 1
                ctlOperations.List(ctlOperations.ListIndex) = "Loading ItemClasses..." & CInt(ProgressBar1.Value / ProgressBar1.Max * 100) & "%"
            Loop
        End With
        
    End If
    
    oRemConn.Close
    oLocConn.Close
    
    Set rstRemote = Nothing
    ProgressBar1.Value = 0
      
    Exit Sub
    
err_Handler:

    If CheckConnection(RemoteConnection) = False Then
        ctlOperations.AddItem ("Connection Failed... Operation Cancelled")
        ctlOperations.ListIndex = ctlOperations.ListCount - 1
        DoEvents
        ctlClose.Enabled = True
        ConnectionFailed = True
    Else
        logGeneralError "frmRemoteServerConnected.General.GetItemClasses", CStr(Err.Number), Err.Description
    End If
    
End Sub

Private Sub GetParts()
    Dim cmdRemote As New ADODB.Command, rstRemote As New ADODB.Recordset, paramRemote As New ADODB.Parameter
    Dim cmdLocal As New ADODB.Command, rstLocal As New ADODB.Recordset, paramLocal As New ADODB.Parameter
    Dim intRecCount As Integer
    
    On Error GoTo err_Handler

    oRemConn.Open
    oLocConn.Open
    
    cmdRemote.CommandText = "usp_RemoteDDC_GetParts"
    cmdRemote.ActiveConnection = oRemConn
    cmdRemote.CommandType = adCmdStoredProc
    Set paramRemote = cmdRemote.CreateParameter
    paramRemote.Name = "@Count"
    paramRemote.Direction = adParamOutput
    paramRemote.Type = adInteger
    cmdRemote.Parameters.Append paramRemote
    
    Set rstRemote = cmdRemote.Execute
    
    cmdRemote.Execute
    intRecCount = cmdRemote.Parameters("@Count").Value
    
    If intRecCount > 0 Then
        
        cmdLocal.CommandText = "usp_DeleteParts"
        cmdLocal.ActiveConnection = oLocConn
        cmdLocal.CommandType = adCmdStoredProc
        cmdLocal.Execute
        
        ctlOperations.AddItem ("Loading Parts...")
        ctlOperations.ListIndex = ctlOperations.ListCount - 1
        DoEvents
        
        cmdLocal.CommandText = "usp_InsertPart"
        Set paramLocal = cmdLocal.CreateParameter
        paramLocal.Name = "@PartNumber"
        paramLocal.Direction = adParamInput
        paramLocal.Type = adVarChar
        paramLocal.Size = 20
        cmdLocal.Parameters.Append paramLocal
    
        Set paramLocal = cmdLocal.CreateParameter
        paramLocal.Name = "@ItemClass"
        paramLocal.Direction = adParamInput
        paramLocal.Type = adVarChar
        paramLocal.Size = 10
        cmdLocal.Parameters.Append paramLocal
        
        Set paramLocal = cmdLocal.CreateParameter
        paramLocal.Name = "@ColourCode"
        paramLocal.Direction = adParamInput
        paramLocal.Type = adVarChar
        paramLocal.Size = 10
        cmdLocal.Parameters.Append paramLocal
    
        Set paramLocal = cmdLocal.CreateParameter
        paramLocal.Name = "@PartDescription"
        paramLocal.Direction = adParamInput
        paramLocal.Type = adVarChar
        paramLocal.Size = 50
        cmdLocal.Parameters.Append paramLocal
        
        Set paramLocal = cmdLocal.CreateParameter
        paramLocal.Name = "WarehouseLocation"
        paramLocal.Direction = adParamInput
        paramLocal.Type = adVarChar
        paramLocal.Size = 6
        cmdLocal.Parameters.Append paramLocal
        
        Set paramLocal = cmdLocal.CreateParameter
        paramLocal.Name = "WorkCenter"
        paramLocal.Direction = adParamInput
        paramLocal.Type = adVarChar
        paramLocal.Size = 6
        cmdLocal.Parameters.Append paramLocal
        
        
        ProgressBar1.Max = IIf(intRecCount > 0, intRecCount, 1)
        ProgressBar1.Value = 0
        
        With rstRemote
            Do While Not (rstRemote.EOF)
                With cmdLocal
                .Parameters(0).Value = RTrim(rstRemote(0).Value)
                .Parameters(1).Value = RTrim(rstRemote(1).Value)
                .Parameters(2).Value = RTrim(rstRemote(2).Value)
                .Parameters(3).Value = RTrim(rstRemote(3).Value)
                .Parameters(4).Value = RTrim(rstRemote(4).Value)
                .Parameters(5).Value = RTrim(rstRemote(5).Value)
                .Execute
                End With
                rstRemote.MoveNext
                ProgressBar1.Value = ProgressBar1.Value + 1
                ctlOperations.List(ctlOperations.ListIndex) = "Loading Parts..." & CInt(ProgressBar1.Value / ProgressBar1.Max * 100) & "%"
            Loop
        End With
        
    End If
    
    oRemConn.Close
    oLocConn.Close
    
    Set rstRemote = Nothing
    ProgressBar1.Value = 0
        
    Exit Sub
    
err_Handler:

    If CheckConnection(RemoteConnection) = False Then
        ctlOperations.AddItem ("Connection Failed... Operation Cancelled")
        ctlOperations.ListIndex = ctlOperations.ListCount - 1
        DoEvents
        ctlClose.Enabled = True
        ConnectionFailed = True
    Else
        logGeneralError "frmRemoteServerConnected.General.GetParts", CStr(Err.Number), Err.Description
    End If
    
End Sub

Private Sub GetDefectTypes()
    Dim cmdRemote As New ADODB.Command, rstRemote As New ADODB.Recordset, paramRemote As New ADODB.Parameter
    Dim cmdLocal As New ADODB.Command, rstLocal As New ADODB.Recordset, paramLocal As New ADODB.Parameter
    Dim intRecCount As Integer
    
    On Error GoTo err_Handler
    
    oRemConn.Open
    oLocConn.Open
    
    cmdRemote.CommandText = "usp_RemoteDDC_GetDefectTypes"
    cmdRemote.ActiveConnection = oRemConn
    cmdRemote.CommandType = adCmdStoredProc
    Set paramRemote = cmdRemote.CreateParameter
    paramRemote.Name = "@Count"
    paramRemote.Direction = adParamOutput
    paramRemote.Type = adInteger
    cmdRemote.Parameters.Append paramRemote
    
    Set rstRemote = cmdRemote.Execute
    
    cmdRemote.Execute
    intRecCount = cmdRemote.Parameters("@Count").Value
    
    If intRecCount > 0 Then
        
        cmdLocal.CommandText = "usp_DeleteDefectTypes"
        cmdLocal.ActiveConnection = oLocConn
        cmdLocal.CommandType = adCmdStoredProc
        cmdLocal.Execute
        
        ctlOperations.AddItem ("Loading Defect Types...")
        ctlOperations.ListIndex = ctlOperations.ListCount - 1
        DoEvents
            
        cmdLocal.CommandText = "usp_InsertDefectType"
        Set paramLocal = cmdLocal.CreateParameter
        paramLocal.Name = "@DefectTypeID"
        paramLocal.Direction = adParamInput
        paramLocal.Type = adInteger
        cmdLocal.Parameters.Append paramLocal
    
        Set paramLocal = cmdLocal.CreateParameter
        paramLocal.Name = "@DefectType"
        paramLocal.Direction = adParamInput
        paramLocal.Type = adVarChar
        paramLocal.Size = 50
        cmdLocal.Parameters.Append paramLocal
        
        ProgressBar1.Max = IIf(intRecCount > 0, intRecCount, 1)
        ProgressBar1.Value = 0
        
        With rstRemote
            Do While Not (rstRemote.EOF)
                With cmdLocal
                .Parameters(0).Value = rstRemote(0).Value
                .Parameters(1).Value = rstRemote(1).Value
                .Execute
                End With
                rstRemote.MoveNext
                ProgressBar1.Value = ProgressBar1.Value + 1
                ctlOperations.List(ctlOperations.ListIndex) = "Loading Defect Types..." & CInt(ProgressBar1.Value / ProgressBar1.Max * 100) & "%"
            Loop
        End With
        
    End If
    
    oRemConn.Close
    oLocConn.Close
    
    Set rstRemote = Nothing
    ProgressBar1.Value = 0
        
    Exit Sub
    
err_Handler:

    If CheckConnection(RemoteConnection) = False Then
        ctlOperations.AddItem ("Connection Failed... Operation Cancelled")
        ctlOperations.ListIndex = ctlOperations.ListCount - 1
        DoEvents
        ctlClose.Enabled = True
        ConnectionFailed = True
    Else
        logGeneralError "frmRemoteServerConnected.General.GetDefectTypes", CStr(Err.Number), Err.Description
    End If
    
End Sub

Private Sub GetDefectDescriptions()
    Dim cmdRemote As New ADODB.Command, rstRemote As New ADODB.Recordset, paramRemote As New ADODB.Parameter
    Dim cmdLocal As New ADODB.Command, rstLocal As New ADODB.Recordset, paramLocal As New ADODB.Parameter
    Dim intRecCount As Integer
    
    On Error GoTo err_Handler
    
    oRemConn.Open
    oLocConn.Open
    
    cmdRemote.CommandText = "usp_RemoteDDC_GetDefectDescriptions"
    cmdRemote.ActiveConnection = oRemConn
    cmdRemote.CommandType = adCmdStoredProc
    Set paramRemote = cmdRemote.CreateParameter
    paramRemote.Name = "@Count"
    paramRemote.Direction = adParamOutput
    paramRemote.Type = adInteger
    cmdRemote.Parameters.Append paramRemote
    
    Set rstRemote = cmdRemote.Execute
    
    cmdRemote.Execute
    intRecCount = cmdRemote.Parameters("@Count").Value
    
    If intRecCount > 0 Then
        
        ctlOperations.AddItem ("Loading Defect Descriptions...")
        ctlOperations.ListIndex = ctlOperations.ListCount - 1
        DoEvents
        
        cmdLocal.CommandText = "usp_DeleteDefectDescriptions"
        cmdLocal.ActiveConnection = oLocConn
        cmdLocal.CommandType = adCmdStoredProc
        cmdLocal.Execute
                
        cmdLocal.CommandText = "usp_InsertDefectDescription"
        Set paramLocal = cmdLocal.CreateParameter
        paramLocal.Name = "@DefectTypeID"
        paramLocal.Direction = adParamInput
        paramLocal.Type = adInteger
        cmdLocal.Parameters.Append paramLocal
    
        Set paramLocal = cmdLocal.CreateParameter
        paramLocal.Name = "@DefectDescriptionID"
        paramLocal.Direction = adParamInput
        paramLocal.Type = adInteger
        cmdLocal.Parameters.Append paramLocal
        
        Set paramLocal = cmdLocal.CreateParameter
        paramLocal.Name = "@DefectDescription"
        paramLocal.Direction = adParamInput
        paramLocal.Type = adVarChar
        paramLocal.Size = 50
        cmdLocal.Parameters.Append paramLocal
    
        Set paramLocal = cmdLocal.CreateParameter
        paramLocal.Name = "@DefectGroup"
        paramLocal.Direction = adParamInput
        paramLocal.Type = adInteger
        cmdLocal.Parameters.Append paramLocal
        
        ProgressBar1.Max = IIf(intRecCount > 0, intRecCount, 1)
        ProgressBar1.Value = 0
        
        With rstRemote
            Do While Not (rstRemote.EOF)
                With cmdLocal
                .Parameters(0).Value = rstRemote(0).Value
                .Parameters(1).Value = rstRemote(1).Value
                .Parameters(2).Value = rstRemote(2).Value
                .Parameters(3).Value = rstRemote(3).Value
                .Execute
                End With
                rstRemote.MoveNext
                ProgressBar1.Value = ProgressBar1.Value + 1
                ctlOperations.List(ctlOperations.ListIndex) = "Loading Defect Descriptions..." & CInt(ProgressBar1.Value / ProgressBar1.Max * 100) & "%"
            Loop
        End With
        
    End If
    
    oRemConn.Close
    oLocConn.Close
    
    Set rstRemote = Nothing
    ProgressBar1.Value = 0
        
    Exit Sub
    
err_Handler:

    If CheckConnection(RemoteConnection) = False Then
        ctlOperations.AddItem ("Connection Failed... Operation Cancelled")
        ctlOperations.ListIndex = ctlOperations.ListCount - 1
        DoEvents
        ctlClose.Enabled = True
        ConnectionFailed = True
    Else
        logGeneralError "frmRemoteServerConnected.General.GetDefectDescriptions", CStr(Err.Number), Err.Description
    End If

End Sub

Private Sub GetInspectionPoints()
    Dim cmdRemote As New ADODB.Command, rstRemote As New ADODB.Recordset, paramRemote As New ADODB.Parameter
    Dim cmdLocal As New ADODB.Command, rstLocal As New ADODB.Recordset, paramLocal As New ADODB.Parameter
    Dim intRecCount As Integer
    
    On Error GoTo err_Handler
    
    oRemConn.Open
    oLocConn.Open
    
    cmdRemote.CommandText = "usp_RemoteDDC_GetInspectionPoints"
    cmdRemote.ActiveConnection = oRemConn
    cmdRemote.CommandType = adCmdStoredProc
    Set paramRemote = cmdRemote.CreateParameter
    paramRemote.Name = "@Count"
    paramRemote.Direction = adParamOutput
    paramRemote.Type = adInteger
    cmdRemote.Parameters.Append paramRemote
    
    Set rstRemote = cmdRemote.Execute
    
    cmdRemote.Execute
    intRecCount = cmdRemote.Parameters("@Count").Value
    
    If intRecCount > 0 Then
        
        ctlOperations.AddItem ("Loading Inspection Points...")
        ctlOperations.ListIndex = ctlOperations.ListCount - 1
        DoEvents
        
        cmdLocal.CommandText = "usp_DeleteInspectionPoints"
        cmdLocal.ActiveConnection = oLocConn
        cmdLocal.CommandType = adCmdStoredProc
        cmdLocal.Execute
                
        cmdLocal.CommandText = "usp_InsertInspectionPoint"
        Set paramLocal = cmdLocal.CreateParameter
        paramLocal.Name = "@IPID"
        paramLocal.Direction = adParamInput
        paramLocal.Type = adInteger
        cmdLocal.Parameters.Append paramLocal
    
        Set paramLocal = cmdLocal.CreateParameter
        paramLocal.Name = "@InspectionPoint"
        paramLocal.Direction = adParamInput
        paramLocal.Type = adVarChar
        paramLocal.Size = 50
        cmdLocal.Parameters.Append paramLocal
        
        Set paramLocal = cmdLocal.CreateParameter
        paramLocal.Name = "@BadgeID"
        paramLocal.Direction = adParamInput
        paramLocal.Type = adChar
        paramLocal.Size = 5
        cmdLocal.Parameters.Append paramLocal
    
        ProgressBar1.Max = IIf(intRecCount > 0, intRecCount, 1)
        ProgressBar1.Value = 0
        
        With rstRemote
            Do While Not (rstRemote.EOF)
                With cmdLocal
                .Parameters(0).Value = rstRemote(0).Value
                .Parameters(1).Value = rstRemote(1).Value
                .Parameters(2).Value = rstRemote(2).Value
                .Execute
                End With
                rstRemote.MoveNext
                ProgressBar1.Value = ProgressBar1.Value + 1
                ctlOperations.List(ctlOperations.ListIndex) = "Loading Inspection Points..." & CInt(ProgressBar1.Value / ProgressBar1.Max * 100) & "%"
            Loop
        End With
        
    End If
    
    oRemConn.Close
    oLocConn.Close
    
    Set rstRemote = Nothing
    ProgressBar1.Value = 0
        
    Exit Sub
    
err_Handler:

    If CheckConnection(RemoteConnection) = False Then
        ctlOperations.AddItem ("Connection Failed... Operation Cancelled")
        ctlOperations.ListIndex = ctlOperations.ListCount - 1
        DoEvents
        ctlClose.Enabled = True
        ConnectionFailed = True
    Else
        logGeneralError "frmRemoteServerConnected.General.GetInspectionPoints", CStr(Err.Number), Err.Description
    End If

End Sub

Private Sub GetDispositions()
    Dim cmdRemote As New ADODB.Command, rstRemote As New ADODB.Recordset, paramRemote As New ADODB.Parameter
    Dim cmdLocal As New ADODB.Command, rstLocal As New ADODB.Recordset, paramLocal As New ADODB.Parameter
    Dim intRecCount As Integer
    
    On Error GoTo err_Handler
    
    oRemConn.Open
    oLocConn.Open
    
    cmdRemote.CommandText = "usp_RemoteDDC_GetDispositions"
    cmdRemote.ActiveConnection = oRemConn
    cmdRemote.CommandType = adCmdStoredProc
    Set paramRemote = cmdRemote.CreateParameter
    paramRemote.Name = "@Count"
    paramRemote.Direction = adParamOutput
    paramRemote.Type = adInteger
    cmdRemote.Parameters.Append paramRemote
    
    Set rstRemote = cmdRemote.Execute
    
    cmdRemote.Execute
    intRecCount = cmdRemote.Parameters("@Count").Value
    
    If intRecCount > 0 Then
        
        ctlOperations.AddItem ("Loading Dispositions...")
        ctlOperations.ListIndex = ctlOperations.ListCount - 1
        DoEvents
        
        cmdLocal.CommandText = "usp_DeleteDispositions"
        cmdLocal.ActiveConnection = oLocConn
        cmdLocal.CommandType = adCmdStoredProc
        cmdLocal.Execute
                
        cmdLocal.CommandText = "usp_InsertDisposition"
        Set paramLocal = cmdLocal.CreateParameter
        paramLocal.Name = "@DispositionID"
        paramLocal.Direction = adParamInput
        paramLocal.Type = adInteger
        cmdLocal.Parameters.Append paramLocal
    
        Set paramLocal = cmdLocal.CreateParameter
        paramLocal.Name = "@DispositionName"
        paramLocal.Direction = adParamInput
        paramLocal.Type = adVarChar
        paramLocal.Size = 50
        cmdLocal.Parameters.Append paramLocal
            
        ProgressBar1.Max = IIf(intRecCount > 0, intRecCount, 1)
        ProgressBar1.Value = 0
        
        With rstRemote
            Do While Not (rstRemote.EOF)
                With cmdLocal
                .Parameters(0).Value = rstRemote(0).Value
                .Parameters(1).Value = rstRemote(1).Value
                .Execute
                End With
                rstRemote.MoveNext
                ProgressBar1.Value = ProgressBar1.Value + 1
                ctlOperations.List(ctlOperations.ListIndex) = "Loading Dispositions..." & CInt(ProgressBar1.Value / ProgressBar1.Max * 100) & "%"
            Loop
        End With
        
    End If
    
    oRemConn.Close
    oLocConn.Close
    
    Set rstRemote = Nothing
    ProgressBar1.Value = 0
        
    Exit Sub
    
err_Handler:

    If CheckConnection(RemoteConnection) = False Then
        ctlOperations.AddItem ("Connection Failed... Operation Cancelled")
        ctlOperations.ListIndex = ctlOperations.ListCount - 1
        DoEvents
        ctlClose.Enabled = True
        ConnectionFailed = True
    Else
        logGeneralError "frmRemoteServerConnected.General.GetDispositions", CStr(Err.Number), Err.Description
    End If

End Sub

Private Sub SendDefectMasterInfo()
    Dim cmdRemote As New ADODB.Command, rstRemote As New ADODB.Recordset, paramRemote As New ADODB.Parameter
    Dim cmdLocal As New ADODB.Command, rstLocal As New ADODB.Recordset, paramLocal As New ADODB.Parameter, strmLocal As New ADODB.Stream
    Dim intRecCount As Integer, blInTrans As Boolean, oEventID As Variant
    
    On Error GoTo err_Handler
    
    oRemConn.Open
    oLocConn.Open
    
    cmdLocal.CommandText = "usp_GetDefectMasterInfo"
    cmdLocal.ActiveConnection = oLocConn
    cmdLocal.CommandType = adCmdStoredProc
    Set paramLocal = cmdLocal.CreateParameter
    paramLocal.Name = "@Count"
    paramLocal.Direction = adParamOutput
    paramLocal.Type = adInteger
    cmdLocal.Parameters.Append paramLocal
    
    Set rstLocal = cmdLocal.Execute

    cmdLocal.Execute
    intRecCount = cmdLocal.Parameters("@Count").Value
    
    ctlOperations.AddItem ("Backup DefectMasters...")
    ctlOperations.ListIndex = ctlOperations.ListCount - 1
    rstLocal.Save strmLocal, adPersistXML
    
    strmLocal.SaveToFile App.Path & "\tblDefectMaster.xml", adSaveCreateOverWrite
    
    strmLocal.Close
    Set strmLocal = Nothing
    
    ctlOperations.List(ctlOperations.ListIndex) = "Backup DefectMasters...done"
    
    Set rstLocal = cmdLocal.Execute
    
    ctlOperations.AddItem ("Sending Defect Masters...")
    ctlOperations.ListIndex = ctlOperations.ListCount - 1
    DoEvents
        
    If intRecCount > 0 Then
               
        cmdRemote.CommandText = "usp_RemoteDDC_InsertDefectMaster"
        cmdRemote.ActiveConnection = oRemConn
        cmdRemote.CommandType = adCmdStoredProc
        Set paramRemote = cmdRemote.CreateParameter
        paramRemote.Name = "@EventID"
        paramRemote.Direction = adParamInput
        paramRemote.Type = adVarChar
        paramRemote.Size = 50
        cmdRemote.Parameters.Append paramRemote
    
        Set paramRemote = cmdRemote.CreateParameter
        paramRemote.Name = "@EventDateTime"
        paramRemote.Direction = adParamInput
        paramRemote.Type = adDBDate
        cmdRemote.Parameters.Append paramRemote
        
        Set paramRemote = cmdRemote.CreateParameter
        paramRemote.Name = "@LotNum"
        paramRemote.Direction = adParamInput
        paramRemote.Type = adVarChar
        paramRemote.Size = 50
        cmdRemote.Parameters.Append paramRemote
    
        Set paramRemote = cmdRemote.CreateParameter
        paramRemote.Name = "@PartSN"
        paramRemote.Direction = adParamInput
        paramRemote.Type = adVarChar
        paramRemote.Size = 50
        cmdRemote.Parameters.Append paramRemote
        
        Set paramRemote = cmdRemote.CreateParameter
        paramRemote.Name = "@ItemClass"
        paramRemote.Direction = adParamInput
        paramRemote.Type = adVarChar
        paramRemote.Size = 50
        cmdRemote.Parameters.Append paramRemote
        
        Set paramRemote = cmdRemote.CreateParameter
        paramRemote.Name = "@PartNumber"
        paramRemote.Direction = adParamInput
        paramRemote.Type = adVarChar
        paramRemote.Size = 50
        cmdRemote.Parameters.Append paramRemote
        
        Set paramRemote = cmdRemote.CreateParameter
        paramRemote.Name = "@Comment"
        paramRemote.Direction = adParamInput
        paramRemote.Type = adVarChar
        paramRemote.Size = 50
        cmdRemote.Parameters.Append paramRemote
        
        Set paramRemote = cmdRemote.CreateParameter
        paramRemote.Name = "@LabelCount"
        paramRemote.Direction = adParamInput
        paramRemote.Type = adInteger
        cmdRemote.Parameters.Append paramRemote
        
        ProgressBar1.Max = IIf(intRecCount > 0, intRecCount, 1)
        ProgressBar1.Value = 0
        
        Set cmdLocal = New ADODB.Command
        cmdLocal.CommandText = "usp_DeleteDefectMasters"
        cmdLocal.ActiveConnection = oLocConn
        cmdLocal.CommandType = adCmdStoredProc
        Set paramLocal = cmdLocal.CreateParameter
        paramLocal.Name = "@EventID"
        paramLocal.Direction = adParamInput
        paramLocal.Type = adVarChar
        paramLocal.Size = 50
        cmdLocal.Parameters.Append paramLocal
    
        With rstLocal
            Do While Not (.EOF)
                With cmdRemote
                    PartSNs.Add rstLocal(3).Value
                    .Parameters(0).Value = rstLocal(0).Value
                    .Parameters(1).Value = rstLocal(1).Value
                    .Parameters(2).Value = rstLocal(2).Value
                    .Parameters(3).Value = rstLocal(3).Value
                    .Parameters(4).Value = rstLocal(4).Value
                    .Parameters(5).Value = rstLocal(5).Value
                    .Parameters(6).Value = rstLocal(6).Value
                    .Parameters(7).Value = rstLocal(7).Value
                    oRemConn.BeginTrans
                    blInTrans = True
                    .Execute
                    cmdLocal.Parameters(0).Value = rstLocal(0).Value
                    cmdLocal.Execute
                    oRemConn.CommitTrans
                    blInTrans = False
                End With
                rstLocal.MoveNext
                ProgressBar1.Value = ProgressBar1.Value + 1
                ctlOperations.List(ctlOperations.ListIndex) = "Sending Defect Masters..." & CInt(ProgressBar1.Value / ProgressBar1.Max * 100) & "%"
            Loop
        End With
        
    Else
    
        ctlOperations.List(ctlOperations.ListIndex) = "Sending Defect Masters...n/a"
    
    End If
    
    oRemConn.Close
    oLocConn.Close
    
    Set rstLocal = Nothing
    ProgressBar1.Value = 0
       
    Exit Sub
    
err_Handler:

    If blInTrans = True Then
        oRemConn.RollbackTrans
        blInTrans = False
    End If

    If CheckConnection(RemoteConnection) = False Then
        ctlOperations.AddItem ("Connection Failed... Operation Cancelled")
        ctlOperations.ListIndex = ctlOperations.ListCount - 1
        DoEvents
        ctlClose.Enabled = True
        ConnectionFailed = True
    Else
        logGeneralError "frmRemoteServerConnected.General.SendDefectMasterInfo", CStr(Err.Number), Err.Description
    End If
    
End Sub

Private Sub SendDefectSubItemInfo()
    Dim cmdRemote As New ADODB.Command, rstRemote As New ADODB.Recordset, paramRemote As New ADODB.Parameter
    Dim cmdLocal As New ADODB.Command, rstLocal As New ADODB.Recordset, paramLocal As New ADODB.Parameter, strmLocal As New ADODB.Stream
    Dim intRecCount As Integer, blInTrans As Boolean
    
    On Error GoTo err_Handler
    
    oRemConn.Open
    oLocConn.Open
    
    cmdLocal.CommandText = "usp_GetDefectSubItemInfo"
    cmdLocal.ActiveConnection = oLocConn
    cmdLocal.CommandType = adCmdStoredProc
    Set paramLocal = cmdLocal.CreateParameter
    paramLocal.Name = "@Count"
    paramLocal.Direction = adParamOutput
    paramLocal.Type = adInteger
    cmdLocal.Parameters.Append paramLocal
    
    Set rstLocal = cmdLocal.Execute
    
    ctlOperations.AddItem ("Backup DefectSubItems...")
    ctlOperations.ListIndex = ctlOperations.ListCount - 1
    rstLocal.Save strmLocal, adPersistXML
    
    strmLocal.SaveToFile App.Path & "\tblDefectSubItems.xml", adSaveCreateOverWrite
    
    strmLocal.Close
    Set strmLocal = Nothing
    
    ctlOperations.List(ctlOperations.ListIndex) = "Backup DefectSubItems...done"
    
    Set rstLocal = cmdLocal.Execute
    
    cmdLocal.Execute
    intRecCount = cmdLocal.Parameters("@Count").Value
    
    ctlOperations.AddItem ("Sending Defect SubItems...")
    ctlOperations.ListIndex = ctlOperations.ListCount - 1
    DoEvents
    
    If intRecCount > 0 Then
               
        cmdRemote.CommandText = "usp_RemoteDDC_InsertDefectSubItem"
        cmdRemote.ActiveConnection = oRemConn
        cmdRemote.CommandType = adCmdStoredProc
        Set paramRemote = cmdRemote.CreateParameter
        paramRemote.Name = "@EventID"
        paramRemote.Direction = adParamInput
        paramRemote.Type = adVarChar
        paramRemote.Size = 50
        cmdRemote.Parameters.Append paramRemote
    
        Set paramRemote = cmdRemote.CreateParameter
        paramRemote.Name = "@DefectID"
        paramRemote.Direction = adParamInput
        paramRemote.Type = adVarChar
        paramRemote.Size = 50
        cmdRemote.Parameters.Append paramRemote
    
        Set paramRemote = cmdRemote.CreateParameter
        paramRemote.Name = "@Xpos"
        paramRemote.Direction = adParamInput
        paramRemote.Type = adInteger
        cmdRemote.Parameters.Append paramRemote
    
        Set paramRemote = cmdRemote.CreateParameter
        paramRemote.Name = "@Ypos"
        paramRemote.Direction = adParamInput
        paramRemote.Type = adInteger
        cmdRemote.Parameters.Append paramRemote
    
        Set paramRemote = cmdRemote.CreateParameter
        paramRemote.Name = "@DefectDescriptionID"
        paramRemote.Direction = adParamInput
        paramRemote.Type = adInteger
        cmdRemote.Parameters.Append paramRemote
    
        Set paramRemote = cmdRemote.CreateParameter
        paramRemote.Name = "@DefectDateTime"
        paramRemote.Direction = adParamInput
        paramRemote.Type = adDBDate
        cmdRemote.Parameters.Append paramRemote
    
        Set paramRemote = cmdRemote.CreateParameter
        paramRemote.Name = "@IPID"
        paramRemote.Direction = adParamInput
        paramRemote.Type = adInteger
        cmdRemote.Parameters.Append paramRemote
        
        ProgressBar1.Max = IIf(intRecCount > 0, intRecCount, 1)
        ProgressBar1.Value = 0
        
        Set cmdLocal = New ADODB.Command
        cmdLocal.CommandText = "usp_DeleteDefectSubItems"
        cmdLocal.ActiveConnection = oLocConn
        cmdLocal.CommandType = adCmdStoredProc
        Set paramLocal = cmdLocal.CreateParameter
        paramLocal.Name = "@DefectID"
        paramLocal.Direction = adParamInput
        paramLocal.Type = adVarChar
        paramLocal.Size = 50
        cmdLocal.Parameters.Append paramLocal
    
        With rstLocal
            Do While Not (.EOF)
                With cmdRemote
                    .Parameters(0).Value = rstLocal(0).Value
                    .Parameters(1).Value = rstLocal(1).Value
                    .Parameters(2).Value = rstLocal(2).Value
                    .Parameters(3).Value = rstLocal(3).Value
                    .Parameters(4).Value = rstLocal(4).Value
                    .Parameters(5).Value = rstLocal(5).Value
                    .Parameters(6).Value = rstLocal(6).Value
                    oRemConn.BeginTrans
                    blInTrans = True
                    .Execute
                    cmdLocal.Parameters(0).Value = rstLocal(1).Value
                    cmdLocal.Execute
                    oRemConn.CommitTrans
                    blInTrans = False
                End With
                rstLocal.MoveNext
                ProgressBar1.Value = ProgressBar1.Value + 1
                ctlOperations.List(ctlOperations.ListIndex) = "Sending Defect SubItems..." & CInt(ProgressBar1.Value / ProgressBar1.Max * 100) & "%"
            Loop
        End With
        
    Else
    
        ctlOperations.List(ctlOperations.ListIndex) = "Sending Defect SubItems...n/a"
    
    End If
    
    oRemConn.Close
    oLocConn.Close
    
    Set rstLocal = Nothing
    ProgressBar1.Value = 0
        
    Exit Sub
    
err_Handler:

    If blInTrans = True Then
        oRemConn.RollbackTrans
        blInTrans = False
    End If

    If CheckConnection(RemoteConnection) = False Then
        ctlOperations.AddItem ("Connection Failed... Operation Cancelled")
        ctlOperations.ListIndex = ctlOperations.ListCount - 1
        DoEvents
        ctlClose.Enabled = True
        ConnectionFailed = True
    Else
        logGeneralError "frmRemoteServerConnected.General.SendDefectSubItemInfo", CStr(Err.Number), Err.Description
    End If
    
End Sub

Private Sub SendInspectionTransInfo()
    Dim cmdRemote As New ADODB.Command, rstRemote As New ADODB.Recordset, paramRemote As New ADODB.Parameter
    Dim cmdLocal As New ADODB.Command, rstLocal As New ADODB.Recordset, paramLocal As New ADODB.Parameter, strmLocal As New ADODB.Stream
    Dim intRecCount As Integer, blInTrans As Boolean
    
    On Error GoTo err_Handler
    
    oRemConn.Open
    oLocConn.Open
    
    cmdLocal.CommandText = "usp_GetInspectionTransInfo"
    cmdLocal.ActiveConnection = oLocConn
    cmdLocal.CommandType = adCmdStoredProc
    Set paramLocal = cmdLocal.CreateParameter
    paramLocal.Name = "@Count"
    paramLocal.Direction = adParamOutput
    paramLocal.Type = adInteger
    cmdLocal.Parameters.Append paramLocal
    
    Set rstLocal = cmdLocal.Execute

    ctlOperations.AddItem ("Backup InspectionTrans ...")
    ctlOperations.ListIndex = ctlOperations.ListCount - 1
    rstLocal.Save strmLocal, adPersistXML
    
    strmLocal.SaveToFile App.Path & "\tblInspectionTrans.xml", adSaveCreateOverWrite
    
    strmLocal.Close
    Set strmLocal = Nothing
    
    ctlOperations.List(ctlOperations.ListIndex) = "Backup InspectionTrans...done"
    
    Set rstLocal = cmdLocal.Execute

    cmdLocal.Execute
    intRecCount = cmdLocal.Parameters("@Count").Value
    
    ctlOperations.AddItem ("Sending InspectionTrans...")
    ctlOperations.ListIndex = ctlOperations.ListCount - 1
    DoEvents
           
    If intRecCount > 0 Then
        
        cmdRemote.CommandText = "usp_RemoteDDC_InsertInspectionTrans"
        cmdRemote.ActiveConnection = oRemConn
        cmdRemote.CommandType = adCmdStoredProc
        Set paramRemote = cmdRemote.CreateParameter
        paramRemote.Name = "@EventID"
        paramRemote.Direction = adParamInput
        paramRemote.Type = adVarChar
        paramRemote.Size = 50
        cmdRemote.Parameters.Append paramRemote
    
        Set paramRemote = cmdRemote.CreateParameter
        paramRemote.Name = "@PartSN"
        paramRemote.Direction = adParamInput
        paramRemote.Type = adVarChar
        paramRemote.Size = 50
        cmdRemote.Parameters.Append paramRemote
    
        Set paramRemote = cmdRemote.CreateParameter
        paramRemote.Name = "@FromIPID"
        paramRemote.Direction = adParamInput
        paramRemote.Type = adInteger
        cmdRemote.Parameters.Append paramRemote
    
        Set paramRemote = cmdRemote.CreateParameter
        paramRemote.Name = "@ToIPID"
        paramRemote.Direction = adParamInput
        paramRemote.Type = adInteger
        cmdRemote.Parameters.Append paramRemote
    
        Set paramRemote = cmdRemote.CreateParameter
        paramRemote.Name = "@DispositionID"
        paramRemote.Direction = adParamInput
        paramRemote.Type = adInteger
        cmdRemote.Parameters.Append paramRemote
    
        Set paramRemote = cmdRemote.CreateParameter
        paramRemote.Name = "@TransactionDateTime"
        paramRemote.Direction = adParamInput
        paramRemote.Type = adDBDate
        cmdRemote.Parameters.Append paramRemote
    
        Set paramRemote = cmdRemote.CreateParameter
        paramRemote.Name = "@EmployeeID"
        paramRemote.Direction = adParamInput
        paramRemote.Type = adInteger
        cmdRemote.Parameters.Append paramRemote
        
        ProgressBar1.Max = IIf(intRecCount > 0, intRecCount, 1)
        ProgressBar1.Value = 0
        
        Set cmdLocal = New ADODB.Command
        cmdLocal.CommandText = "usp_DeleteInspectionTrans"
        cmdLocal.ActiveConnection = oLocConn
        cmdLocal.CommandType = adCmdStoredProc
        Set paramLocal = cmdLocal.CreateParameter
        paramLocal.Name = "@PartSN"
        paramLocal.Direction = adParamInput
        paramLocal.Type = adVarChar
        paramLocal.Size = 50
        cmdLocal.Parameters.Append paramLocal
        
        Set paramLocal = cmdLocal.CreateParameter
        paramLocal.Name = "@TransactionDateTime"
        paramLocal.Direction = adParamInput
        paramLocal.Type = adDBDate
        cmdLocal.Parameters.Append paramLocal
    
        With rstLocal
            Do While Not (.EOF)
                With cmdRemote
                    .Parameters(0).Value = rstLocal(0).Value
                    .Parameters(1).Value = rstLocal(1).Value
                    .Parameters(2).Value = rstLocal(2).Value
                    .Parameters(3).Value = rstLocal(3).Value
                    .Parameters(4).Value = rstLocal(4).Value
                    .Parameters(5).Value = rstLocal(5).Value
                    .Parameters(6).Value = rstLocal(6).Value
                    oRemConn.BeginTrans
                    blInTrans = True
                    .Execute
                    cmdLocal.Parameters(0).Value = rstLocal(1).Value
                    cmdLocal.Parameters(1).Value = rstLocal(5).Value
                    cmdLocal.Execute
                    oRemConn.CommitTrans
                    blInTrans = False
                End With
                rstLocal.MoveNext
                ProgressBar1.Value = ProgressBar1.Value + 1
                ctlOperations.List(ctlOperations.ListIndex) = "Sending InspectionTrans..." & CInt(ProgressBar1.Value / ProgressBar1.Max * 100) & "%"
            Loop
        End With
        
    Else
    
        ctlOperations.List(ctlOperations.ListIndex) = "Sending InspectionTrans...n/a"
    
    End If
    
    oRemConn.Close
    oLocConn.Close
    
    Set rstLocal = Nothing
    ProgressBar1.Value = 0
        
    Exit Sub
    
err_Handler:

    If blInTrans = True Then
        oRemConn.RollbackTrans
        blInTrans = False
    End If

    If CheckConnection(RemoteConnection) = False Then
        ctlOperations.AddItem ("Connection Failed... Operation Cancelled")
        ctlOperations.ListIndex = ctlOperations.ListCount - 1
        DoEvents
        ctlClose.Enabled = True
        ConnectionFailed = True
    Else
        logGeneralError "frmRemoteServerConnected.General.SendInspectionTransInfo", CStr(Err.Number), Err.Description
    End If
    
End Sub

Private Sub CleanBufferData()

    Dim cmdRemote As New ADODB.Command
    
    On Error GoTo err_Handler
    
    oRemConn.Open
    
    cmdRemote.CommandText = "usp_RemoteDDC_CleanBufferData"
    cmdRemote.ActiveConnection = oRemConn
    cmdRemote.CommandType = adCmdStoredProc
    
    cmdRemote.Execute
    
    oRemConn.Close
    
    Exit Sub
    
err_Handler:

    If CheckConnection(RemoteConnection) = False Then
        ctlOperations.AddItem ("Connection Failed... Operation Cancelled")
        ctlOperations.ListIndex = ctlOperations.ListCount - 1
        DoEvents
        ctlClose.Enabled = True
        ConnectionFailed = True
    Else
        logGeneralError "frmRemoteServerConnected.General.CleanBufferData", CStr(Err.Number), Err.Description
    End If

End Sub

Private Sub RemoveBufferData()
    Dim cmdRemote As New ADODB.Command, paramRemote As New ADODB.Parameter, oEventID As Variant, oPartSN As Variant
    
    On Error GoTo err_Handler
    
    oRemConn.Open
    
    cmdRemote.CommandText = "usp_RemoteDDC_GetBufferEventID"
    cmdRemote.ActiveConnection = oRemConn
    cmdRemote.CommandType = adCmdStoredProc
    
    Set paramRemote = cmdRemote.CreateParameter
    paramRemote.Name = "@PartSN"
    paramRemote.Direction = adParamInput
    paramRemote.Type = adVarChar
    paramRemote.Size = 50
    cmdRemote.Parameters.Append paramRemote
    
    Set paramRemote = cmdRemote.CreateParameter
    paramRemote.Name = "@EventID"
    paramRemote.Direction = adParamOutput
    paramRemote.Type = adVarChar
    paramRemote.Size = 50
    cmdRemote.Parameters.Append paramRemote
      
    For Each oPartSN In PartSNs
        With cmdRemote
            .Parameters(0).Value = oPartSN
            .Execute
            oEventID = .Parameters(1).Value
            EventIDs.Add oEventID
        End With
    Next oPartSN

    Do While cmdRemote.Parameters.Count > 0
        cmdRemote.Parameters.Delete 0
    Loop
    
    cmdRemote.CommandText = "usp_RemoteDDC_RemoveBufferData"
    cmdRemote.ActiveConnection = oRemConn
    cmdRemote.CommandType = adCmdStoredProc
    Set paramRemote = cmdRemote.CreateParameter
    paramRemote.Name = "@EventID"
    paramRemote.Direction = adParamInput
    paramRemote.Type = adVarChar
    paramRemote.Size = 50
    cmdRemote.Parameters.Append paramRemote
    
    If EventIDs.Count > 0 Then
        ProgressBar1.Max = EventIDs.Count
    Else
        ProgressBar1.Max = 1
    End If
    
    For Each oEventID In EventIDs
        With cmdRemote
            .Parameters(0).Value = oEventID
            .Execute
        End With
        ProgressBar1.Value = ProgressBar1.Value + 1
        ctlOperations.List(ctlOperations.ListIndex) = "Removing Buffer Data from Server..." & CInt(ProgressBar1.Value / ProgressBar1.Max * 100) & "%"
    Next oEventID
    
    oRemConn.Close
    
    Set EventIDs = Nothing
    Set PartSNs = Nothing
    
    ProgressBar1.Value = 0
    
    Exit Sub
    
err_Handler:

    If CheckConnection(RemoteConnection) = False Then
        ctlOperations.AddItem ("Connection Failed... Operation Cancelled")
        ctlOperations.ListIndex = ctlOperations.ListCount - 1
        DoEvents
        ctlClose.Enabled = True
        ConnectionFailed = True
    Else
        logGeneralError "frmRemoteServerConnected.General.RemoveBufferData", CStr(Err.Number), Err.Description
    End If

End Sub

Private Sub Timer1_Timer()

    InitiateConnection RemoteConnection
    
    If ConnectionFailed = True Then Exit Sub
    
    InitiateConnection LocalConnection
    
    If ConnectionFailed = True Then Exit Sub
    
    If (CheckConnection(RemoteConnection) And CheckConnection(LocalConnection)) = True Then
        ctlOperations.AddItem ("Checking For Updates...")
        ctlOperations.ListIndex = ctlOperations.ListCount - 1
        DoEvents
        GetItemClasses
        GetParts
        GetDefectTypes
        GetDefectDescriptions
        GetInspectionPoints
        GetDispositions
        ctlOperations.AddItem ("Updates Complete")
        ctlOperations.ListIndex = ctlOperations.ListCount - 1
        ctlOperations.AddItem ("Checking For Data to Send...")
        ctlOperations.ListIndex = ctlOperations.ListCount - 1
        SendDefectMasterInfo
        SendDefectSubItemInfo
        SendInspectionTransInfo
        ctlOperations.AddItem ("All Data Sent")
        ctlOperations.ListIndex = ctlOperations.ListCount - 1
        ctlOperations.AddItem ("Cleansing Data on Server...")
        ctlOperations.ListIndex = ctlOperations.ListCount - 1
        CleanBufferData
        ctlOperations.AddItem ("Data Cleansing Complete")
        ctlOperations.ListIndex = ctlOperations.ListCount - 1
        ctlOperations.AddItem ("Removing Buffer Data from Server...")
        ctlOperations.ListIndex = ctlOperations.ListCount - 1
        RemoveBufferData
        ctlOperations.AddItem ("Data Removal Complete")
        ctlOperations.ListIndex = ctlOperations.ListCount - 1
    End If
    
    ctlClose.Enabled = True
    Command1.Enabled = True
    
    Timer1.Enabled = False

End Sub
