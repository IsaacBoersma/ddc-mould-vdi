Attribute VB_Name = "mdlErrors"
Public Sub logGeneralError(ByVal CalledBy As String, ByVal ErrorNum As String, ByVal ErrorDesc As String)
    
    App.StartLogging "", vbLogAuto
    App.LogEvent "Trapped: " & CalledBy & ", Error: " & ErrorNum & ", Description: " & ErrorDesc, vbLogEventTypeError
    
    If errLoggingStatus = True Then
        Dim eLog As New AppMessages.Logging
        eLog.StartLog App.Path & "\SVErrors.log", mLogToFile
        eLog.AddLogEvent "Trapped: " & CalledBy & ", Error: " & ErrorNum & ", Description: " & ErrorDesc, vbLogEventTypeError
        Set eLog = Nothing
    End If
    
    If ErrorNum = 3709 Then
        CheckConnectionStatus
    End If
    
End Sub

