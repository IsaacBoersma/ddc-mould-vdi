VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.2#0"; "MSCOMCTL.OCX"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{648A5603-2C6E-101B-82B6-000000000014}#1.1#0"; "MSCOMM32.OCX"
Begin VB.Form frmDefectEntry 
   Appearance      =   0  'Flat
   BorderStyle     =   1  'Fixed Single
   Caption         =   "DDCMould 1024"
   ClientHeight    =   11625
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   15315
   ForeColor       =   &H00000000&
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   12051.05
   ScaleMode       =   0  'User
   ScaleWidth      =   14819.42
   Begin MSComctlLib.ListView ctlDefects 
      Height          =   1815
      Left            =   120
      TabIndex        =   12
      Top             =   9240
      Width           =   11655
      _ExtentX        =   20558
      _ExtentY        =   3201
      LabelWrap       =   -1  'True
      HideSelection   =   -1  'True
      FullRowSelect   =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      NumItems        =   0
   End
   Begin VB.ListBox lsComponents 
      BackColor       =   &H00FFC0C0&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   14.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1380
      Left            =   3840
      TabIndex        =   85
      Top             =   9600
      Width           =   5055
   End
   Begin VB.CommandButton cmdProcess 
      Caption         =   "Submit"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   18
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   705
      Left            =   12480
      TabIndex        =   17
      Top             =   8760
      Width           =   1920
   End
   Begin VB.TextBox txQty 
      Alignment       =   2  'Center
      BackColor       =   &H00FFC0C0&
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   24
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000080&
      Height          =   675
      Left            =   6600
      TabIndex        =   83
      Text            =   "0"
      Top             =   10320
      Width           =   1065
   End
   Begin VB.CommandButton DefectButtons 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   915
      Index           =   0
      Left            =   120
      Style           =   1  'Graphical
      TabIndex        =   82
      Top             =   960
      Width           =   2025
   End
   Begin VB.CommandButton DefectButtons 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   915
      Index           =   7
      Left            =   120
      Style           =   1  'Graphical
      TabIndex        =   81
      Top             =   1944
      Width           =   2025
   End
   Begin VB.CommandButton DefectButtons 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   915
      Index           =   21
      Left            =   120
      Style           =   1  'Graphical
      TabIndex        =   80
      Top             =   3912
      Width           =   2025
   End
   Begin VB.CommandButton DefectButtons 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   915
      Index           =   14
      Left            =   120
      Style           =   1  'Graphical
      TabIndex        =   79
      Top             =   2928
      Width           =   2025
   End
   Begin VB.CommandButton DefectButtons 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   915
      Index           =   28
      Left            =   120
      Style           =   1  'Graphical
      TabIndex        =   78
      Top             =   4896
      Width           =   2025
   End
   Begin VB.CommandButton DefectButtons 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   915
      Index           =   35
      Left            =   120
      Style           =   1  'Graphical
      TabIndex        =   77
      Top             =   5880
      Width           =   2025
   End
   Begin VB.CommandButton DefectButtons 
      Caption         =   "LastButton(0-41)"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   915
      Index           =   41
      Left            =   13080
      Style           =   1  'Graphical
      TabIndex        =   76
      Top             =   5880
      Width           =   2025
   End
   Begin VB.CommandButton DefectButtons 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   915
      Index           =   40
      Left            =   10920
      Style           =   1  'Graphical
      TabIndex        =   75
      Top             =   5880
      Width           =   2025
   End
   Begin VB.CommandButton DefectButtons 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   915
      Index           =   39
      Left            =   8760
      Style           =   1  'Graphical
      TabIndex        =   74
      Top             =   5880
      Width           =   2025
   End
   Begin VB.CommandButton DefectButtons 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   915
      Index           =   38
      Left            =   6600
      Style           =   1  'Graphical
      TabIndex        =   73
      Top             =   5880
      Width           =   2025
   End
   Begin VB.CommandButton DefectButtons 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   915
      Index           =   37
      Left            =   4440
      Style           =   1  'Graphical
      TabIndex        =   72
      Top             =   5880
      Width           =   2025
   End
   Begin VB.CommandButton DefectButtons 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   915
      Index           =   36
      Left            =   2280
      Style           =   1  'Graphical
      TabIndex        =   71
      Top             =   5880
      Width           =   2025
   End
   Begin VB.CommandButton DefectButtons 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   915
      Index           =   34
      Left            =   13080
      Style           =   1  'Graphical
      TabIndex        =   70
      Top             =   4896
      Width           =   2025
   End
   Begin VB.CommandButton DefectButtons 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   915
      Index           =   33
      Left            =   10920
      Style           =   1  'Graphical
      TabIndex        =   69
      Top             =   4896
      Width           =   2025
   End
   Begin VB.CommandButton DefectButtons 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   915
      Index           =   32
      Left            =   8760
      Style           =   1  'Graphical
      TabIndex        =   68
      Top             =   4896
      Width           =   2025
   End
   Begin VB.CommandButton DefectButtons 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   915
      Index           =   31
      Left            =   6600
      Style           =   1  'Graphical
      TabIndex        =   67
      Top             =   4896
      Width           =   2025
   End
   Begin VB.CommandButton DefectButtons 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   915
      Index           =   30
      Left            =   4440
      Style           =   1  'Graphical
      TabIndex        =   66
      Top             =   4896
      Width           =   2025
   End
   Begin VB.CommandButton DefectButtons 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   915
      Index           =   29
      Left            =   2280
      Style           =   1  'Graphical
      TabIndex        =   65
      Top             =   4896
      Width           =   2025
   End
   Begin VB.CommandButton DefectButtons 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   915
      Index           =   26
      Left            =   10920
      Style           =   1  'Graphical
      TabIndex        =   64
      Top             =   3912
      Width           =   2025
   End
   Begin VB.CommandButton DefectButtons 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   915
      Index           =   27
      Left            =   13080
      Style           =   1  'Graphical
      TabIndex        =   63
      Top             =   3912
      Width           =   2025
   End
   Begin VB.CommandButton DefectButtons 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   915
      Index           =   25
      Left            =   8760
      Style           =   1  'Graphical
      TabIndex        =   62
      Top             =   3912
      Width           =   2025
   End
   Begin VB.CommandButton DefectButtons 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   915
      Index           =   16
      Left            =   4440
      Style           =   1  'Graphical
      TabIndex        =   61
      Top             =   2928
      Width           =   2025
   End
   Begin VB.CommandButton DefectButtons 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   915
      Index           =   17
      Left            =   6600
      Style           =   1  'Graphical
      TabIndex        =   60
      Top             =   2928
      Width           =   2025
   End
   Begin VB.CommandButton DefectButtons 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   915
      Index           =   18
      Left            =   8760
      Style           =   1  'Graphical
      TabIndex        =   59
      Top             =   2928
      Width           =   2025
   End
   Begin VB.CommandButton DefectButtons 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   915
      Index           =   19
      Left            =   10920
      Style           =   1  'Graphical
      TabIndex        =   58
      Top             =   2928
      Width           =   2025
   End
   Begin VB.CommandButton DefectButtons 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   915
      Index           =   15
      Left            =   2280
      Style           =   1  'Graphical
      TabIndex        =   57
      Top             =   2928
      Width           =   2025
   End
   Begin VB.CommandButton DefectButtons 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   915
      Index           =   24
      Left            =   6600
      Style           =   1  'Graphical
      TabIndex        =   56
      Top             =   3912
      Width           =   2025
   End
   Begin VB.CommandButton DefectButtons 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   915
      Index           =   2
      Left            =   4440
      Style           =   1  'Graphical
      TabIndex        =   54
      Top             =   960
      Width           =   2025
   End
   Begin VB.CommandButton DefectButtons 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   915
      Index           =   8
      Left            =   2280
      Style           =   1  'Graphical
      TabIndex        =   53
      Top             =   1944
      Width           =   2025
   End
   Begin VB.CommandButton DefectButtons 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   915
      Index           =   20
      Left            =   13080
      Style           =   1  'Graphical
      TabIndex        =   52
      Top             =   2928
      Width           =   2025
   End
   Begin VB.CommandButton DefectButtons 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   915
      Index           =   3
      Left            =   6600
      Style           =   1  'Graphical
      TabIndex        =   51
      Top             =   960
      Width           =   2025
   End
   Begin VB.CommandButton DefectButtons 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   915
      Index           =   9
      Left            =   4440
      Style           =   1  'Graphical
      TabIndex        =   50
      Top             =   1944
      Width           =   2025
   End
   Begin VB.CommandButton DefectButtons 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   915
      Index           =   23
      Left            =   4440
      Style           =   1  'Graphical
      TabIndex        =   49
      Top             =   3912
      Width           =   2025
   End
   Begin VB.CommandButton DefectButtons 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   915
      Index           =   22
      Left            =   2280
      Style           =   1  'Graphical
      TabIndex        =   48
      Top             =   3912
      Width           =   2025
   End
   Begin VB.CommandButton DefectButtons 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   915
      Index           =   13
      Left            =   13080
      Style           =   1  'Graphical
      TabIndex        =   47
      Top             =   1944
      Width           =   2025
   End
   Begin VB.CommandButton DefectButtons 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   915
      Index           =   12
      Left            =   10920
      Style           =   1  'Graphical
      TabIndex        =   46
      Top             =   1944
      Width           =   2025
   End
   Begin VB.CommandButton DefectButtons 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   915
      Index           =   11
      Left            =   8760
      Style           =   1  'Graphical
      TabIndex        =   45
      Top             =   1944
      Width           =   2025
   End
   Begin VB.CommandButton DefectButtons 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   915
      Index           =   10
      Left            =   6600
      Style           =   1  'Graphical
      TabIndex        =   44
      Top             =   1944
      Width           =   2025
   End
   Begin VB.CommandButton DefectButtons 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   915
      Index           =   6
      Left            =   13080
      Style           =   1  'Graphical
      TabIndex        =   43
      Top             =   960
      Width           =   2025
   End
   Begin VB.CommandButton DefectButtons 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   915
      Index           =   5
      Left            =   10920
      Style           =   1  'Graphical
      TabIndex        =   42
      Top             =   960
      Width           =   2025
   End
   Begin VB.CommandButton DefectButtons 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   915
      Index           =   4
      Left            =   8760
      Style           =   1  'Graphical
      TabIndex        =   41
      Top             =   960
      Width           =   2025
   End
   Begin VB.CommandButton DefectButtons 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   915
      Index           =   1
      Left            =   2280
      Style           =   1  'Graphical
      TabIndex        =   40
      Top             =   960
      Width           =   2025
   End
   Begin MSComCtl2.DTPicker ctlTime 
      Height          =   375
      Left            =   1080
      TabIndex        =   39
      Top             =   2850
      Visible         =   0   'False
      Width           =   1935
      _ExtentX        =   3413
      _ExtentY        =   661
      _Version        =   393216
      Format          =   101711874
      CurrentDate     =   38797
   End
   Begin VB.CommandButton ctlDateTimeSet 
      Caption         =   "Set"
      Height          =   375
      Left            =   3045
      TabIndex        =   38
      Top             =   2850
      Visible         =   0   'False
      Width           =   735
   End
   Begin VB.CommandButton Command3 
      Caption         =   "..."
      Enabled         =   0   'False
      Height          =   315
      Left            =   3480
      TabIndex        =   37
      Top             =   120
      Width           =   375
   End
   Begin VB.TextBox ctlEventDateTime 
      Enabled         =   0   'False
      Height          =   315
      Left            =   1080
      TabIndex        =   36
      TabStop         =   0   'False
      Top             =   120
      Width           =   2415
   End
   Begin MSComCtl2.MonthView ctlDateSelect 
      Height          =   2370
      Left            =   1200
      TabIndex        =   35
      Top             =   600
      Visible         =   0   'False
      Width           =   2700
      _ExtentX        =   4763
      _ExtentY        =   4180
      _Version        =   393216
      ForeColor       =   -2147483630
      BackColor       =   -2147483633
      Appearance      =   1
      StartOfWeek     =   101711873
      CurrentDate     =   38797
   End
   Begin MSCommLib.MSComm MSComm1 
      Left            =   7800
      Top             =   7680
      _ExtentX        =   1005
      _ExtentY        =   1005
      _Version        =   393216
      DTREnable       =   -1  'True
   End
   Begin VB.ListBox ctllbDisposition 
      BeginProperty Font 
         Name            =   "Book Antiqua"
         Size            =   15.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FF0000&
      Height          =   2010
      ItemData        =   "frmDefectEntry.frx":0000
      Left            =   7080
      List            =   "frmDefectEntry.frx":0002
      TabIndex        =   32
      Top             =   7080
      Width           =   3495
   End
   Begin VB.CommandButton cmdReset 
      BackColor       =   &H8000000B&
      Caption         =   "Reset"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   18
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   705
      Left            =   12480
      TabIndex        =   18
      Top             =   7800
      Width           =   1920
   End
   Begin MSComCtl2.UpDown UpDown1 
      Height          =   675
      Left            =   14809
      TabIndex        =   30
      Top             =   120
      Width           =   255
      _ExtentX        =   450
      _ExtentY        =   1191
      _Version        =   393216
      Value           =   1
      AutoBuddy       =   -1  'True
      BuddyControl    =   "ctlLabelCount"
      BuddyDispid     =   196618
      OrigLeft        =   12720
      OrigTop         =   600
      OrigRight       =   12975
      OrigBottom      =   1215
      Max             =   3
      Min             =   1
      SyncBuddy       =   -1  'True
      BuddyProperty   =   65547
      Enabled         =   -1  'True
   End
   Begin VB.TextBox ctlLabelCount 
      Alignment       =   2  'Center
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   24
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   674
      Left            =   14159
      TabIndex        =   29
      Text            =   "1"
      Top             =   120
      Width           =   638
   End
   Begin VB.ListBox ctlDefectName 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1260
      Left            =   7440
      TabIndex        =   28
      Top             =   7200
      Visible         =   0   'False
      Width           =   2655
   End
   Begin VB.ListBox ctlDefectType 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   960
      Left            =   7080
      TabIndex        =   27
      Top             =   8040
      Visible         =   0   'False
      Width           =   3135
   End
   Begin MSComctlLib.StatusBar ctlStatBar 
      Align           =   2  'Align Bottom
      Height          =   150
      Left            =   0
      TabIndex        =   26
      Top             =   11475
      Width           =   15315
      _ExtentX        =   27014
      _ExtentY        =   265
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   1
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
         EndProperty
      EndProperty
   End
   Begin VB.CommandButton Command1 
      Caption         =   "Command1"
      Height          =   375
      Left            =   7440
      TabIndex        =   24
      Top             =   8520
      Visible         =   0   'False
      Width           =   975
   End
   Begin VB.Timer ctlTimerConn 
      Left            =   7440
      Top             =   7920
   End
   Begin VB.TextBox ctlLotNum 
      Height          =   315
      Left            =   7800
      TabIndex        =   3
      Top             =   9720
      Visible         =   0   'False
      Width           =   1695
   End
   Begin VB.CheckBox ctlLocked 
      Caption         =   "Locked"
      Height          =   255
      Left            =   11640
      TabIndex        =   21
      TabStop         =   0   'False
      Top             =   120
      Width           =   855
   End
   Begin VB.TextBox ctlInspectionPointPre 
      BackColor       =   &H8000000F&
      Enabled         =   0   'False
      Height          =   315
      Left            =   6000
      TabIndex        =   19
      Top             =   120
      Width           =   1815
   End
   Begin VB.CommandButton cmdRemove 
      Caption         =   "Remove Defect"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   675
      Left            =   11760
      TabIndex        =   13
      TabStop         =   0   'False
      Top             =   10200
      Width           =   1545
   End
   Begin VB.ComboBox ctlInspectionPoint 
      Height          =   315
      Left            =   9360
      Style           =   2  'Dropdown List
      TabIndex        =   4
      Top             =   120
      Width           =   2055
   End
   Begin VB.TextBox ctlPartNumber 
      Enabled         =   0   'False
      Height          =   315
      Left            =   1080
      TabIndex        =   10
      TabStop         =   0   'False
      Top             =   600
      Width           =   2415
   End
   Begin VB.ComboBox ctlPartColour 
      Height          =   315
      Left            =   7320
      Style           =   2  'Dropdown List
      TabIndex        =   2
      Top             =   600
      Width           =   2775
   End
   Begin VB.ComboBox ctlItemClass 
      Height          =   315
      Left            =   3600
      Style           =   2  'Dropdown List
      TabIndex        =   1
      Top             =   600
      Width           =   2895
   End
   Begin VB.TextBox ctlPartSN 
      Alignment       =   2  'Center
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00C00000&
      Height          =   360
      Left            =   11640
      TabIndex        =   0
      Top             =   480
      Width           =   1575
   End
   Begin VB.Timer ctlTimer 
      Left            =   7440
      Top             =   7560
   End
   Begin VB.CommandButton ctlScan 
      Appearance      =   0  'Flat
      Caption         =   "Scan"
      Height          =   315
      Left            =   10320
      TabIndex        =   23
      Top             =   9600
      Width           =   555
   End
   Begin VB.CommandButton ctlDontScan 
      Caption         =   "Dont"
      Height          =   315
      Left            =   9600
      TabIndex        =   34
      Top             =   9720
      Width           =   555
   End
   Begin VB.TextBox txDontScan 
      Height          =   375
      Left            =   8280
      TabIndex        =   55
      Top             =   9360
      Width           =   1575
   End
   Begin MSComCtl2.UpDown UpDown2 
      Height          =   675
      Left            =   7665
      TabIndex        =   84
      Top             =   10320
      Width           =   255
      _ExtentX        =   450
      _ExtentY        =   1191
      _Version        =   393216
      BuddyControl    =   "txQty"
      BuddyDispid     =   196611
      OrigLeft        =   12720
      OrigTop         =   600
      OrigRight       =   12975
      OrigBottom      =   1215
      Max             =   999
      SyncBuddy       =   -1  'True
      BuddyProperty   =   0
      Enabled         =   -1  'True
   End
   Begin VB.Label lbInspect 
      Alignment       =   2  'Center
      BackColor       =   &H0080C0FF&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Inspect"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   15.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FF0000&
      Height          =   615
      Left            =   13680
      TabIndex        =   89
      Top             =   10200
      Width           =   1335
   End
   Begin VB.Label lbRefresh 
      BackColor       =   &H00FFC0C0&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Refresh List"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00004000&
      Height          =   615
      Left            =   8640
      TabIndex        =   88
      Top             =   10320
      Width           =   975
   End
   Begin VB.Label lbClose 
      Alignment       =   2  'Center
      BackColor       =   &H00FFFFC0&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Close Application"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   18
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   615
      Left            =   11880
      TabIndex        =   87
      Top             =   6960
      Width           =   3255
   End
   Begin VB.Label lbProcessComponents 
      BackColor       =   &H00FFC0C0&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Process Components"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000080&
      Height          =   615
      Left            =   9840
      TabIndex        =   86
      Top             =   10320
      Width           =   1455
   End
   Begin VB.Shape Shape1 
      Height          =   924
      Left            =   0
      Top             =   0
      Width           =   23865
   End
   Begin VB.Label Label13 
      Caption         =   "Disposition:"
      ForeColor       =   &H00FF0000&
      Height          =   255
      Left            =   7200
      TabIndex        =   33
      Top             =   6840
      Width           =   975
   End
   Begin VB.Label Label2 
      Alignment       =   2  'Center
      Caption         =   "Label Count"
      Height          =   375
      Left            =   13680
      TabIndex        =   31
      Top             =   360
      Width           =   495
   End
   Begin VB.Label ctlNoScan 
      Caption         =   "No Scan"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   24
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   615
      Left            =   2400
      TabIndex        =   25
      Top             =   7920
      Visible         =   0   'False
      Width           =   2055
   End
   Begin VB.Label Label12 
      Caption         =   "Lot Number:"
      Height          =   255
      Left            =   13680
      TabIndex        =   22
      Top             =   7200
      Width           =   975
   End
   Begin VB.Label Label11 
      Caption         =   "Previous Inspection Point:"
      Height          =   255
      Left            =   4080
      TabIndex        =   20
      Top             =   165
      Width           =   1935
   End
   Begin VB.Label Label10 
      Caption         =   " Defect Information"
      Height          =   255
      Left            =   8640
      TabIndex        =   16
      Top             =   8640
      Visible         =   0   'False
      Width           =   1455
   End
   Begin VB.Shape ctlDot 
      BorderColor     =   &H000000C0&
      FillColor       =   &H000000FF&
      FillStyle       =   0  'Solid
      Height          =   60
      Index           =   0
      Left            =   13560
      Shape           =   3  'Circle
      Top             =   7440
      Visible         =   0   'False
      Width           =   60
   End
   Begin VB.Label Label8 
      Caption         =   "Defect Type:"
      Height          =   255
      Left            =   7920
      TabIndex        =   15
      Top             =   7320
      Visible         =   0   'False
      Width           =   975
   End
   Begin VB.Label Label9 
      Caption         =   "Defect Name:"
      Height          =   255
      Left            =   8160
      TabIndex        =   14
      Top             =   7320
      Visible         =   0   'False
      Width           =   1095
   End
   Begin VB.Image ctlImage 
      Appearance      =   0  'Flat
      BorderStyle     =   1  'Fixed Single
      Height          =   2340
      Left            =   120
      Top             =   6840
      Width           =   6915
   End
   Begin VB.Label Label7 
      Caption         =   "Inspection Point:"
      Height          =   255
      Left            =   8040
      TabIndex        =   11
      Top             =   165
      Width           =   1215
   End
   Begin VB.Label Label6 
      Caption         =   "Part Number:"
      Height          =   255
      Left            =   120
      TabIndex        =   9
      Top             =   600
      Width           =   975
   End
   Begin VB.Label Label5 
      Caption         =   "Part Style:"
      Height          =   255
      Left            =   6600
      TabIndex        =   8
      Top             =   645
      Width           =   735
   End
   Begin VB.Label Label4 
      Caption         =   "Tool:"
      Height          =   255
      Left            =   3120
      TabIndex        =   7
      Top             =   645
      Width           =   375
   End
   Begin VB.Label Label3 
      Caption         =   "Part Serial Number:"
      Height          =   255
      Left            =   10200
      TabIndex        =   6
      Top             =   600
      Width           =   1455
   End
   Begin VB.Label Label1 
      Caption         =   "Date / Time:"
      Height          =   255
      Left            =   120
      TabIndex        =   5
      Top             =   165
      Width           =   975
   End
End
Attribute VB_Name = "frmDefectEntry"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public ctlComputername As String
Public InspDefault As String

Private oParts As New Parts
Private DefMax As Integer
Private oDefParent As New DefectParents
Private oDefChildren As New DefectChildren
Private strScan As String
Private BackOut As Boolean
Private intStart As Integer
Private intLength As Integer
Private tempIndex As Integer
Private pressDesc As String
Private newScan As Boolean
Private pressNum As Integer
'Private machineID As Integer
Private Declare Function FindWindow Lib "user32" Alias "FindWindowA" (ByVal lpClassName As String, ByVal lpWindowName As String) As Long
Private Declare Function SendMessage Lib "USER32.DLL" Alias "SendMessageA" (ByVal hwnd As Long, ByVal uMsg As Long, ByVal wParam As Long, ByVal lParam As Long) As Long
Private Const WM_CLOSE = &H10

Public machineID As Integer, pressID As Integer, pressSetupFolder As String, InspectionPoint As String
Public ComputerName As String, DefectProfile As String, appName As String, ImagePath As String
Public DispositionOverriding As String, LockEnabled As Integer, ServerName As String
Public DatabaseName As String, PartsIDConnStr As String, DDCMouldConnStr As String, loginName As String

Private Sub gtChildrenDefects()
    Dim ListItem As ListItem
    
    On Error GoTo Err_Handler
    
    Set oDefChildren = Nothing
    For Each ListItem In ctlDefects.ListItems
        If ListItem.SubItems(itmNew) = "Yes" Then
            With oDefChildren.Add
                .DefectDateTime = ListItem.SubItems(itmDefectDateTime)
                .DefectID = ListItem.SubItems(itmDefectID)
                .DefectDescriptionID = ListItem.SubItems(itmDefectDescriptionID)
                .EventID = ListItem.SubItems(itmEventID)
                .IPID = ListItem.SubItems(itmIPID)
                .Xpos = ListItem.SubItems(itmXpos)
                .Ypos = ListItem.SubItems(itmYpos)
            End With
        End If
    Next ListItem
    
    'Dim str As String
    'str = CStr(oDefChildren(1).IPID)
    
    Exit Sub
    
Err_Handler:
    logGeneralError "frmDefectEntry.General.gtChildrenDefects", CStr(Err.Number), Err.Description

End Sub

Private Function gtPartColour(ByVal pnum As String) As String
    Dim oPartItem As Part
    
    On Error GoTo Err_Handler
    
    For Each oPartItem In oParts
        If oPartItem.PartNumber = pnum Then
            gtPartColour = oPartItem.ColourDescription
            Exit Function
        End If
    Next oPartItem
    
    Exit Function
    
Err_Handler:
    logGeneralError "frmDefectEntry.General.gtPartColour", CStr(Err.Number), Err.Description
    
End Function

Private Sub ldDefParent()
    Dim strsql As String, rst As New ADODB.Recordset, itmDesc As String, PrtDesc As String
    Dim PrtInf As Variant, strDefProf As String
    
    On Error GoTo Err_Connect
'IN THE BEGINNING, THERE WON'T BE ANY DEFECT TO LOAD, THUS EMPTY STRING RETURNED
'    If InspDefault = "Moulding" Then
'    strsql = "SELECT DefMaster.*, InspTrans.ToIPID AS IPID,InspPoints.InspectionPoint," _
'        & "InspTrans.DispositionID,Disp.DispositionName AS Disposition,DefMaster.LabelCount " _
'        & "FROM tblInspectionPoints InspPoints INNER JOIN tblDefectMaster DefMaster INNER JOIN " _
'        & "tblInspectionTrans InspTrans ON DefMaster.EventID = InspTrans.EventID ON InspPoints.IPID = InspTrans.ToIPID " _
'        & "INNER JOIN tblDispositions Disp ON InspTrans.DispositionID = Disp.DispositionID WHERE " _
'        & "DefMaster.PartSN = " & ctlPartSN & " ORDER BY InspTrans.TransactionDateTime DESC"
'    Else
'IF THERE IS NO DEFECT, THE FOLLOWING RETURNS NO ENTRY
    strsql = "SELECT dm.*, it.ToIPID AS IPID, " _
            & "ip.InspectionPoint, it.DispositionID, " _
            & "d.DispositionName AS Disposition, dm.LabelCount FROM " _
            & "tblInspectionPoints ip INNER JOIN tblDefectMaster dm INNER JOIN " _
            & "tblInspectionTrans it ON dm.EventID = it.EventID " _
            & "ON ip.IPID = it.ToIPID INNER JOIN " _
            & "tblDispositions d ON it.DispositionID = " _
            & "d.DispositionID WHERE (dm.PartSN = '" & ctlPartSN & "') " _
            & "ORDER BY it.TransactionDateTime DESC"
    rst.Open strsql, connCurrent
'    End If
    
    newScan = True
    
    If Not (rst.BOF And rst.EOF) = True Then
        newScan = False
        itmDesc = GetDescriptions(rst("ItemClass"), rst("PartNumber"))(0)
        PrtDesc = GetDescriptions(rst("ItemClass"), rst("PartNumber"))(1)
    
        With oDefParent(1)
            .Comment = rst("Comment")
            
            
''''''''''''MASK OUT ON 02-NOV-2014
'            If ctllbDisposition <> "Ok" And OverrideOK = 0 Then
'                ctllbDisposition.Enabled = False
'            End If
            
            
            ctlLabelCount = IIf(IsNull(rst("LabelCount")), 1, rst("LabelCount"))
            .EventDateTime = rst("EventDateTime")
            ctlEventDateTime = .EventDateTime
            .EventID = rst("EventID")
            ctlTimer.Enabled = False
            ctlInspectionPointPre = rst("InspectionPoint")
            ctlInspectionPointPre.Tag = rst("IPID")
            .ItemClass = rst("ItemClass")
            ctlItemClass_DropDown
'            ctlItemClass = itmDesc

            If Not itmDesc = "" Then
                Dim idx As Integer, oItem As ItemClass
                For Each oItem In ItemLst
                    If oItem.ItemClass = oDefParent(1).ItemClass Then
                        idx = oItem.ItemClassIDX
                        Exit For
                    End If
                Next oItem
                
                ctlItemClass.ListIndex = idx
                
            End If
            
            ctllbDisposition = rst("Disposition")

            .LotNum = rst("LotNum")
            ctlLotNum = .LotNum
            ctlPartColour_DropDown
            ctlPartColour = PrtDesc
            .PartNumber = rst("PartNumber")
            ctlPartNumber = .PartNumber
            .PartSN = rst("PartSN")
        End With
        ctlItemClass.Enabled = False
        ctlPartColour.Enabled = False
        ctlLotNum.Enabled = False
        'WHEN EOB AND EOF=TRUE
        Else:
            If ClientIsRemote = False Then
                ctlItemClass.Enabled = True
                ctlPartColour.Enabled = True
                ctlLotNum.Enabled = True
                'ctllbDisposition = "Ok"
                ctllbDisposition = "Regrind"
                
                rst.Close
                
                Dim adocmd As New ADODB.Command
                Dim oParam As New ADODB.Parameter
                
                adocmd.ActiveConnection = connCurrent
                adocmd.CommandType = adCmdStoredProc
                
                'CALL STORED PROCEDURE TO GET INFO BASED ON SCANNED PARTSN
                'strDefProf = GetSettingString(HKEY_LOCAL_MACHINE, SV_KEY_ROOT & "\" & SV_KEY_PRFDATA, SV_VAL_DEFPROF, "")
                strDefProf = DefectProfile
                
                If strDefProf = "IMM" Then
                    adocmd.CommandText = "sp_GetPartSNScanInfo"
                Else
                    adocmd.CommandText = "sp_GetPartSNFromScan"
                End If
                
                Set oParam = adocmd.CreateParameter("@PartSN", adVarChar, adParamInput, 10, ctlPartSN)
                adocmd.Parameters.Append oParam
                
                Set rst = adocmd.Execute
                
                If Not (rst.BOF And rst.EOF) Then
                    With oDefParent(1)
                        .ItemClass = rst("ITEM_CLASS")
                        .LotNum = rst("PROCESS_LOT_ID")
                        ctlLotNum = .LotNum
                        .PartNumber = rst("PART_NUMBER")
                        ctlPartNumber = .PartNumber
                        'the following line return says the part is obsolete when it is not
                        PrtInf = GetDescriptions(.ItemClass, .PartNumber)
                        itmDesc = PrtInf(0)
                        PrtDesc = PrtInf(1)
                        If itmDesc = "" Or PrtDesc = "" Then
                            BackOut = True
                            Exit Sub
                        End If
                        .LabelCount = ctlLabelCount.Text
                    rst.Close
                                    
                    End With
                    
                        ctlItemClass_DropDown
                        
                        If Not itmDesc = "" Then
                            Dim idx1 As Integer
                            For Each oItem In ItemLst
                                If oItem.ItemClass = oDefParent(1).ItemClass Then
                                    idx1 = oItem.ItemClassIDX
                                    Exit For
                                End If
                            Next oItem
                            
                            ctlItemClass.ListIndex = idx1
                            
                        End If
                        
                        
                        ctlPartColour_DropDown
                        If Not ctlPartNumber = "" And Not PrtDesc = "" Then
                            ctlPartColour = PrtDesc
                        End If
                End If
            Else
                SetDropDowns ctlPartNumber.Text
            End If
    End If
    
    Set adocmd = Nothing
    Set rst = Nothing
    Set oParam = Nothing
    Exit Sub
    
Err_Connect:
    logGeneralError "frmDefectEntry.General.ldDefParent", CStr(Err.Number), Err.Description
    Set adocmd = Nothing
    Set rst = Nothing
    Set oParam = Nothing
    
    If Not (Err.Number = 13) Then
        If connState = 2 Then Resume
    End If
    
End Sub

Private Sub SetDropDowns(ByVal PartNum As String)
    Dim cmdItemClass As New ADODB.Command, oParam As New ADODB.Parameter, oItemClassItem As New ItemClass, strItemClass As String
    Dim oPartItem As New Part
    
    ctlItemClass_DropDown
    
    cmdItemClass.ActiveConnection = connCurrent
    cmdItemClass.CommandType = adCmdStoredProc
    cmdItemClass.CommandText = "usp_GetItemClass"
    Set oParam = cmdItemClass.CreateParameter("@PartNumber", adVarChar, adParamInput, 50, PartNum)
    cmdItemClass.Parameters.Append oParam
    Set oParam = cmdItemClass.CreateParameter("@ItemClass", adVarChar, adParamOutput, 50)
    cmdItemClass.Parameters.Append oParam
    
    cmdItemClass.Execute
    strItemClass = cmdItemClass.Parameters("@ItemClass").Value
    
    If strItemClass = "NotFound" Then
        Exit Sub
    End If
    
    For Each oItemClassItem In ItemLst
        If oItemClassItem.ItemClass = strItemClass Then
            ctlItemClass.ListIndex = oItemClassItem.ItemClassIDX
            Exit For
        End If
    Next oItemClassItem
    
    ctlPartColour_DropDown
    
    For Each oPartItem In oParts
        If oPartItem.PartNumber = PartNum Then
            ctlPartColour.ListIndex = oPartItem.PartIDX
            Exit For
        End If
    Next oPartItem
    
    Set cmdItemClass = Nothing
    Set oParam = Nothing
    
End Sub

Private Function GetDescriptions(ByVal ItemClass As String, ByVal PartNumber As String) As Variant
    Dim PartItm As Part, ItemClassItm As ItemClass, GtG As Boolean
    
    On Error GoTo Err_Handler
    
    For Each PartItm In PartLst
        If PartNumber = PartItm.PartNumber Then
            For Each ItemClassItm In ItemLst
                If PartItm.ItemClass = ItemClassItm.ItemClass Then
                    GtG = True
                    Exit For
                End If
            Next ItemClassItm
        End If
        If GtG = True Then Exit For
    Next PartItm
    If GtG = True Then
        GetDescriptions = Array(ItemClassItm.ItemDescription, PartItm.ColourDescription)
    Else
        MsgBox "The part label scanned may be obsolete." & _
        vbCrLf & "Please ensure a current part number is on the label." & _
        vbCrLf & "A new label may be required.", vbOKOnly, "Label Problem"
        GetDescriptions = Array("", "")
    End If
    
    Exit Function
    
Err_Handler:
    logGeneralError "frmDefectEntry.General.GetDescriptions", CStr(Err.Number), Err.Description

End Function

Private Sub ldDefChildren()
    Dim strsql As String, rst As New ADODB.Recordset, litm As ListItem
    
    On Error GoTo Err_Connect
    
    DefMax = 0
    strsql = "SELECT ds.*, dd.DefectDescription AS " _
            & "DefectName, dt.DefectType, ip.InspectionPoint " _
            & "FROM (tblDefectMaster dm INNER JOIN tblDefectSubItems ds ON dm.EventID=ds.EventID " _
            & "INNER JOIN tblInspectionPoints ip ON " _
            & "ds.IPID = ip.IPID) INNER JOIN " _
            & "(tblDefectTypes dt INNER JOIN tblDefectDescriptions dd ON " _
            & "dt.DefectTypeID = dd.DefectTypeID) ON " _
            & "ds.DefectDescriptionID = dd.DefectDescriptionID " _
            & "WHERE (((dm.EventID)='" & oDefParent(1).EventID _
            & "')) ORDER BY ds.DefectDateTime;"
    rst.Open strsql, connCurrent
    
    newScan = True
    If Not (rst.BOF And rst.EOF) = True Then
        newScan = False
        Do Until rst.EOF
            Load ctlDot(DefMax + 1)
            ctlDot(DefMax + 1).Top = ctlImage.Top + rst("Ypos") - 30
            ctlDot(DefMax + 1).Left = ctlImage.Left + rst("Xpos") - 30
            ctlDot(DefMax + 1).ZOrder
            ctlDot(DefMax + 1).Visible = True
            Set litm = ctlDefects.ListItems.Add(, , DefMax + 1)
            litm.SubItems(itmDefectDateTime) = rst("DefectDateTime")
            litm.SubItems(itmDefectName) = rst("DefectName")
            litm.SubItems(itmDefectID) = rst("DefectID")
            litm.SubItems(itmDefectType) = rst("DefectType")
            litm.SubItems(itmEventID) = rst("EventID")
            litm.SubItems(itmInspectionPoint) = rst("InspectionPoint")
            litm.SubItems(itmNew) = "No"
            litm.SubItems(itmXpos) = rst("Xpos")
            litm.SubItems(itmYpos) = rst("Ypos")
            litm.SubItems(itmDefectDescriptionID) = rst("DefectDescriptionID")
            litm.SubItems(itmIPID) = rst("IPID")
            litm.Tag = DefMax + 1
            DefMax = DefMax + 1
            rst.MoveNext
        Loop
    End If
    
    Set rst = Nothing
    Exit Sub
    
Err_Connect:
    logGeneralError "frmDefectEntry.General.ldDefChildren", CStr(Err.Number), Err.Description
    Set rst = Nothing
    If connState = 2 Then Resume
    
End Sub

'Public Sub stInspectionPoints()
'    Dim rst As New ADODB.Recordset, strsql As String, lstind As Long
'
'    On Error GoTo err_connect
'
'    If Not ctlLocked = 1 Then
'        ctlInspectionPoint.Clear
'        strsql = "SELECT * FROM tblInspectionPoints ORDER BY IPID"
'        rst.Open strsql, connCurrent
'        Do Until rst.EOF
'            With frmDefectEntry.ctlInspectionPoint
'                If Not rst("IPID") = 0 Then
'                    .AddItem rst("InspectionPoint"), lstind
'                    .ItemData(lstind) = rst("IPID")
'                    lstind = lstind + 1
'                End If
'            End With
'            rst.MoveNext
'        Loop
'    End If
'    Exit Sub
'
'err_connect:
'
'    logGeneralError "frmDefectEntry.General.stInspectionPoints", CStr(Err.Number), Err.Description
'    If connState = 2 Then Resume
'
'End Sub
'Private Sub FillctllbDisposition()
'    Dim rst As New ADODB.Recordset, strsql As String
'
'    On Error GoTo err_Handler
'
'    strsql = "SELECT * FROM tblDispositions ORDER BY DispositionID"
'
'    rst.Open strsql, connCurrent
'
'    Do Until rst.EOF
'        With frmDefectEntry.ctllbDisposition
'            .AddItem rst("DispositionName")
'            .ItemData(ctllbDisposition.NewIndex) = rst("DispositionID")
'        End With
'        rst.MoveNext
'    Loop
'
'    Exit Sub
'
'err_Handler:
'
'    logGeneralError "frmDefectEntry.General.FillctllbDisposition", CStr(Err.Number), Err.Description
'
'End Sub
Private Sub clrDots()
    Dim cntr As Integer
    On Error Resume Next
    For cntr = 1 To ctlDot().UBound
        Unload ctlDot(cntr)
    Next cntr
End Sub

Private Sub clrFrm(ByVal clrall As Boolean, Optional ByVal ResetBtn As Boolean)
    
    On Error GoTo Err_Handler
    
    Set oDefParent = Nothing
    Set oDefChildren = Nothing
    oDefParent.Add GetGUID
    If ctlLocked = False Then
        ctlInspectionPoint.Clear
    End If
    ctlInspectionPointPre = ""
    ctlInspectionPointPre.Tag = 0
    ctlLotNum = ""
'    ctlInspectionPoint_DropDown
    ctlItemClass.Clear
    ctlItemClass_DropDown
    ctlPartColour.Clear
    ctlPartColour_DropDown
    If ClientIsRemote = False Then
        ctlPartNumber = ""
    End If
    If ResetBtn = True Then
        ctlPartNumber = ""
    End If
    ctllbDisposition.ListIndex = -1
    ctlDefectName.Clear
    ctlDefects.ListItems.Clear
    clrDots
    ctlItemClass.Enabled = True
    ctlPartColour.Enabled = True
    'commented out on 01-Apr-2008
    ctlTimer.Enabled = True
    ctlImage.Picture = Nothing
    ctlLabelCount = 1
    
    DefectButtons(tempIndex).BackColor = &H8000000F
    tempIndex = 0
    
    Select Case clrall
        Case True
            ctlPartSN = ""
            ctlPartSN.SetFocus
            'ctlScan.SetFocus
        Case False
            'ctlPartSN.SetFocus
            ctlScan.SetFocus
    End Select
    
    Exit Sub
    
Err_Handler:
    logGeneralError "frmDefectEntry.General.clrFrm", CStr(Err.Number), Err.Description

End Sub

Private Sub UpdateDefectData()
    Dim Item As DefectChild
    Dim strsql As String, rst As New ADODB.Recordset
    Dim adocmd As New ADODB.Command
    Dim cnt As Integer
    Dim tempPressID As Integer
    Dim productionID As Double
    Dim toolID As Double
    
    On Error GoTo Err_Connect
    cnt = 0
    productionID = 0
    toolID = 0
    
    oDefParent(1).EventDateTime = ctlEventDateTime.Text
    oDefParent(1).LotNum = ctlLotNum
    oDefParent(1).LabelCount = ctlLabelCount
    oDefParent(1).pressDesc = ctlInspectionPoint.Text
'Since this is update defect, you want the current InspectionPoint, not the previous defect inspectionpoint!!!
'similarly, you need to get PressID for new defect
    If mouldPress = 1 Then
        'oDefParent(1).pressID = Right(ctlInspectionPoint.Text, 2)
        oDefParent(1).pressID = getPressIDfromInspPoint(ctlInspectionPoint.Text)
        
        Call getPressProdToolfromSN(UCase(ctlPartSN.Text), tempPressID, productionID, toolID)
    Else
        'oDefParent(1).pressID = getPressIDfromSN(UCase(ctlPartSN.Text))
        Call getPressProdToolfromSN(UCase(ctlPartSN.Text), tempPressID, productionID, toolID)
    End If
    
    cnt = 1
    If ctlEval = True Then
        With oDefParent(1)
            cnt = 2
            strsql = "SELECT EventID FROM tblDefectMaster WHERE EventID = '" & .EventID & "'"
            rst.Open strsql, connCurrent
            cnt = 3
            If (rst.EOF And rst.BOF) = True Then
'                strsql = "INSERT INTO tblDefectMaster (EventID, EventDateTime, " _
'                        & "LotNum, PartSN, ItemClass, PartNumber, Comment, LabelCount, PressID, pressDesc, Flag) " _
'                        & "VALUES ('" & .EventID & "', '" & .EventDateTime & "', '" _
'                        & .LotNum & "', '" & .PartSN & "', '" & .ItemClass & "', '" _
'                        & .PartNumber & "', '" & .Comment & "', " & .LabelCount & ", " & .pressID & ", '" & .pressDesc & "', " & .Flag & ")"
                
                'ADD PRODUCTIONID, TOOLID 02-JUN-2013 JJ
                strsql = "INSERT INTO tblDefectMaster (EventID, EventDateTime, " _
                        & "LotNum, PartSN, ItemClass, PartNumber, Comment, LabelCount, PressID, pressDesc, Flag, ProductionID, ToolID) " _
                        & "VALUES ('" & .EventID & "', '" & .EventDateTime & "', '" _
                        & .LotNum & "', '" & .PartSN & "', '" & .ItemClass & "', '" _
                        & .PartNumber & "', '" & .Comment & "', " & .LabelCount & ", " & tempPressID & ", '" & .pressDesc & "', " & .Flag & "," & productionID & "," & toolID & ")"
                If rst.State = adStateOpen Then rst.Close
                cnt = 4
                rst.Open strsql, connCurrent
                cnt = 5
            Else
                cnt = 6
'                adocmd.ActiveConnection = connCurrent
'                adocmd.CommandType = adCmdText
'                adocmd.CommandText = "UPDATE tblDefectMaster SET LabelCount = " & .LabelCount & " WHERE EventID = '" & .EventID & "'"
'                adocmd.Execute
'                Set adocmd = Nothing
'ORIGINAL
                strsql = "UPDATE tblDefectMaster SET LabelCount = " & .LabelCount & " WHERE " _
                        & "EventID = '" & .EventID & "'"
                If rst.State = adStateOpen Then rst.Close
                rst.Open strsql, connCurrent
                cnt = 7
            End If
        End With
        If Not ctlDefects.ListItems.Count = 0 Then
            cnt = 8
            gtChildrenDefects
            cnt = 9
            For Each Item In oDefChildren
                With Item
                adocmd.ActiveConnection = connCurrent
                adocmd.CommandType = adCmdText
                adocmd.CommandText = "INSERT INTO tblDefectSubItems (EventID, DefectID, " _
                            & "Xpos, Ypos, DefectDescriptionID, DefectDateTime, " _
                            & "IPID) VALUES ('" & .EventID & "', '" _
                            & .DefectID & "', " & .Xpos & ", " & .Ypos & ", " _
                            & .DefectDescriptionID & ", '" & .DefectDateTime _
                            & "', " & .IPID & ")"
                adocmd.Execute
                Set adocmd = Nothing
'ORIGINAL
'                    strsql = "INSERT INTO tblDefectSubItems (EventID, DefectID, " _
'                            & "Xpos, Ypos, DefectDescriptionID, DefectDateTime, " _
'                            & "IPID) VALUES ('" & .EventID & "', '" _
'                            & .DefectID & "', " & .Xpos & ", " & .Ypos & ", " _
'                            & .DefectDescriptionID & ", '" & .DefectDateTime _
'                            & "', " & .IPID & ")"
'                    If rst.State = adStateOpen Then rst.Close
'                    rst.Open strsql, connCurrent
                End With
            Next Item
            cnt = 10
        End If
    
' COMMENT OUT ON 08-FEB-2010
'   If SendDataToOracle = 1 Then
'        SendDataToOracleReceiver oDefParent(1).EventDateTime, oDefParent(1).PartSN, oDefParent(1).PartNumber, oDefParent(1).ItemClass, _
'            oDefChildren(1).DefectDescriptionID
'    End If
    cnt = 11
    inspTrans       'call sp_HandleTransactionScan to insert into tblInspectionTrans table
    cnt = 12
    Else:
        Exit Sub
    End If
    'commented out on 01-Apr-2008
    ctlTimer.Enabled = True
    ctlScan.SetFocus
    cnt = 13
    RefreshPartsAndDefects
    cnt = 14
    
    clrFrm True
    Set adocmd = Nothing
    Set rst = Nothing
    Exit Sub

Err_Connect:
    logGeneralError "frmDefectEntry.General.UpdateDefectData", CStr(Err.Number), "cnt=" & cnt & " " & Err.Description
    Set adocmd = Nothing
    Set rst = Nothing
    If connState = 2 Then Resume
    
End Sub

Private Sub SendDataToOracleReceiver(ByVal DefectDateTime As Date, ByVal PartSN As String, ByVal PartNumber As String, _
    ByVal ItemClass As String, ByVal DefectDescriptionID As Integer)

    Dim strsql As String, rst As New ADODB.Recordset
    Dim cmd As New ADODB.Command, prm As ADODB.Parameter
    
    On Error GoTo Err_Handler
    
    cmd.ActiveConnection = connCurrent
    cmd.CommandType = adCmdStoredProc
    cmd.CommandText = "usp_SendDataToOracleReceiver"
    Set prm = cmd.CreateParameter("@DefectDateTime", adDate, adParamInput, , DefectDateTime)
    cmd.Parameters.Append prm
    Set prm = cmd.CreateParameter("@PartSN", adVarChar, adParamInput, 50, PartSN)
    cmd.Parameters.Append prm
    Set prm = cmd.CreateParameter("@PartNumber", adVarChar, adParamInput, 50, PartNumber)
    cmd.Parameters.Append prm
    Set prm = cmd.CreateParameter("@ItemClass", adVarChar, adParamInput, 50, ItemClass)
    cmd.Parameters.Append prm
    Set prm = cmd.CreateParameter("@DefectDescriptionID", adInteger, adParamInput, , DefectDescriptionID)
    cmd.Parameters.Append prm
    
    
    'cmd.Execute
    Set cmd = Nothing
    Set rst = Nothing
    Set prm = Nothing
    
    Exit Sub

'insert into tblOracleReceiver(PressNumber, DefectDateTime, PartSN, PartNumber, ItemClass, DefectDescriptionID, Processed)
'values (@PressID, @DefectDateTime, @PartSN, @PartNumber, @ItemClass, @DefectDescriptionID, 0)

Err_Handler:
    logGeneralError "frmDefectEntry.General.SendDataToOracleReceiver", CStr(Err.Number), Err.Description
    
End Sub

Private Sub inspTrans()
    Dim strsql As String, rst As New ADODB.Recordset
    Dim cmd As New ADODB.Command, prm As ADODB.Parameter
    
    On Error GoTo Err_Handler
    
    With oDefParent(1)
        cmd.ActiveConnection = connCurrent
        cmd.CommandType = adCmdStoredProc
        cmd.CommandText = "sp_HandleTransactionScan"
        Set prm = cmd.CreateParameter("@EventID", adVarChar, adParamInput, 50, .EventID)
        cmd.Parameters.Append prm
        Set prm = cmd.CreateParameter("@PartSN", adVarChar, adParamInput, 7, .PartSN)
        cmd.Parameters.Append prm
        Set prm = cmd.CreateParameter("@FromIPID", adInteger, adParamInput, , ctlInspectionPointPre.Tag)
        cmd.Parameters.Append prm
        Set prm = cmd.CreateParameter("@ToIPID", adInteger, adParamInput, , ctlInspectionPoint.ItemData(ctlInspectionPoint.ListIndex))
        cmd.Parameters.Append prm
        Set prm = cmd.CreateParameter("@DispositionID", adInteger, adParamInput, , ctllbDisposition.ItemData(ctllbDisposition.ListIndex))
        cmd.Parameters.Append prm
        Set prm = cmd.CreateParameter("@TransactionDateTime", adDate, adParamInput, , .EventDateTime)
        cmd.Parameters.Append prm
    End With
    'Set rst = cmd.Execute
    cmd.Execute
    
    Set cmd = Nothing
    Set rst = Nothing
    Set prm = Nothing

    Exit Sub
    
Err_Handler:
    logGeneralError "frmDefectEntry.General.inspTrans", CStr(Err.Number), Err.Description

End Sub

Private Function ctlEval() As Boolean
    Dim intTemp As Integer, msgStr As String, counter As Integer
    
    On Error GoTo Err_Handler
        
    With oDefParent(1)
        intTemp = intTemp + IIf(ctllbDisposition.ListIndex = -1, 256, 0)
        intTemp = intTemp + IIf(.PartSN = "", 128, 0)
        intTemp = intTemp + IIf(.PartNumber = "", 64, 0)
        intTemp = intTemp + IIf(ctlPartColour = "", 32, 0)
        intTemp = intTemp + IIf(.LotNum = "", 16, 0)
        intTemp = intTemp + IIf(.ItemClass = "", 8, 0)
        intTemp = intTemp + IIf(ctlInspectionPoint = "", 4, 0)
        intTemp = intTemp + IIf(.EventID = "", 2, 0)
        intTemp = intTemp + IIf(.EventDateTime = "", 1, 0)
        ctlEval = IIf(intTemp = 0, True, False)
    End With
    If ctlEval = False Then
        Select Case DataMask - (Not intTemp And DataMask)
            Case 256
                msgStr = "A Part Disposition is required to continue." _
                        & vbCrLf & "Please select an appropriate disposition for this part."
            Case 128
                msgStr = "A Part Serial Number is required to continue." _
                        & vbCrLf & "Please scan or type in a Serial Number."
            Case 64
                msgStr = "A Part Number is required to continue." _
                        & vbCrLf & "Please re-scan or type in a Part Number."
            Case 32
                msgStr = "A Part Colour is required to continue." _
                        & vbCrLf & "Please re-scan or select a colour" _
                        & vbCrLf & "from the dropdown."
            Case 16
                msgStr = "A Lot Number is required to continue." _
                        & vbCrLf & "Please re-scan or type in a Lot Number."
            Case 8
                msgStr = "A Part Type is required to continue." _
                        & vbCrLf & "Please re-scan or select a part" _
                        & vbCrLf & "type from the dropdown."
            Case 4
                msgStr = "An Inspection Piont is required to continue." _
                        & vbCrLf & "Please select an inspection point" _
                        & vbCrLf & "from the dropdown."
            Case 2
                msgStr = "An Event ID is required to continue." _
                        & vbCrLf & "Please contact a systems Administrator."
            Case 1
                msgStr = "An Event Date/Time is required to continue." _
                        & vbCrLf & "Please contact a systems Administrator."
            Case 0
                ctlEval = True
                Exit Function
            Case Else
                msgStr = "Insufficient information is present to process your " _
                        & "request please review the input form and ensure all" _
                        & vbCrLf & "required information is provided."
        End Select
        MsgBox msgStr, vbOKOnly, "Missing Data"
    End If
    
    Exit Function
    
Err_Handler:
    logGeneralError "frmDefectEntry.General.ctlEval", CStr(Err.Number), Err.Description

End Function

Private Function rstFilter(ByVal rstName As ADODB.Recordset, ByVal strField As String, ByVal strFilter As String) As ADODB.Recordset
    
    On Error GoTo Err_Handler
    
    rstName.Filter = strField & " = '" & strFilter & "'"
    Set rstFilter = rstName
    
    Exit Function
    
Err_Handler:
    logGeneralError "frmDefectEntry.General.rstFilter", CStr(Err.Number), Err.Description

End Function

Private Function getPressIDfromSN(ByVal SN As String) As Integer
'[dbo].[GetPressIDFromPartSN] @PartSN nvarchar(50), @PressID int output
'    select @PressID = Location from SerialNumberMaster where PartSN = @PartSN
'    return @PressID
Dim cmdPressID As New ADODB.Command, oParam As New ADODB.Parameter
On Error GoTo Err_Handler

'connStr2 = PartsIDConnStr?

 


    cmdPressID.ActiveConnection = connCurrent
    'cmdPressID.ConnectionString = connStr2
    
    
    
    
    cmdPressID.CommandType = adCmdStoredProc
    cmdPressID.CommandText = "GetPressIDFromPartSN"
    Set oParam = cmdPressID.CreateParameter("@PartSN", adVarChar, adParamInput, 50, SN)
    cmdPressID.Parameters.Append oParam
    Set oParam = cmdPressID.CreateParameter("@PressID", adInteger, adParamOutput)
    cmdPressID.Parameters.Append oParam
    
    cmdPressID.Execute
    getPressIDfromSN = cmdPressID.Parameters("@PressID").Value
    
    Set cmdPressID = Nothing
    Set oParam = Nothing
    
    Exit Function
    
Err_Handler:
    logGeneralError "frmDefectEntry.getPressIDfromSN", CStr(Err.Number), Err.Description

End Function

'Private Sub getPressProdToolfromSN(ByVal SN As String, ByRef pressID As Integer, ByRef productionID As Double, ByRef toolID As Double)
Private Sub getPressProdToolfromSN(ByVal SN As String, pressID As Integer, productionID As Double, toolID As Double)
Dim cmd As New ADODB.Command, oParam As New ADODB.Parameter
'''Dim conn As New ADODB.Connection

On Error GoTo Err_Handler


    If connPartsID.State <> adStateOpen Then
        connPartsID.Open
    End If

    cmd.ActiveConnection = connPartsID
     
    cmd.CommandType = adCmdStoredProc
    cmd.CommandText = "GetPressProdToolFromSN"
    Set oParam = cmd.CreateParameter("@PartSN", adVarChar, adParamInput, 50, SN)
    cmd.Parameters.Append oParam
    Set oParam = cmd.CreateParameter("@PressID", adInteger, adParamOutput)
    cmd.Parameters.Append oParam

'    Also get ProductionID, ToolID from SerialNumberMaster
    Set oParam = cmd.CreateParameter("@ProductionID", adDouble, adParamOutput)
    cmd.Parameters.Append oParam
    Set oParam = cmd.CreateParameter("@ToolID", adDouble, adParamOutput)
    cmd.Parameters.Append oParam

    cmd.Execute

    pressID = cmd.Parameters("@PressID").Value
    productionID = cmd.Parameters("@ProductionID").Value
    toolID = cmd.Parameters("@ToolID").Value
    
'''MsgBox "getPressProdToolFromSN " & SN & " PressID=" & pressID & " productionID=" & productionID & " toolID=" & toolID
    
    connPartsID.Close

    
    Set cmd = Nothing
    Set oParam = Nothing
    
    Exit Sub
    
Err_Handler:
    logGeneralError "frmDefectEntry.getPressIDfromSN", CStr(Err.Number), Err.Description

End Sub

Private Function getPressIDfromInspPoint(ByVal inspPoint As String) As Integer
'[dbo].[GetPressIDFromPartSN] @PartSN nvarchar(50), @PressID int output
'    select @PressID = Location from SerialNumberMaster where PartSN = @PartSN
'    return @PressID
Dim cmd As New ADODB.Command, param As New ADODB.Parameter, pressID As Integer
On Error GoTo Err_Handler

    cmd.ActiveConnection = connStr
    cmd.CommandType = adCmdStoredProc
    cmd.CommandText = "GetPressIDFromInspPnt"
    Set param = cmd.CreateParameter("@InspPoint", adVarChar, adParamInput, 50, inspPoint)
    cmd.Parameters.Append param
    Set param = cmd.CreateParameter("@pressID", adInteger, adParamOutput, , pressID)
    cmd.Parameters.Append param
    
    cmd.Execute
    getPressIDfromInspPoint = cmd.Parameters("@pressID").Value

    Set cmd = Nothing
    Set param = Nothing

    Exit Function
    
Err_Handler:
    logGeneralError "frmDefectEntry.getPressIDfromInspPoint", CStr(Err.Number), Err.Description

End Function

Private Function OkToProceed() As Boolean

    Dim sDisposition As String
    Dim iDefectCount As Integer
    
    sDisposition = ctllbDisposition.Text
    iDefectCount = ctlDefects.ListItems.Count
    If iDefectCount < 1 Then
    'If sDisposition <> "Ok" And iDefectCount < 1 Then
        OkToProceed = False
    ElseIf ctllbDisposition.ListIndex = -1 Then
        OkToProceed = False
    Else
        OkToProceed = True
    End If
End Function
'Private Function OkToProceed() As Boolean
'Dim sDisposition As String
'Dim iDefectCount As Integer
'    sDisposition = ctllbDisposition.Text
'    iDefectCount = ctlDefects.ListItems.Count
'    If sDisposition <> "Ok" And iDefectCount < 1 Then
'        OkToProceed = False
'    Else
'        OkToProceed = True
'    End If
'End Function

Private Sub cmdComponents_Click()
   
    If txQty.Text <= 0 Then
        MsgBox "Quantity = 0. Nothing to Process!!!"
    End If
    If frmDefectEntry.lsComponents.ListIndex < 0 Then
        MsgBox "No Selected Part. Nothing to Process!!!"
    End If
    
    ctlPartSN.SetFocus
    
'    Private Type POINTAPI 'Declare types
'        x As Long
'        y As Long
'    End Type
'
'    Private Declare Function GetCursorPos Lib "user32" (lpPoint As POINTAPI) As Long
'    Dim apiPoint As POINTAPI 'Declare variable
'
'    GetCursorPos apiPoint 'Get Co-ordinets
'    MsgBox apiPoint.x & " " & apiPoint.y
    'MsgBox frmDefectEntry.lsComponents.List(frmDefectEntry.lsComponents.ListIndex)

End Sub

'Private Sub form_MouseDown(Button As Integer, shift As Integer, x As Single, y As Single)
''If e.Button = Windows.Forms.MouseButtons.Left Then
''MsgBox ("Left")
''Else
''MsgBox ("right")
''End If
''Left button = 1 Right button = 2, shift = 0
''Text1.Text = "Button=" & CStr(Button) & " Shift=" & CStr(shift) & " " & CStr(x) & "," & CStr(y)
'
''MsgBox "x=" & x & "  y=" & y
''    If (x >= 11380 And x <= 14530 And y >= 7215 And y <= 7854) Then
'    If (x >= 11405 And x <= 13278 And y >= 10076 And y <= 10589) Then
'        cmdReset_Click
'        Unload Me
'    End If
'
'    'top left x = 13353.44, top left y = 9081.05, bottom right x = 14761.35, bottom right y = 9718.544,
''    If (x >= 13353 And x <= 14761 And y >= 9081 And y <= 9718) Then
''        lbComponentsClick
''    End If
'End Sub

Private Sub cmdProcess_Click()
On Error GoTo Err_Handler
'NEWSCAN MEANS BRAND NEW PART, NO PREVIOUS DEFECTS, WHICH IS JUST MOULDED, THUS NO PREVIOUS DEFECTS
'COMMENT OUT IF NEWSCAN THEN, NEWSCAN=FALSE, END IF 05-NOV-2008 16:01
'    If newScan Then
Dim tempPartNum As String, tempSerial As String, tempRef As String

'YOU NEED THESE 3 TEMP VARIABLES, IF NOT THE CTLXXXXXX ARE ALREADY CLEARED
tempPartNum = ctlPartNumber.Text
tempSerial = ctlPartSN.Text
tempRef = ctlPartSN.Text

'YOU STILL NEED A WAY TO PREVENT INCORRECTLY PRESS THE PROCESS BUTTON
If mouldPress = 1 Then
    If newScan Then
'        If OverrideOK = 0 Then
'            MsgBox "This part may not be acceptable to proceed." _
'                & vbCrLf & "It cannot be shipped with its current disposition of '" & ctllbDisposition & "'.", _
'                vbCritical, "Attention:  Defective Part"
'        End If
        
        UpdateDefectData
        ctllbDisposition.Enabled = True
        ctlPartSN.SetFocus
        'newScan = False
    End If
Else
    If OkToProceed Then
'        If OverrideOK = 0 Then
'            MsgBox "This part may not be acceptable to proceed." _
'                & vbCrLf & "It cannot be shipped with its current disposition of '" & ctllbDisposition & "'.", _
'                vbCritical, "Attention:  Defective Part"
'        End If

        UpdateDefectData
        ctllbDisposition.Enabled = True
        ctlPartSN.SetFocus
    Else
        MsgBox "You must enter a defect to continue", vbOKOnly, "No Defects Entered"
    End If
End If

'*************************************************************************************************
'CONSIDER TAKING THIS CHUNK OUT AND LET INSPECTIONTRANS TAKES CARE OF IT
'TAKEN CARE BY UPDATEDEFECTDATA() ABOVE AFTER TABLE TRIGGER IS CREATED
   'Insert into SQLSERVER.PolyconTrans.dbo.tblInvTransactionsToAS400 (TransactionDateTime,TransactionCode,PartNumber,TransactionQty,SerialNumber,Reference,FromLocation,FromWareHouse,ToLocation,ToWarehouse,ReasonCode,EmployeeNumber,Processed)
   'VALUES(getdate(),'TW','308000000',1,'BA1Z2S3','BA1Z2S3','MOULD','1','RW01','2','999999',0,0)
       
   'SELECT ITNBR,TRQTY,TRNDT,REFNO FROM AS400.POLCYON.AMFLIB.IMHIST
   'WHERE ITNBR='308000000' AND HOUSE='1' AND TRNDT=1090522 AND REFNO='BA1Z2S3'
   'SELECT * FROM OPENQUERY(AS400,'SELECT ITNBR,TRQTY,TRNDT,REFNO FROM POLCYON.AMFLIB.IMHIST
   'WHERE ITNBR=''308000000'' AND HOUSE=''1'' AND TRNDT=1090522 AND REFNO=''BA1Z2S3''')

'If newScan Then
'   newScan = False
'   Dim cmd As New ADODB.Command
'
'   With cmd
'     .ActiveConnection = connCurrent
'     .CommandType = adCmdStoredProc
'     .CommandText = "insertNewTransactions"
'     '.CommandText = "insertPolyConTransInvTransAs400"
'     '.CommandText = "insertTransToInvTransAs400"
'  End With
'  cmd.Parameters.Append cmd.CreateParameter("partNum", adVarChar, adParamInput, 15, tempPartNum)
'  cmd.Parameters.Append cmd.CreateParameter("serialNum", adVarChar, adParamInput, 10, tempSerial)
'  cmd.Parameters.Append cmd.CreateParameter("ref", adVarChar, adParamInput, 10, tempRef)
'  'cmd.Parameters.Append cmd.CreateParameter("prevDispID", adInt, adParamInput, 10, prevDisp)
'  'cmd.Parameters.Append cmd.CreateParameter("currDispID", adInt, adParamInput, 10, currDisp)
'
'  cmd.Execute
'  Set cmd.ActiveConnection = Nothing
'End If
'*************************************************************************************************
newScan = False
ctlPartSN.SetFocus

Exit Sub

Err_Handler:
    logGeneralError "frmDefectEntry.cmdProcess_Click", CStr(Err.Number), Err.Description
End Sub

Private Sub cmdReset_Click()

    On Error GoTo Err_Handler

    clrFrm True, True
    ctlDefectTypeRefresh
    ctllbDisposition.Enabled = True
    
    ctlPartSN.SetFocus
    newScan = False
    
    Exit Sub
    
Err_Handler:
    logGeneralError "frmDefectEntry.cmdReset_Click", CStr(Err.Number), Err.Description
End Sub

Private Sub cmdRemove_Click()
    Dim ListItem As ListItem
    Dim defID As Integer
    
    On Error GoTo Err_Handler
    
    If Not ctlDefects.ListItems.Count = 0 Then
        If ctlDefects.SelectedItem.SubItems(5) = "Yes" Then
            defID = ctlDefects.SelectedItem.Tag
            ctlDefects.ListItems.Remove (ctlDefects.SelectedItem.Index)
            Unload ctlDot(defID)
            For Each ListItem In ctlDefects.ListItems
                With ListItem
                    .Text = .Index
                End With
            Next ListItem
        End If
    End If
    
    txDontScan.SetFocus
    'ctlDontScan.SetFocus
    ctlPartSN.SetFocus
    Exit Sub
    
Err_Handler:
    logGeneralError "frmDefectEntry.cmdRemove_Click", CStr(Err.Number), Err.Description

End Sub

Private Sub Command3_Click()
    If ctlTime.Visible = False Then
        ctlTime.Visible = True
'        ctlTime.Value = Time
        ctlDateSelect.Visible = True
'        ctlDateSelect.Value = Date
        ctlDateTimeSet.Visible = True
        ctlTimer.Enabled = False
    Else
        ctlTime.Visible = False
        ctlDateSelect.Visible = False
        ctlDateTimeSet.Visible = False
        ctlTimer.Enabled = True
    End If
End Sub

Private Sub ctlDateSelect_DateClick(ByVal DateClicked As Date)
    ctlDateTimeSet.SetFocus
End Sub

Private Sub ctlDateTimeSet_Click()
    Dim strTime As String, varTime As Variant

    On Error GoTo Err_Handler

    ctlDateSelect.Visible = False
    ctlTime.Visible = False
    ctlDateTimeSet.Visible = False
    
    If ctlDateSelect.Value > Date Then
        ctlDateSelect.Value = Date
        If ctlTime.Value > Time Then
            ctlTime.Value = Time
        End If
    ElseIf ctlDateSelect.Value = Date And ctlTime.Value > Time Then
        ctlTime.Value = Time
    End If
    
    strTime = ctlTime.Value
    varTime = Split(strTime, " ")
    
'    If Len(strTime) > 10 Then
        ctlEventDateTime.Text = ctlDateSelect.Value & " " & varTime(0) & " " & varTime(1)
'    Else
'        ctlEventDateTime.Text = ctlDateSelect.Value & " 12:00:00 AM"
'    End If
    
    Exit Sub
    
Err_Handler:
    MsgBox "You must set a specific Date and Time for the Inspection", vbOKOnly, "Input Missing"
    
    'commented out on 01-Apr-2008
    ctlTimer.Enabled = True
    
    logGeneralError "frmDefectEntry.ctlDateTimeSet_Click", CStr(Err.Number), Err.Description
    
End Sub

Private Sub ctlDefectName_Click()

    On Error GoTo Err_Handler

    txDontScan.SetFocus
    'ctlDontScan.SetFocus
    
    Exit Sub
    
Err_Handler:
    logGeneralError "frmDefectEntry.ctlDefectName_Click", CStr(Err.Number), Err.Description

End Sub

Private Sub ctlDefects_ItemClick(ByVal Item As MSComctlLib.ListItem)
    Dim ListItem As ListItem
    
    On Error GoTo Err_Handler
    
    For Each ListItem In ctlDefects.ListItems
        If ctlDot(ListItem.Tag).FillColor = &HFFFF& Then
            ctlDot(ListItem.Tag).Top = ctlDot(ListItem.Tag).Top + 30
            ctlDot(ListItem.Tag).Left = ctlDot(ListItem.Tag).Left + 30
            ctlDot(ListItem.Tag).Height = ctlDot(ListItem.Tag).Height - 60
            ctlDot(ListItem.Tag).Width = ctlDot(ListItem.Tag).Width - 60
            ctlDot(ListItem.Tag).FillColor = &HFF&
            ctlDot(ListItem.Tag).BorderColor = &HC0&
        End If
    Next ListItem
    ctlDot(Item.Tag).Top = ctlDot(Item.Tag).Top - 30
    ctlDot(Item.Tag).Left = ctlDot(Item.Tag).Left - 30
    ctlDot(Item.Tag).Height = ctlDot(Item.Tag).Height + 60
    ctlDot(Item.Tag).Width = ctlDot(Item.Tag).Width + 60
    ctlDot(Item.Tag).FillColor = &HFFFF&
    ctlDot(Item.Tag).BorderColor = &H0&
    
    txDontScan.SetFocus
    'ctlDontScan.SetFocus
    
    Exit Sub
    
Err_Handler:
    logGeneralError "frmDefectEntry.ctlDefects_ItemClick", CStr(Err.Number), Err.Description

End Sub

Private Sub ctlDefectType_Click()
    Dim rst As New ADODB.Recordset, lstind As Long
    
    On Error GoTo Err_Handler
        
    Set rst = rstFilter(rstDefectDescriptions, "DTypeID", ctlDefectType.ItemData(ctlDefectType.ListIndex))
            ctlDefectName.Clear
            Do Until rst.EOF
                With ctlDefectName
                    .AddItem rst("DefDescrip"), lstind
                    .ItemData(lstind) = rst("DefID")
'                    .AddItem rst("DefectDescription"), lstind
'                    .ItemData(lstind) = rst("DefectDescriptionID")
                End With
                lstind = lstind + 1
                rst.MoveNext
            Loop
'            .DefectTypeID = rstDefectDescriptions("DTypeID")
'            .DefectDescriptionID = rstDefectDescriptions("DefID")
'            .DefectDescription = rstDefectDescriptions("DefDescrip")
'            .DefectInfo = rstDefectDescriptions("DefGrpDescrip")
    
    txDontScan.SetFocus
    'ctlDontScan.SetFocus
    
    Set rst = Nothing

    Exit Sub
    
Err_Handler:
    logGeneralError "frmDefectEntry.ctlDefectType_Click", CStr(Err.Number), Err.Description

End Sub

Private Sub lbClose_Click()
    cmdReset_Click
    Unload Me
End Sub

Private Sub lbProcessComponents_Click()
    If txQty.Text = 0 Then
        MsgBox "Quantity = 0. Nothing to Process!!!"
        ctlPartSN.SetFocus
        Exit Sub
    End If
    If frmDefectEntry.lsComponents.ListIndex < 0 Then
        MsgBox "No Selected Part. Nothing to Process!!!"
        ctlPartSN.SetFocus
        Exit Sub
    End If
    
    'MsgBox "Done"
    Dim PartNumber As String, ItemClass As String, entry As String
    entry = frmDefectEntry.lsComponents.List(frmDefectEntry.lsComponents.ListIndex)
    PartNumber = Right$(entry, Len(entry) - 31)
    ItemClass = Trim(Right$(PartNumber, 4))
    PartNumber = Trim(Left$(PartNumber, 12))
    
    'pressNum = getPressIDfromInspPoint(ctlInspectionPoint.Text)
    
    transactComponents InspDefault, pressNum, ItemClass, PartNumber, txQty
    txQty = 0
    
    ctlPartSN.SetFocus

End Sub

Private Sub lbRefresh_Click()
    lsComponents.Clear
    pressNum = getPressIDfromInspPoint(ctlInspectionPoint.Text)
    getPrint0QtyComponents (pressNum)     '************************************************
    If frmDefectEntry.lsComponents.ListIndex = -1 Then
        txQty = 0
    End If
End Sub

'Private Sub txClose_Click()
'    cmdReset_Click
'    Unload Me
'End Sub

Private Sub txDontScan_LostFocus()
    If txDontScan.Text <> "" Then
        MsgBox "You cannot scan another part until the current part has been processed or reset.", vbCritical, "Incomplete Process"
        txDontScan.SetFocus
        'ctlDontScan.SetFocus
        txDontScan = ""
    End If
End Sub

'**********************************************************************
'Private Sub ctlDontScan_LostFocus()
'    MsgBox "You cannot scan another part until the current part has been processed or reset.", vbCritical, "Incomplete Process"
'    'ctlDontScan.SetFocus ***********************
'    txDontScan.SetFocus
'    txDontScan = ""
'End Sub

Private Sub ctlDontScan_Click()
    MsgBox "You cannot scan another part until the current part has been processed or reset.", vbCritical, "Incomplete Process"
End Sub

Private Sub ctlImage_MouseDown(Button As Integer, shift As Integer, x As Single, y As Single)
    Dim litm As ListItem, counter As Integer
    
    On Error GoTo Err_Handler
    
    If DefectButtons(tempIndex).BackColor = &H80FF& Then
    'If Not ctlDefectType = "" And Not ctlDefectName = "" And Not ctlInspectionPoint = "" Then
        Load ctlDot(DefMax + 1)
        ctlDot(DefMax + 1).Top = ctlImage.Top + y + 30
        ctlDot(DefMax + 1).Left = ctlImage.Left + x - 113
'        ctlDot(DefMax + 1).Top = ctlImage.Top + Y - 30
'        ctlDot(DefMax + 1).Left = ctlImage.Left + x - 30
        ctlDot(DefMax + 1).ZOrder
        ctlDot(DefMax + 1).Visible = True
        Set litm = ctlDefects.ListItems.Add(, , ctlDefects.ListItems.Count + 1)
        litm.SubItems(itmDefectType) = defTypeDescArray(tempIndex)
        'litm.SubItems(itmDefectType) = ctlDefectType
        litm.SubItems(itmDefectName) = DefectButtons(tempIndex).Caption
        'litm.SubItems(itmDefectName) = ctlDefectName
        litm.SubItems(itmXpos) = x
        litm.SubItems(itmYpos) = y
        litm.SubItems(itmNew) = "Yes"
        litm.SubItems(itmDefectDateTime) = ctlEventDateTime.Text
        litm.SubItems(itmEventID) = oDefParent(1).EventID
        litm.SubItems(itmInspectionPoint) = ctlInspectionPoint
        litm.SubItems(itmDefectID) = GetGUID
        '*********
        litm.SubItems(itmDefectDescriptionID) = defIDArray(tempIndex)
        'litm.SubItems(itmDefectDescriptionID) = ctlDefectName.ItemData(ctlDefectName.ListIndex)
        'litm.SubItems(itmIPID) = ctlInspectionPoint.ItemData(ctlInspectionPoint.ListIndex)
        litm.SubItems(itmIPID) = ctlInspectionPoint.ItemData(ctlInspectionPoint.ListIndex)
        litm.Tag = DefMax + 1
        DefMax = DefMax + 1
        'I took the following out on 04-Mar-2008 to allow it to default at Regrind
        'If OverrideOK = 1 Then
        '    ctllbDisposition.ListIndex = -1
        'End If
        
        'Added on 01-Apr-2008 to prevent loop endlessly by triggering ctlPartSN_LostFocus()
        If ctlPartSN = "" Then
            ctlPartSN.SetFocus
        Else
            txDontScan.SetFocus
            'ctlDontScan.SetFocus
        End If
    End If

    Exit Sub
    
Err_Handler:
    logGeneralError "frmDefectEntry.ctlImage_MouseDown", CStr(Err.Number), Err.Description

End Sub

Private Sub ctlInspectionPoint_Click()

    On Error GoTo Err_Handler

    'Added on 01-Apr-2008 to prevent loop endlessly by triggering ctlPartSN_LostFocus()
    If ctlPartSN.Text = "" Then
        ctlPartSN.SetFocus
    Else
        txDontScan.SetFocus
        'ctlDontScan.SetFocus
    End If
    
    Exit Sub
    
Err_Handler:
    logGeneralError "frmDefectEntry.ctlInspectionPoint_Click", CStr(Err.Number), Err.Description

End Sub

Private Sub ctlInspectionPoint_DropDown()

    On Error GoTo Err_Handler

    stInspectionPoints
    
    Exit Sub
    
Err_Handler:
    logGeneralError "frmDefectEntry.ctlInspectionPoint_DropDown", CStr(Err.Number), Err.Description

End Sub

Private Sub ctlItemClass_DropDown()
    Dim ItemClass As ItemClass
    
    On Error GoTo Err_Handler
    
    ctlItemClass.Clear
    For Each ItemClass In ItemLst
        With frmDefectEntry.ctlItemClass
            .AddItem ItemClass.ItemDescription, ItemClass.ItemClassIDX
            .ItemData(ItemClass.ItemClassIDX) = ItemClass.ItemClassIDX
        End With
    Next ItemClass
    
    Exit Sub
    
Err_Handler:
    logGeneralError "frmDefectEntry.ctlItemClass_DropDown", CStr(Err.Number), Err.Description

End Sub

Private Sub ctlLocked_Click()

    On Error GoTo Err_Handler

    ctlInspectionPoint.Enabled = IIf(ctlLocked = 1, False, True)
    
    txDontScan.SetFocus
    'ctlDontScan.SetFocus
    
    Exit Sub
    
Err_Handler:
    logGeneralError "frmDefectEntry.ctlLocked_Click", CStr(Err.Number), Err.Description

End Sub

Private Sub ctlLotNum_KeyDown(KeyCode As Integer, shift As Integer)

    On Error GoTo Err_Handler

    If KeyCode = vbKeyReturn Then
        txDontScan.SetFocus
        'ctlDontScan.SetFocus
    End If
    
    Exit Sub
    
Err_Handler:
    logGeneralError "frmDefectEntry.ctlLotNum_KeyDown", CStr(Err.Number), Err.Description

End Sub

Private Sub ctlPartColour_Click()
    Dim oPartItem As Part
    Dim rst As New ADODB.Recordset, strPartColour As String
    
    On Error GoTo Err_Handler
    
    strPartColour = ctlPartColour.ItemData(ctlPartColour.ListIndex)
    For Each oPartItem In oParts
        If strPartColour = oPartItem.PartIDX Then
            ctlPartNumber = oPartItem.PartNumber
            oDefParent(1).PartNumber = oPartItem.PartNumber
        End If
    Next oPartItem
    
    'Added on 01-Apr-2008 to prevent loop endlessly by triggering ctlPartSN_LostFocus()
    If ctlPartSN = "" Then
        ctlPartSN.SetFocus
    Else
        txDontScan.SetFocus
        'ctlDontScan.SetFocus
    End If
    
    Set rst = Nothing

    Exit Sub
    
Err_Handler:
    logGeneralError "frmDefectEntry.ctlPartColour_Click", CStr(Err.Number), Err.Description

End Sub

Private Sub ctlPartColour_DropDown()
    Dim oPartItem As Part
    
    On Error GoTo Err_Handler
    
    ctlPartColour.Clear
    For Each oPartItem In oParts
        With ctlPartColour
            .AddItem oPartItem.ColourDescription, oPartItem.PartIDX
            .ItemData(oPartItem.PartIDX) = oPartItem.PartIDX
        End With
    Next oPartItem
    
    Exit Sub
    
Err_Handler:
    logGeneralError "frmDefectEntry.ctlPartColour_DropDown", CStr(Err.Number), Err.Description

End Sub

Private Sub OriginctlPartSN_KeyDown(KeyCode As Integer, shift As Integer)

    On Error GoTo Err_Handler
    
    If KeyCode = vbKeyReturn Then
        clrFrm False
        ldDefParent
        If BackOut = True Then
            clrFrm True
            BackOut = False
            Exit Sub
        End If
        ldDefChildren
        
        txDontScan.SetFocus
        'ctlDontScan.SetFocus
        
        BackOut = False
    End If
    
    Exit Sub
    
Err_Handler:
    logGeneralError "frmDefectEntry.ctlPartSN_KeyDown", CStr(Err.Number), Err.Description

End Sub

Private Sub ctlPartSN_LostFocus()
    On Error GoTo Err_Handler
    
    ctlPartSN = UCase(ctlPartSN)
    
    'MsgBox cmdComponents.Value
    
    If Len(ctlPartSN.Text) = 7 Then
        clrFrm False
        ldDefParent
        If BackOut = True Then
            clrFrm True
            BackOut = False
            Exit Sub
        End If
        ldDefChildren
        
        txDontScan.SetFocus
        'ctlDontScan.SetFocus
        
        BackOut = False
    
        oDefParent(1).PartSN = ctlPartSN
    ElseIf Len(ctlPartSN.Text) > 7 Then
        MsgBox "Wrong BarCode Being Scanned. Invalid Serial Number!!! Try Scan A Different BarCode."
        ctlPartSN = ""
        ctlPartSN.SetFocus
    ElseIf (txQty.Text > 0 And lsComponents.ListIndex > 0) Then
        cmdComponents_Click
        ctlPartSN.SetFocus
'    ElseIf ctlPartSN.Text = "" And (txQty.Text = 0 Or lsComponents.ListIndex = -1) Then
'        ctlPartSN.SetFocus
    ElseIf ctlPartSN.Text = "" Then
        ctlPartSN.SetFocus
    End If

    Exit Sub
    
Err_Handler:
    logGeneralError "frmDefectEntry.ctlPartSN_LostFocus", CStr(Err.Number), Err.Description

End Sub

Private Sub OriginalctlPartSN_LostFocus()

    On Error GoTo Err_Handler

    oDefParent(1).PartSN = UCase(ctlPartSN)
    
    Exit Sub
    
Err_Handler:
    logGeneralError "frmDefectEntry.ctlPartSN_LostFocus", CStr(Err.Number), Err.Description

End Sub

Private Sub ctlScan_Click()
    Dim strPartNumber As String
    
    On Error GoTo Err_Handler
    
    If SendScan = 1 Then
        With MSComm1
            .CommPort = SendPort
            .Handshaking = comNone
            .Settings = "9600,N,8,1"
        End With
    End If
    
'For Scanning new SN only labels at moulding
'    If Len(strScan) >= 7 And Len(strScan) < 18 Then
'    If Len(strScan) <= 21 And Len(strScan) >= 18 Then
'        If ClientIsRemote = True Then
'            ctlPartSN = Mid(strScan, 4, 7)
'            strPartNumber = Mid(strScan, 11, 11)
'            ctlPartNumber = strPartNumber
'        Else
'            ctlPartSN = strScan
'        End If
     strScan = UCase(strScan)
     If Len(strScan) >= 7 And Len(strScan) < 18 Then
        ctlNoScan.Visible = False
        If ClientIsRemote = True Then
            ctlPartSN = Mid(strScan, 4, 7)
            strPartNumber = Mid(strScan, 11, 11)
            ctlPartNumber = strPartNumber
        Else
            ctlPartSN = strScan
        End If
        
        If SendScan = 1 Then
            With MSComm1
                .PortOpen = True
                .Output = strScan       'Mid(strScan, 11, 9)
                .PortOpen = False
            End With
        End If
        'Added on 01-Apr-2008 to prevent loop endlessly by triggering ctlPartSN_LostFocus()
       If ctlPartSN.Text = "" Then
           ctlPartSN.SetFocus
        Else
            ctlScan.SetFocus
        End If
        'ctlScan.SetFocus
        
        SendKeys "{ENTER}"
        strScan = ""
    Else:
        clrFrm True
        ctlNoScan.Visible = True
        strScan = ""
        'Added on 01-Apr-2008 to prevent loop endlessly by triggering ctlPartSN_LostFocus()
        If ctlPartSN.Text = "" Then
            ctlPartSN.SetFocus
        Else
            ctlScan.SetFocus
        End If
    End If
    
    Exit Sub
    
Err_Handler:
    logGeneralError "frmDefectEntry.ctlPartSN_LostFocus", CStr(Err.Number), Err.Description

End Sub

Private Sub ctlScan_KeyPress(KeyAscii As Integer)

    On Error GoTo Err_Handler

    strScan = strScan & Chr(KeyAscii)
    Exit Sub
    
Err_Handler:
    logGeneralError "frmDefectEntry.ctlScan_KeyPress", CStr(Err.Number), Err.Description

End Sub

Private Sub ctlTimerConn_Timer()

    On Error GoTo Err_Handler

    CheckConnectionStatus
    
    RefreshOK = True
    
    Exit Sub
    
Err_Handler:
    logGeneralError "frmDefectEntry.ctlTimerConn_Timer", CStr(Err.Number), Err.Description

End Sub

Private Sub Form_Activate()

    On Error GoTo Err_Handler

    ctlInspectionPoint_DropDown
    ctlInspectionPoint = InspDefault
    ctlLocked = 1
    ctlLocked.Enabled = LockEnabled
    'commented out 01-Apr-2008
    ctlTimerConn_Timer
    ctlPartSN.SetFocus
    'ctlScan.SetFocus
    
    Exit Sub
    
Err_Handler:
    logGeneralError "frmDefectEntry.Form_Activate", CStr(Err.Number), Err.Description

End Sub

Public Sub DefectButtons_Click(Index As Integer)

    If ctlPartSN.Text = "" Then
       MsgBox "Scan A Part Serial Number Before Selecting a Defect!!!"
       'Added on 01-Apr-2008 to prevent loop endlessly by triggering ctlPartSN_LostFocus()
       ctlPartSN.SetFocus
       Exit Sub
    End If
    
    frmDefectEntry.DefectButtons(tempIndex).Caption = tempCaption
    frmDefectEntry.DefectButtons(tempIndex).Font.Italic = False
    frmDefectEntry.DefectButtons(tempIndex).BackColor = &H8000000F
    
    tempCaption = frmDefectEntry.DefectButtons(Index).Caption
    frmDefectEntry.DefectButtons(Index).BackColor = &H80FF&
    frmDefectEntry.DefectButtons(Index).Font.Italic = True
    '&H80C0FF light Orange
    '&H000080FF& medium orange
    '&H000000C0& Darker Orange
    '&H000000FF& red
    '&H008080FF& light red
    'frmDefectEntry.DefectButtons(index).Font.Bold = True
    'frmDefectEntry.DefectButtons(index).Font.Underline = True
    tempIndex = Index
        
    'If Not ctlDefectType = "" And Not ctlDefectName = "" And Not ctlInspectionPoint = "" Then
    ctlDefectType = defTypeDescArray(tempIndex)
    'ctlDefectName.ListIndex = tempIndex
    ctlDefectName.Text = tempCaption
    
    'Added on 01-Apr-2008 to prevent loop endlessly by triggering ctlPartSN_LostFocus()
    If ctlPartSN.Text = "" Then
        ctlPartSN.SetFocus
    Else
        ctlScan.SetFocus
    End If
    'frmDefectEntry.ctlPartSN.SetFocus
End Sub

Private Sub Form_Load()

    ''Dim args() As String
    ''args = Split(Command$, " ")
    ''loginName = args(0)
    ''pressSetupFolder = "C:\ApplicationFolders\MouldPressSetup\"
    'pressSetupFolder = Trim(Command$)
    'ReadXML.ParseXmlDocument (pressSetupFolder)
    
    Dim sStr As String
    Dim found As Boolean
    
    ctlComputername = ComputerName

    getPressDesc
    
    On Error GoTo Err_Handler
    
    ctlStatBar.Panels.Add
    ctlStatBar.Panels(1).Width = 400
    ctlStatBar.Panels(1).Picture = LoadResPicture("YELLOW", vbResBitmap)
    ctlStatBar.Panels(2).Width = 14970
    
    ctlTimerConn.Interval = 60000
    ctlTimer.Interval = 1000
    ctlTimerConn.Enabled = True
    ctlTimer.Enabled = True
    ctlEventDateTime = Now
    ctlDefects.View = lvwReport
    ctlDefects.LabelEdit = lvwManual
    ctlInspectionPointPre.Tag = 0
    
    With ctlDefects.ColumnHeaders
        .Add , , "Defect", 960              '885
        .Add , , "Defect Type", 2194        '1200
        .Add , , "Defect Name", 3480        '1695
        .Add , , "X val", 0                 '540
        .Add , , "Y val", 0                 '540
        .Add , , "New", 705                 '570
        .Add , , "Date \ Time", 2670        '1830
        .Add , , "EventID", 0               '3000
        .Add , , "InspectionPoint", 2000
        .Add , , "DefectID", 0              '3000
        .Add , , "DefectDescriptionID", 0   '3000
        .Add , , "IPID", 0                  '3000
    End With

    bldDefectTypes
    bldDefects
    FillctllbDisposition
    ctlDefectTypeRefresh
    ctlDateSelect.Value = Date
    oDefParent.Add GetGUID
    mdlFillDefButtons.FillDefectButtons

    If ClientIsRemote Then Command3.Enabled = True

    Exit Sub
Err_Handler:
    logGeneralError "frmDefectEntry.Form_Load", CStr(Err.Number), Err.Description
End Sub

'Private Sub ParseXmlDocument()
'   Dim doc As New MSXML2.DOMDocument
'   Dim success As Boolean
'    On Error GoTo err_Handler
'
'   success = doc.Load("C:\ApplicationFolders\PressInterfaces\InterfaceAlert.xml")
'   'success = doc.Load(App.Path & "\InterfaceAlert.xml")
'   If success = False Then
'      MsgBox doc.parseError.reason
'   Else
'      Dim nodeList As MSXML2.IXMLDOMNodeList
'
'      Set nodeList = doc.selectNodes("/Entries/Entry")
'
'      If Not nodeList Is Nothing Then
'         Dim node As MSXML2.IXMLDOMNode
'         'Dim name As String
'         'Dim value As String
'        Dim i As Integer
'        i = 0
'
'         For Each node In nodeList
'            ' Could also do node.attributes.getNamedItem("name").text
'            nodeName(i) = node.selectSingleNode("@name").Text
'            nodeValue(i) = node.selectSingleNode("@value").Text
'            i = i + 1
'         Next node
'      End If
'   End If
'
'    Exit Sub
'
'err_Handler:
'        logGeneralError "OPCInterfaceTimed ParseXmlDocument", CStr(Err.Number), Err.Description
'        'frmMain.LoggeneralError "mdlReg.General.GetSettingString", CStr(Err.Number), Err.Description
'End Sub
''===============================================================================
''C:\ApplicationFolders\PressInterfaces\InterfaceAlert.xml
''<Entries>
''      <Entry name="SERVER" value="POLGUELPH02SQL" />
''      <Entry name="DATABASENAME" value="POLYCONINJECTION" />
''      <Entry name="QNAME" value="MAILQUEUE" />
''</Entries>

Private Sub gtParts(ByVal intItemClass As Integer)
    Dim ItemClass As ItemClass, rst As New ADODB.Recordset
    
    On Error GoTo Err_Handler
    
    For Each ItemClass In ItemLst
        If intItemClass = ItemClass.ItemClassIDX Then
            Set rst = rstFilter(rstColours, "ItemClass", ItemClass.ItemClass)
                oDefParent(1).ItemClass = ItemClass.ItemClass
                ctlPartColour.Clear
                If Not oParts.Count = 0 Then Set oParts = Nothing
                Do Until rst.EOF
                    With oParts.Add
                        .ColourDescription = RTrim(rst("PartDescription"))
                        .ItemClass = RTrim(rst("ItemClass"))
                        .PartIDX = rst.AbsolutePosition - 1
                        .PartNumber = RTrim(rst("PartNumber"))
                    End With
                    rst.MoveNext
                Loop
            Exit For
        End If
    Next ItemClass
    
    Set rst = Nothing

    Exit Sub
    
Err_Handler:
    logGeneralError "frmDefectEntry.gtParts", CStr(Err.Number), Err.Description

End Sub

Private Sub ctlItemClass_Click()
    'Height=2430, Left=4920, Top=9240, Width=6690   small image  .BMP
    'Height=3870, Left=120,  Top=8040, Width=10845  large image  .JPG

    Dim rst As New ADODB.Recordset, strItemClass As String
    On Error GoTo err_picLoad
    gtParts ctlItemClass.ItemData(ctlItemClass.ListIndex)
    'ctlImage.Picture = LoadPicture(ImgPath & Mid(oDefParent(1).ItemClass, 2, 2) & ".JPG")
    ctlImage.Picture = LoadPicture(ImgPath & Mid(oDefParent(1).ItemClass, 2, 2) & ".bmp")
    
    'Added on 01-Apr-2008 to prevent loop endlessly by triggering ctlPartSN_LostFocus()
    If ctlPartSN = "" Then
        ctlPartSN.SetFocus
    Else
        txDontScan.SetFocus
        'ctlDontScan.SetFocus
    End If
    
    Set rst = Nothing
    
    Exit Sub
    
err_picLoad:
    logGeneralError "frmDefectEntry.ctlItemClass_Click", CStr(Err.Number), Err.Description
    If Err.Number = 53 Then
        'ctlImage.Picture = LoadPicture(ImgPath & "default.JPG")
        ctlImage.Picture = LoadPicture(ImgPath & "default.bmp")
    End If
    
    txDontScan.SetFocus
    'ctlDontScan.SetFocus
    
End Sub

'Private Sub getPressDesc()
'    pressDesc = GetSettingString(HKEY_LOCAL_MACHINE, SV_KEY_ROOT & "\" & SV_KEY_PRFDATA, SV_VAL_INSPPNT, "")
'End Sub

Private Sub getPressDesc()
    'ctlComputername = GetSettingString(HKEY_LOCAL_MACHINE, SV_KEY_COMPNAMEPATH, SV_VAL_COMPNAME, "")

'    InspDefault = GetSettingString(HKEY_LOCAL_MACHINE, SV_KEY_ROOT & "\" & SV_KEY_PRFDATA, SV_VAL_INSPPNT, "")
'    pressDesc = GetSettingString(HKEY_LOCAL_MACHINE, SV_KEY_ROOT & "\" & SV_KEY_PRFDATA, SV_VAL_INSPPNT, "")
'    MachineID = GetSettingString(HKEY_CURRENT_USER, "SOFTWARE\VB and VBA Program Settings\MoldingOperatorInterface\Properties", "MACHINEID", "")
'    UpdateInspComputerName InspDefault, MachineID, ctlComputername
    InspDefault = InspectionPoint
    pressDesc = pressDesc
    'MachineID = GetSettingString(HKEY_CURRENT_USER, "SOFTWARE\VB and VBA Program Settings\MoldingOperatorInterface\Properties", "MACHINEID", "")
    UpdateInspComputerName InspDefault, machineID, ctlComputername



'MsgBox "getPressDesc " & InspDefault & " " & pressDesc




End Sub

Private Sub ctlTimer_Timer()

    On Error GoTo Err_Handler

    ctlEventDateTime = Now
    
    Exit Sub
    
Err_Handler:
    logGeneralError "frmDefectEntry.ctlTimer_Timer", CStr(Err.Number), Err.Description

End Sub

Private Sub UpdateInspComputerName(ByVal inspPoint As String, ByVal machineID As Integer, ByVal ComputerName As String)
On Error GoTo Err_Handler
    Dim adocmd As New ADODB.Command, oParam As New ADODB.Parameter
    
    adocmd.ActiveConnection = connCurrent
    adocmd.CommandType = adCmdStoredProc
    adocmd.CommandText = "UpdateInspComputerName"
    
    Set oParam = adocmd.CreateParameter("@inspPoint", adVarChar, adParamInput, 50, inspPoint)
    adocmd.Parameters.Append oParam
    Set oParam = adocmd.CreateParameter("@machineID", adInteger, adParamInput, , machineID)
    adocmd.Parameters.Append oParam
    Set oParam = adocmd.CreateParameter("@compName", adVarChar, adParamInput, 50, ComputerName)
    adocmd.Parameters.Append oParam

    adocmd.Execute

    Exit Sub
Err_Handler:
    logGeneralError "mdlMain.UpdateInspComputerName", CStr(Err.Number), Err.Description
    Set adocmd = Nothing
    Set oParam = Nothing
End Sub
