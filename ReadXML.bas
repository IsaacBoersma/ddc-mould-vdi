Attribute VB_Name = "ReadXML"
Public nodeName(14) As String
Public nodeValue(14) As String
Public nodeLength As Integer

Public Sub ParseXmlDocument(path As String)
    Dim doc As New MSXML2.DOMDocument
    Dim success As Boolean
    
    On Error GoTo Err_Handler
    
    success = doc.Load(path)
    If success = False Then
        MsgBox doc.parseError.reason
    Else
        Dim nodeList As MSXML2.IXMLDOMNodeList

        Set nodeList = doc.selectNodes("/Entries/Entry")

        If Not nodeList Is Nothing Then
            Dim node As MSXML2.IXMLDOMNode
            Dim i As Integer
            i = 0
        
            For Each node In nodeList
                ' Could also do node.attributes.getNamedItem("name").text
                nodeName(i) = node.selectSingleNode("@name").Text
                nodeValue(i) = node.selectSingleNode("@value").Text
                i = i + 1
            Next node
         
            nodeLength = i - 1
        End If
    End If
    
    Exit Sub
Err_Handler:
    'logGeneralError "MouldOpIntf ParseXmlDocument " + CStr(Err.Number) + " " + Err.Description
    MsgBox "MouldOpIntf ParseXmlDocument " + CStr(Err.Number) + " " + Err.Description
End Sub

Public Function getNodeValue(nName As String) As String
Dim i As Integer

    For i = 0 To nodeLength
       If nodeName(i) = nName Then
          getNodeValue = nodeValue(i)
       End If
    Next i
End Function


