VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "DefectChild"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
'local variable(s) to hold property value(s)
Private mvarEventID As String 'local copy
Private mvarDefectID As String 'local copy
Private mvarXpos As Integer 'local copy
Private mvarYpos As Integer 'local copy
Private mvarDefectDescriptionID As Integer 'local copy
Private mvarDefectDateTime As Variant 'local copy
Private mvarIPID As Integer 'local copy

Public Property Let IPID(ByVal vData As Integer)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.IPID = 5
    mvarIPID = vData
End Property

Public Property Get IPID() As Integer
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.IPID
    IPID = mvarIPID
End Property

Public Property Let DefectDateTime(ByVal vData As Variant)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.DefectDateTime = 5
    mvarDefectDateTime = vData
End Property

Public Property Set DefectDateTime(ByVal vData As Variant)
'used when assigning an Object to the property, on the left side of a Set statement.
'Syntax: Set x.DefectDateTime = Form1
    Set mvarDefectDateTime = vData
End Property

Public Property Get DefectDateTime() As Variant
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.DefectDateTime
    If IsObject(mvarDefectDateTime) Then
        Set DefectDateTime = mvarDefectDateTime
    Else
        DefectDateTime = mvarDefectDateTime
    End If
End Property

Public Property Let DefectDescriptionID(ByVal vData As Integer)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.DefectDescriptionID = 5
    mvarDefectDescriptionID = vData
End Property

Public Property Get DefectDescriptionID() As Integer
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.DefectDescriptionID
    DefectDescriptionID = mvarDefectDescriptionID
End Property

Public Property Let Ypos(ByVal vData As Integer)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.Ypos = 5
    mvarYpos = vData
End Property

Public Property Get Ypos() As Integer
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.Ypos
    Ypos = mvarYpos
End Property

Public Property Let Xpos(ByVal vData As Integer)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.Xpos = 5
    mvarXpos = vData
End Property

Public Property Get Xpos() As Integer
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.Xpos
    Xpos = mvarXpos
End Property

Public Property Let DefectID(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.DefectID = 5
    mvarDefectID = vData
End Property

Public Property Get DefectID() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.DefectID
    DefectID = mvarDefectID
End Property

Public Property Let EventID(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.EventID = 5
    mvarEventID = vData
End Property

Public Property Get EventID() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.EventID
    EventID = mvarEventID
End Property



