VERSION 5.00
Begin VB.Form frmSplash 
   BorderStyle     =   3  'Fixed Dialog
   ClientHeight    =   5625
   ClientLeft      =   255
   ClientTop       =   1410
   ClientWidth     =   9735
   ClipControls    =   0   'False
   ControlBox      =   0   'False
   Icon            =   "frmSplash.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form2"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   5625
   ScaleWidth      =   9735
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.Timer Timer1 
      Enabled         =   0   'False
      Interval        =   500
      Left            =   8640
      Top             =   720
   End
   Begin VB.Label Label1 
      BackStyle       =   0  'Transparent
      Caption         =   "Loading...   Please Wait"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   375
      Left            =   5400
      TabIndex        =   1
      Top             =   2040
      Width           =   3375
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H00FF0000&
      BackStyle       =   1  'Opaque
      Height          =   195
      Index           =   20
      Left            =   8280
      Top             =   2040
      Visible         =   0   'False
      Width           =   105
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H00FF0000&
      BackStyle       =   1  'Opaque
      Height          =   195
      Index           =   19
      Left            =   8160
      Top             =   2040
      Visible         =   0   'False
      Width           =   105
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H00FF0000&
      BackStyle       =   1  'Opaque
      Height          =   195
      Index           =   18
      Left            =   8040
      Top             =   2040
      Visible         =   0   'False
      Width           =   105
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H00FF0000&
      BackStyle       =   1  'Opaque
      Height          =   195
      Index           =   17
      Left            =   7920
      Top             =   2040
      Visible         =   0   'False
      Width           =   105
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H00FF0000&
      BackStyle       =   1  'Opaque
      Height          =   195
      Index           =   16
      Left            =   7800
      Top             =   2040
      Visible         =   0   'False
      Width           =   105
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H00FF0000&
      BackStyle       =   1  'Opaque
      Height          =   195
      Index           =   15
      Left            =   7680
      Top             =   2040
      Visible         =   0   'False
      Width           =   105
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H00FF0000&
      BackStyle       =   1  'Opaque
      Height          =   195
      Index           =   14
      Left            =   7560
      Top             =   2040
      Visible         =   0   'False
      Width           =   105
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H00FF0000&
      BackStyle       =   1  'Opaque
      Height          =   195
      Index           =   13
      Left            =   7440
      Top             =   2040
      Visible         =   0   'False
      Width           =   105
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H00FF0000&
      BackStyle       =   1  'Opaque
      Height          =   195
      Index           =   12
      Left            =   7320
      Top             =   2040
      Visible         =   0   'False
      Width           =   105
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H00FF0000&
      BackStyle       =   1  'Opaque
      Height          =   195
      Index           =   11
      Left            =   7200
      Top             =   2040
      Visible         =   0   'False
      Width           =   105
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H00FF0000&
      BackStyle       =   1  'Opaque
      Height          =   195
      Index           =   10
      Left            =   7080
      Top             =   2040
      Visible         =   0   'False
      Width           =   105
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H00FF0000&
      BackStyle       =   1  'Opaque
      Height          =   195
      Index           =   9
      Left            =   6960
      Top             =   2040
      Visible         =   0   'False
      Width           =   105
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H00FF0000&
      BackStyle       =   1  'Opaque
      Height          =   195
      Index           =   8
      Left            =   6840
      Top             =   2040
      Visible         =   0   'False
      Width           =   105
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H00FF0000&
      BackStyle       =   1  'Opaque
      Height          =   195
      Index           =   7
      Left            =   6720
      Top             =   2040
      Visible         =   0   'False
      Width           =   105
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H00FF0000&
      BackStyle       =   1  'Opaque
      Height          =   195
      Index           =   6
      Left            =   6600
      Top             =   2040
      Visible         =   0   'False
      Width           =   105
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H00FF0000&
      BackStyle       =   1  'Opaque
      Height          =   195
      Index           =   5
      Left            =   6480
      Top             =   2040
      Visible         =   0   'False
      Width           =   105
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H00FF0000&
      BackStyle       =   1  'Opaque
      Height          =   195
      Index           =   4
      Left            =   6360
      Top             =   2040
      Visible         =   0   'False
      Width           =   105
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H00FF0000&
      BackStyle       =   1  'Opaque
      Height          =   195
      Index           =   3
      Left            =   6240
      Top             =   2040
      Visible         =   0   'False
      Width           =   105
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H00FF0000&
      BackStyle       =   1  'Opaque
      Height          =   195
      Index           =   2
      Left            =   6120
      Top             =   2040
      Visible         =   0   'False
      Width           =   105
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H00FF0000&
      BackStyle       =   1  'Opaque
      Height          =   195
      Index           =   1
      Left            =   6000
      Top             =   2040
      Visible         =   0   'False
      Width           =   105
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H00FF0000&
      BackStyle       =   1  'Opaque
      Height          =   195
      Index           =   0
      Left            =   5880
      Top             =   2040
      Visible         =   0   'False
      Width           =   105
   End
   Begin VB.Label lblVersion 
      Alignment       =   1  'Right Justify
      BackStyle       =   0  'Transparent
      Caption         =   "Label1"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   5880
      TabIndex        =   0
      Top             =   2760
      Width           =   3015
   End
   Begin VB.Image Image1 
      Height          =   5415
      Left            =   120
      Top             =   120
      Width           =   9495
   End
End
Attribute VB_Name = "frmSplash"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Option Explicit
'Private xcntr As Integer

Private Sub Form_Load()

    On Error GoTo err_Handler
    
'    Timer1.Enabled = True
    
    Image1.Picture = LoadResPicture("DECOMA", vbResBitmap)
    lblVersion.Caption = "Version " & App.Major & "." & App.Minor & "." & App.Revision
    
    Exit Sub
    
err_Handler:
    
    logGeneralError "frmSplash.Form.Form_Load", CStr(Err.Number), Err.Description

End Sub

Private Sub Timer1_Timer()

'    DoEvents
'
'    If xcntr = Shape1.Count - 1 Then xcntr = 0
'
'        Shape1.Item(xcntr).BackColor = &HFF00&
'
'        Select Case xcntr
'            Case 0
'                Shape1.Item(Shape1.Count - 1).BackColor = &HFF0000
'            Case Else
'                Shape1.Item(xcntr - 1).BackColor = &HFF0000
'        End Select
'
'        xcntr = xcntr + 1
'
End Sub
