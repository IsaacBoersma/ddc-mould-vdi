Attribute VB_Name = "mdlCheckNetwork"

Public Declare Function InternetGetConnectedState Lib "wininet" _
  (ByRef dwflags As Long, _
   ByVal dwReserved As Long) As Long
Public Const INTERNET_CONNECTION_LAN As Long = &H2

Public Function IsNetConnectViaLAN() As Boolean
   Dim dwflags As Long
   
   On Error GoTo err_Handler
   
   Call InternetGetConnectedState(dwflags, 0&)
   IsNetConnectViaLAN = dwflags And INTERNET_CONNECTION_LAN
   
   Exit Function

err_Handler:
    
    logGeneralError "mdlCheckNetwork.IsNetConnectViaLAN", CStr(Err.Number), Err.Description

End Function

Public Function IsNetConnectOnline() As Boolean

    On Error GoTo err_Handler
    
    IsNetConnectOnline = InternetGetConnectedState(0&, 0&)
    
    Exit Function
    
err_Handler:
    
    logGeneralError "mdlCheckNetwork.IsNetConnectOnline", CStr(Err.Number), Err.Description

End Function


