Attribute VB_Name = "mdlReg"
Public Declare Function RegCloseKey Lib "advapi32.dll" _
(ByVal hkey As Long) As Long

Public Declare Function RegCreateKey Lib "advapi32.dll" _
Alias "RegCreateKeyA" (ByVal hkey As Long, ByVal lpSubKey _
As String, phkResult As Long) As Long

Public Declare Function RegDeleteKey Lib "advapi32.dll" _
Alias "RegDeleteKeyA" (ByVal hkey As Long, ByVal lpSubKey _
As String) As Long

Public Declare Function RegDeleteValue Lib "advapi32.dll" _
Alias "RegDeleteValueA" (ByVal hkey As Long, ByVal _
lpValueName As String) As Long

Public Declare Function RegOpenKey Lib "advapi32.dll" _
Alias "RegOpenKeyA" (ByVal hkey As Long, ByVal lpSubKey _
As String, phkResult As Long) As Long

Public Declare Function RegQueryValueEx Lib "advapi32.dll" _
Alias "RegQueryValueExA" (ByVal hkey As Long, ByVal lpValueName _
As String, ByVal lpReserved As Long, lpType As Long, lpData _
As Any, lpcbData As Long) As Long

Public Declare Function RegSetValueEx Lib "advapi32.dll" _
Alias "RegSetValueExA" (ByVal hkey As Long, ByVal _
lpValueName As String, ByVal Reserved As Long, ByVal _
dwType As Long, lpData As Any, ByVal cbData As Long) As Long

Public Declare Function RegEnumValue Lib "advapi32.dll" Alias "RegEnumValueA" _
(ByVal hkey As Long, ByVal dwIndex As Long, ByVal lpValueName As String, _
lpcbValueName As Long, ByVal lpReserved As Long, lpType As Long, _
lpData As Byte, lpcbData As Long) As Long

Public Const REG_SZ = 1 'Unicode nul terminated string
Public Const REG_BINARY = 3 'Free form binary
Public Const REG_DWORD = 4 '32-bit number
Public Const ERROR_SUCCESS = 0&

Public Const HKEY_CLASSES_ROOT = &H80000000
Public Const HKEY_CURRENT_USER = &H80000001
Public Const HKEY_LOCAL_MACHINE = &H80000002
Public Const HKEY_USERS = &H80000003
Public Const HKEY_CURRENT_CONFIG = &H80000005
Public Const HKEY_DYN_DATA = &H80000006

Public Const SV_KEY_ROOT = "Software\DefectDataCollection"
Public Const SV_KEY_PRFDATA = "MouldProfile"
Public Const SV_VAL_SVRNAME = "ServerName"
Public Const SV_VAL_DBSNAME = "DatabaseName"
Public Const SV_VAL_CONNSTR = "SQLConnectionString"
Public Const SV_VAL_SQLTOUT = "ConnectionTimeout"
Public Const SV_VAL_CONSTR2 = "IMMConnectionString"
Public Const SV_VAL_INSPPNT = "DefaultInspectionPoint"
Public Const SV_VAL_IMGPATH = "ImagePath"
Public Const SV_VAL_DATMASK = "DataMask"
Public Const SV_VAL_MLDPRES = "MouldPress"
Public Const SV_VAL_CTLLOCK = "LockEnabled"
Public Const SV_VAL_DISPCHG = "DispositionOverriding"
Public Const SV_VAL_SNDSCAN = "SendScanOverSerial"
Public Const SV_VAL_SRLPORT = "UseSerialPort"
Public Const SV_VAL_REMCLNT = "RemoteClient"
Public Const SV_VAL_REMSERV = "RemoteServerName"
Public Const SV_VAL_REMDBNM = "RemoteDatabaseName"
Public Const SV_VAL_DEFPROF = "DefectProfile"
Public Const SV_VAL_SNDORCL = "SendDataToOracle"
Public Const SV_VAL_COMPNAME = "ComputerName"
Public Const SV_KEY_COMPNAMEPATH = "SYSTEM\CurrentControlSet\Control\ComputerName\ComputerName"

Public Function GetSettingString(hkey As Long, _
strpath As String, strValue As String, Optional _
Default As String, Optional lErr As Long) As String

    Dim hCurKey As Long
    Dim lValueType As Long
    Dim strBuffer As String
    Dim lDataBufferSize As Long
    Dim intZeroPos As Integer
    Dim lRegResult As Long
    
    On Error GoTo err_Handler
    
    If Not IsEmpty(Default) Then
        GetSettingString = Default
    Else
        GetSettingString = ""
    End If
    
    lRegResult = RegOpenKey(hkey, strpath, hCurKey)
    lRegResult = RegQueryValueEx(hCurKey, strValue, 0&, _
    lValueType, ByVal 0&, lDataBufferSize)
    
    If lRegResult = ERROR_SUCCESS Then
        If lValueType = REG_SZ Then
            strBuffer = String(lDataBufferSize, " ")
            lResult = RegQueryValueEx(hCurKey, strValue, 0&, 0&, _
                    ByVal strBuffer, lDataBufferSize)
            intZeroPos = InStr(strBuffer, Chr$(0))
            If intZeroPos > 0 Then
                GetSettingString = Left$(strBuffer, intZeroPos - 1)
            Else
                GetSettingString = strBuffer
            End If
        End If
    Else
        lErr = lRegResult
        lRegResult = RegCloseKey(hCurKey)
    End If
    Exit Function

err_Handler:
    
    logGeneralError "mdlReg.General.GetSettingString", CStr(Err.Number), Err.Description

End Function

Public Sub SaveSettingString(hkey As Long, strpath _
As String, strValue As String, strData As String)
    Dim hCurKey As Long
    Dim lRegResult As Long
    
    On Error GoTo err_Handler
    
    lRegResult = RegCreateKey(hkey, strpath, hCurKey)
    lRegResult = RegSetValueEx(hCurKey, strValue, 0, REG_SZ, _
    ByVal strData, Len(strData))
    
    If lRegResult <> ERROR_SUCCESS Then
    'there is a problem, you'll probably want to handle this from calling function
    End If
    
    lRegResult = RegCloseKey(hCurKey)
    Exit Sub

err_Handler:
    
    logGeneralError "mdlReg.General.SaveSettingString", CStr(Err.Number), Err.Description

End Sub

Public Function GetSettingLong(ByVal hkey As Long, _
ByVal strpath As String, ByVal strValue As String, _
Optional Default As Long) As Long

    Dim lRegResult As Long
    Dim lValueType As Long
    Dim lBuffer As Long
    Dim lDataBufferSize As Long
    Dim hCurKey As Long
    
    On Error GoTo err_Handler
    
    'Set up default value
    If Not IsEmpty(Default) Then
    GetSettingLong = Default
    Else
    GetSettingLong = 0
    End If
    
    lRegResult = RegOpenKey(hkey, strpath, hCurKey)
    lDataBufferSize = 4 '4 bytes = 32 bits = long
    
    lRegResult = RegQueryValueEx(hCurKey, strValue, 0&, _
    lValueType, lBuffer, lDataBufferSize)
    
    If lRegResult = ERROR_SUCCESS Then
    
        If lValueType = REG_DWORD Then
            GetSettingLong = lBuffer
        End If
        
    Else
    'there is a problem, you'll probably want to handle this from calling function
    End If
    
    lRegResult = RegCloseKey(hCurKey)
    Exit Function
    
err_Handler:
    
    logGeneralError "mdlReg.General.GetSettingLong", CStr(Err.Number), Err.Description

End Function

Public Sub SaveSettingLong(ByVal hkey As Long, ByVal _
strpath As String, ByVal strValue As String, ByVal _
lData As Long)

    Dim hCurKey As Long
    Dim lRegResult As Long
    
    On Error GoTo err_Handler
    
    lRegResult = RegCreateKey(hkey, strpath, hCurKey)
    
    lRegResult = RegSetValueEx(hCurKey, strValue, 0&, _
    REG_DWORD, lData, 4)
    
    If lRegResult <> ERROR_SUCCESS Then
    'there is a problem, you'll probably want to handle this from calling function
    End If
    
    lRegResult = RegCloseKey(hCurKey)
    Exit Sub

err_Handler:
    
    logGeneralError "mdlReg.General.SaveSettingLong", CStr(Err.Number), Err.Description

End Sub

Public Sub DeleteValue(ByVal hkey As Long, ByVal strpath As String, ByVal strValue As String)

    Dim hCurKey As Long
    Dim lRegResult As Long
    
    On Error GoTo err_Handler
    
    lRegResult = RegOpenKey(hkey, strpath, hCurKey)
    lRegResult = RegDeleteValue(hCurKey, strValue)
    lRegResult = RegCloseKey(hCurKey)
    Exit Sub

err_Handler:
    
    logGeneralError "mdlReg.General.DeleteValue", CStr(Err.Number), Err.Description

End Sub


