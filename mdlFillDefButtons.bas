Attribute VB_Name = "mdlFillDefButtons"
Public Sub FillDefectButtons()
    Dim rst As New ADODB.Recordset, strsql As String
    Dim cnt As Integer
    
    On Error GoTo err_Handler
    
    
    strsql = "select dt.DefectTypeID as DTypeID, dt.defecttype as DefTypedescrip,dg.defectGroupDescription DefGrpDescrip, dd.DefectDescriptionID DefID,"
    strsql = strsql & "dd.DefectDescription DefDescrip from tbldefectdescriptions dd, tblDefecttypes dt, tblDefectGroup dg where dt.defecttypeid=dd.defecttypeid and "
    strsql = strsql & "dg.defectgroupid=dd.defectgroup and defectGroupID =1 and dd.include=1 and defectType='" & ProfileString & "' order by 5"

    'strsql = "select dt.DefectTypeID as DTypeID, dt.defecttype as DefTypedescrip,dg.defectGroupDescription DefGrpDescrip, dd.DefectDescriptionID DefID,"
    'strsql = strsql & "dd.DefectDescription DefDescrip from tbldefectdescriptions dd, tblDefecttypes dt, tblDefectGroup dg where dt.defecttypeid=dd.defecttypeid and "
    'strsql = strsql & "dg.defectgroupid=dd.defectgroup and dt.defectTypeID in (1,6) and defectGroupID =1 and dd.include=1 and RIGHT(defectdescription,4) <> ' RIM' order by 4"
    
    'strsql = "select dt.DefectTypeID as DTypeID, dt.defecttype as DefTypedescrip, dg.defectGroupDescription DefGrpDescrip, dd.DefectDescriptionID DefID, dd.DefectDescription DefDescrip " _
    '    & "from tbldefectdescriptions dd, tblDefecttypes dt, tblDefectGroup dg where dt.defecttypeid=dd.defecttypeid and " _
    '    & "dg.defectgroupid=dd.defectgroup and dt.defectType=" & "'IMM'" & " order by 5"

    rst.Open strsql, connCurrent

    cnt = 0
    Do Until rst.EOF
        frmDefectEntry.DefectButtons(cnt).Caption = rst(4).Value
        defIDArray(cnt) = rst(3).Value
        defTypeDescArray(cnt) = rst(1).Value
        cnt = cnt + 1
        rst.MoveNext
    Loop

   For i = cnt To frmDefectEntry.DefectButtons.Count - 1
      frmDefectEntry.DefectButtons(i).Visible = False
   Next
   
  ' rst = Null
      
   Exit Sub

err_Handler:
    logGeneralError "ModuleFillDefectButtons.General.mdlFillDefButtons", CStr(Err.Number), Err.Description
End Sub


