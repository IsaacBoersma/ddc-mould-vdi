Attribute VB_Name = "MatchPartialAppName"
'****************ADD THE FOLLOWING TO PREVENT MULTIPLE INSTANCES OF SAME APP. BY JJ 14-AUG-2013 *******************

Option Explicit
Option Compare Text

Private Declare Function EnumWindows Lib "user32" (ByVal lpEnumFunc As Long, ByVal lParam As Long) As Long
Private Declare Function GetWindowText Lib "user32" Alias "GetWindowTextA" (ByVal hwnd As Long, ByVal lpString As String, ByVal cch As Long) As Long

Private psAppNameContains As String
Private pbFound As Boolean
Private sTitle As String

Public Function AppPartialStringFound(partialString As String) As Boolean
Dim lRet As Long

    psAppNameContains = partialString
    lRet = EnumWindows(AddressOf CheckForInstance, 0)

    AppPartialStringFound = pbFound
    'Reset
    pbFound = False
End Function

Public Function retFullAppTitle(partialString As String, ByRef fullTitle As String) As Boolean
Dim lRet As Long

    psAppNameContains = partialString
    lRet = EnumWindows(AddressOf CheckForInstance, 0)

    fullTitle = sTitle
    retFullAppTitle = pbFound
    
    'Reset
    pbFound = False
End Function

Private Function CheckForInstance(ByVal lhWnd As Long, ByVal lParam As Long) As Long
Dim lRet As Long
Dim iNew As Integer

    If Trim(psAppNameContains = "") Then
        CheckForInstance = False
        Exit Function
    End If

    sTitle = Space(255)
    lRet = GetWindowText(lhWnd, sTitle, 255)

    sTitle = StripNull(sTitle)
    If InStr(sTitle, psAppNameContains) > 0 Then
        'We're done, stop looking
        CheckForInstance = False
        pbFound = True
    Else
        CheckForInstance = True
    End If
End Function

Private Function StripNull(ByVal InString As String) As String
'Input: String containing null terminator (Chr(0))
'Returns: all character before the null terminator

Dim iNull As Integer
    If Len(InString) > 0 Then
        iNull = InStr(InString, vbNullChar)
        Select Case iNull
        Case 0
            StripNull = InString
        Case 1
            StripNull = ""
        Case Else
            StripNull = Left$(InString, iNull - 1)
        End Select
    End If
End Function

