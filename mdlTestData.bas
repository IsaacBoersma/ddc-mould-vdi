Attribute VB_Name = "mdlTestData"
Private oParts As New Parts
'Private oDefDefs As New DefectDefinitions
Private oDefParents As New DefectParents
Private oDefChildren As New DefectChildren
Private InspPoints() As String
Private DefMax As Integer
Private DefIdx As Integer
Private PrtMax As Integer
Private PrtIdx As Integer
Private dForward As Integer
Private snDigit(7) As Integer
Private disp(5) As String

'Public Sub genTestData()
'    disp(1) = "Override": disp(2) = "Repair": disp(3) = "Rework": disp(4) = "Scrap": disp(5) = "Ok"
'    snDigit(1) = 0: snDigit(2) = 0: snDigit(3) = 0: snDigit(4) = 2: snDigit(5) = 0: snDigit(6) = 0: snDigit(7) = 0
'    dForward = 100
'    popoParts
''    popoDefDefs
'    popInspPoints
'    popEventsAndDefects
'    popEventRecordset
''    popDefectRecordSet
'End Sub
'Private Sub popEventRecordset()
'    Dim strsql As String, rst As New ADODB.Recordset
'    strsql = "SELECT * FROM tblDefectMaster"
'    rst.CursorLocation = adUseClient
'    rst.CursorType = adOpenDynamic
'    rst.LockType = adLockBatchOptimistic
'    rst.Open strsql, connCurrent
'    For Each Item In oDefParents
'        rst.AddNew
'        rst("Disposition") = Item.Disposition
'        rst("EventDateTime") = Item.EventDateTime
'        rst("EventID") = Item.EventID
'        rst("InspectionPoint") = Item.InspectionPoint
'        rst("LotNum") = Item.LotNum
'        rst("PartNumber") = Item.PartNumber
'        rst("PartColour") = Item.PartColour
'        rst("ItemClass") = Item.ItemClass
'        rst("PartSN") = Item.PartSN
'        rst("Shift") = ""
'        rst("Comment") = ""
'    Next Item
'    rst.UpdateBatch
'End Sub
'Private Sub popDefectRecordSet()
'    Dim strsql As String, rst As New ADODB.Recordset
'    strsql = "SELECT * FROM tblDefectSubItems"
'    rst.CursorLocation = adUseClient
'    rst.CursorType = adOpenDynamic
'    rst.LockType = adLockBatchOptimistic
'    rst.Open strsql, connCurrent
'    For Each Item In oDefChildren
'        rst.AddNew
'        rst("DefectDateTime") = Item.DefectDateTime
'        rst("DefectID") = Item.DefectID
'        rst("DefectType") = Item.DefectType
'        rst("DefectName") = Item.DefectName
'        rst("EventID") = Item.EventID
'        rst("InspectionPoint") = Item.InspectionPoint
'        rst("Xpos") = Item.Xpos
'        rst("Ypos") = Item.Ypos
'    Next Item
'    rst.UpdateBatch
'End Sub
'Private Sub popEventsAndDefects()
'    Dim sncntr As Long, defcntr As Integer, defnum As Integer
'    For sncntr = 1 To 50000
'        defnum = (CInt(Rnd() * 10)) + 1
'        With oDefParents.Add
'            .Disposition = rndDisp
'            .EventDateTime = rndDateTime
'            .EventID = uniqID
'            .InspectionPoint = rndInspPoint
'            .LotNum = rndLotNum
'            .PartNumber = rndPrt
'            .PartColour = rndColourDescription
'            .ItemClass = rndItemClass
'            .PartSN = nxtSN
'            For defcntr = 1 To defnum
'                With oDefChildren.Add
'                    .DefectDateTime = rndDateTime
'                    .DefectID = defcntr
'                    .DefectType = rndDefectType
'                    .DefectName = rndDefectName
'                    .EventID = oDefParents(sncntr).EventID
'                    .InspectionPoint = rndInspPoint
'                    .Xpos = rndX
'                    .Ypos = rndY
'                End With
'            Next defcntr
'        End With
'    Next sncntr
'End Sub
'Private Sub popoParts()
'    Dim strsql As String, rst As New ADODB.Recordset
'    strsql = "SELECT * FROM vwColours"
'    rst.CursorLocation = adUseClient
'    rst.CursorType = adOpenDynamic
'    rst.LockType = adLockBatchOptimistic
'    rst.Open strsql, connCurrent
'    Do Until rst.EOF
'        With oParts.Add
'            .ColourDescription = rst("ColourDescription")
'            .ItemClass = rst("ItemClass")
'            .PartIDX = rst.AbsolutePosition
'            .PartNumber = rst("PartNumber")
'        End With
'        PrtMax = PrtMax + 1
'        rst.MoveNext
'    Loop
'End Sub
'Private Sub popoDefDefs()
'    Dim strsql As String, rst As New ADODB.Recordset
'    strsql = "SELECT * FROM tblDefectTypes"
'    rst.CursorLocation = adUseClient
'    rst.CursorType = adOpenDynamic
'    rst.LockType = adLockBatchOptimistic
'    rst.Open strsql, connCurrent
'    Do Until rst.EOF
'        With oDefDefs.Add
'            .DefectDefIDX = rst.AbsolutePosition
'            .DefectDescription = rst("DefectDescription")
'            .DefectType = rst("DefectType")
'        End With
'        DefMax = DefMax + 1
'        rst.MoveNext
'    Loop
'End Sub
'Private Sub popInspPoints()
'    Dim strsql As String, rst As New ADODB.Recordset
'    strsql = "SELECT * FROM tblInspectionPoints"
'    rst.CursorLocation = adUseClient
'    rst.CursorType = adOpenDynamic
'    rst.LockType = adLockBatchOptimistic
'    rst.Open strsql, connCurrent
'    ReDim InspPoints(rst.RecordCount)
'    Do Until rst.EOF
'        InspPoints(rst("IPID")) = rst("InspectionPoint")
'        rst.MoveNext
'    Loop
'End Sub
'Private Function rndLotNum() As Integer
'    rndLotNum = Format(CInt(Rnd() * 100), "000")
'End Function
'Private Function rndPrt() As String
'    PrtIdx = CInt(Rnd() * (PrtMax - 1)) + 1
'    rndPrt = oParts(PrtIdx).PartNumber
'End Function
'Private Function rndItemClass() As String
'    rndItemClass = oParts(PrtIdx).ItemClass
'End Function
'Private Function rndColourDescription() As String
'    rndColourDescription = oParts(PrtIdx).ColourDescription
'End Function
'Private Function uniqID() As String
'    uniqID = GetGUID
'End Function
'Private Function rndInspPoint() As String
'    rndInspPoint = InspPoints(CInt(Rnd() * (UBound(InspPoints) - 1)) + 1)
'End Function
'Private Function rndDateTime() As Date
'    rndDateTime = Now + (CInt(Rnd() * dForward))
'    rndDateTime = rndDateTime + (CInt(Rnd() * 1440) / 1440)
'End Function
'Private Function rndDefectType() As String
'    DefIdx = CInt(Rnd() * (DefMax - 1)) + 1
'    rndDefectType = oDefDefs(DefIdx).DefectType
'End Function
'Private Function rndDefectName() As String
'    rndDefectName = oDefDefs(DefIdx).DefectDescription
'End Function
'Private Function rndDisp() As String
'    rndDisp = disp((CInt(Rnd() * 4)) + 1)
'End Function
'Private Function rndX() As Integer
'    Randomize
'    rndX = CInt(Rnd() * 4600) + 220
'End Function
'Private Function rndY() As Integer
'    Randomize
'    rndY = CInt(Rnd() * 1400) + 700
'End Function
'Private Function nxtSN() As String
'    Dim cntr As Integer
'    For cntr = 1 To 6
'        If snDigit(cntr) = 26 Then
'            snDigit(cntr + 1) = snDigit(cntr + 1) + 1
'            snDigit(cntr) = 0
'        End If
'    Next cntr
'        If snDigit(7) = 26 Then
'            For cntr = 1 To 7
'                snDigit(cntr) = 0
'            Next cntr
'        End If
'    nxtSN = Chr(snDigit(7) + 65) & Chr(snDigit(6) + 65) & Chr(snDigit(5) + 65) & Chr(snDigit(4) + 65) & Chr(snDigit(3) + 65) & Chr(snDigit(2) + 65) & Chr(snDigit(1) + 65)
'    snDigit(1) = snDigit(1) + 1
'End Function
