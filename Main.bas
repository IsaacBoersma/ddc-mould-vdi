Attribute VB_Name = "mdlMain"
Option Explicit

Public connCurrent As New ADODB.Connection
Public connPartsID As New ADODB.Connection
Private connMain(2) As New ADODB.Connection
Public rstItems As New ADODB.Recordset
Public rstColours As New ADODB.Recordset
Public rstDefectTypes As New ADODB.Recordset
Public rstDefectDescriptions As New ADODB.Recordset
Public defIDArray(48) As Integer
Public defTypeDescArray(48) As String
Private connTO As Long
Public connStr As String
Public connStr2 As String
Public connState As Integer
Public ImgPath As String
Public DataMask As Integer
Public LockEnabled As Integer
Public mouldPress As Integer ''
Public OverrideOK As Integer
Public ItemLst As New ItemClasses
Public PartLst As New Parts
Public oDefectTypes As New DefectTypes
Public oDefectDescs As New DefectDescriptions
Public RefreshOK As Boolean
Public RetryCount As Integer
Public SendScan As Integer ''
Public SendPort As Integer ''
Public ClientIsRemote As Boolean
Public DefProfile As Integer
Public ProfileString As String

Public Enum ChildDefProperties
    itmDefectType = 1
    itmDefectName = 2
    itmXpos = 3
    itmYpos = 4
    itmNew = 5
    itmDefectDateTime = 6
    itmEventID = 7
    itmInspectionPoint = 8
    itmDefectID = 9
    itmDefectDescriptionID = 10
    itmIPID = 11
End Enum

Public SendDataToOracle As Integer ''
Public tempCaption As String

Public Sub Main()
    Dim strServerName As String, strDatabaseName As String
    Dim oWait As New CheckForServer.mdlMain
    
    'Dim args() As String
    'args = Split(Command$, " ")
    'frmDefectEntry.loginName = args(0)
    'frmDefectEntry.pressSetupFolder = args(1)
    frmDefectEntry.pressSetupFolder = Trim(Command$)    'XML Config File Location.
    
    If Left(frmDefectEntry.pressSetupFolder, 1) = """" Then
        frmDefectEntry.pressSetupFolder = Mid(frmDefectEntry.pressSetupFolder, 2, Len(frmDefectEntry.pressSetupFolder) - 1)
    End If
    If Right(frmDefectEntry.pressSetupFolder, 1) = """" Then
        frmDefectEntry.pressSetupFolder = Mid(frmDefectEntry.pressSetupFolder, 1, Len(frmDefectEntry.pressSetupFolder) - 1)
    End If

    ReadXML.ParseXmlDocument (frmDefectEntry.pressSetupFolder)

    frmDefectEntry.machineID = CInt(ReadXML.getNodeValue("MachineID"))
    frmDefectEntry.pressID = CInt(ReadXML.getNodeValue("PressID"))
    frmDefectEntry.InspectionPoint = ReadXML.getNodeValue("InspectionPoint")
    frmDefectEntry.ComputerName = ReadXML.getNodeValue("ComputerName")
    frmDefectEntry.DefectProfile = ReadXML.getNodeValue("DefectProfile")
    frmDefectEntry.DispositionOverriding = CInt(ReadXML.getNodeValue("DispositionOverriding"))
    frmDefectEntry.LockEnabled = CInt(ReadXML.getNodeValue("LockEnabled"))
    frmDefectEntry.ImagePath = ReadXML.getNodeValue("ImagePath")
    frmDefectEntry.ServerName = ReadXML.getNodeValue("ServerName")
    frmDefectEntry.DatabaseName = ReadXML.getNodeValue("DatabaseName")
    frmDefectEntry.PartsIDConnStr = ReadXML.getNodeValue("PartsIDConnStr")
    frmDefectEntry.DDCMouldConnStr = ReadXML.getNodeValue("DDCMouldConnStr")
    
    frmDefectEntry.appName = App.EXEName

    Dim remoteClient As Integer
    remoteClient = 0
    
    On Error GoTo Err_Handler

    If App.PrevInstance = True Then
        MsgBox "This program is already running please check your taskbar and use the current instance.", vbOKOnly, "Program Already Running"
        Exit Sub
    End If
    
    SendDataToOracle = 0
    
    If remoteClient = 1 Then
        oWait.OpenForm
        DoEvents
        ClientIsRemote = True
        If RemoteServerPresent = True Then
            oWait.CloseForm
            ShowConnectedToRemote
        End If
        oWait.CloseForm
    Else
        ClientIsRemote = False
    End If
    
    strServerName = frmDefectEntry.ServerName
    strDatabaseName = frmDefectEntry.DatabaseName

    connStr = frmDefectEntry.DDCMouldConnStr
    connStr2 = frmDefectEntry.PartsIDConnStr

    frmSplash.Show
    
    DoEvents

    ImgPath = frmDefectEntry.ImagePath & IIf(Right(frmDefectEntry.ImagePath, 1) <> "\", "\", "")
    DataMask = 495
    connTO = 2
    'LockEnabled = 1
    OverrideOK = frmDefectEntry.DispositionOverriding
    SendScan = 0
    SendPort = 0
    mouldPress = 0

    bldConnections

    retItemClasses
    
    retParts
    
    retDefectProfile
    
    retDefectTypes
            
    retDefects
    
    RetryCount = 0
    
    frmDefectEntry.Show
    
    mdlFillDefButtons.FillDefectButtons
    
    tempCaption = frmDefectEntry.DefectButtons(0).Caption

    Unload frmSplash
    
    
    Exit Sub
Err_Handler:
    logGeneralError "mdlMain.Main", CStr(Err.Number), Err.Description
End Sub

Private Sub ShowConnectedToRemote()
    frmRemoteServerConnected.Show vbModal
End Sub

Private Function RemoteServerPresent() As Boolean
    Dim RemServer As String, RemDatabase As String, RemConnStr As String, RemConn As New ADODB.Connection, RemConnADOErrors As ADODB.Errors

    On Error GoTo Err_Handler
    
    RemServer = "172.18.26.213"
    RemDatabase = "DDC"

    RemConnStr = "Provider=SQLNCLI.1;Persist Security Info=True;User " _
                & "ID=DDCApp;PWD=keRrTRN3;Initial Catalog=" & RemDatabase & ";Data Source=" _
                & RemServer

    RemConn.ConnectionString = RemConnStr
    RemConn.Open

    RemoteServerPresent = True

    RemConn.Close


    Exit Function
Err_Handler:
    Set RemConnADOErrors = RemConn.Errors
    If RemConnADOErrors.Count > 0 Then RemoteServerPresent = False
    If RemConn.State = adStateOpen Then RemConn.Close
End Function

Public Sub retItemClasses()
    Dim strsql As String, adocmd As New ADODB.Command, temprstItems As New ADODB.Recordset
    Dim oParam As New ADODB.Parameter
    
    On Error GoTo Err_Handler
    
    Select Case ClientIsRemote
        Case False
            strsql = "SELECT DISTINCT TOP 100 PERCENT tbl1.ItemClass, tbl1.ItemDescription, LEFT(tbl1.ItemClass, 1) " _
                    & "AS Expr1 FROM SQLSERVER.PolyconDW.dbo.tblItemClasses tbl1 inner join SQLSERVER.PolyconDW.dbo.tblParts tbl2 " _
                    & "on tbl1.ItemClass = tbl2.ItemClass where left(tbl1.ItemClass,1)='1' ORDER BY LEFT(tbl1.ItemClass, 1) DESC, tbl1.ItemDescription"
                    
        Case True
            strsql = "SELECT DISTINCT TOP 100 PERCENT tbl1.ItemClass, tbl1.ItemDescription, LEFT(tbl1.ItemClass, 1) " _
                    & "AS Expr1 FROM PolyconDWRemote.dbo.tblItemClasses tbl1 inner join PolyconDWRemote.dbo.tblParts tbl2 " _
                    & "on tbl1.ItemClass = tbl2.ItemClass ORDER BY LEFT(tbl1.ItemClass, 1) DESC, tbl1.ItemDescription"
    End Select
    
    temprstItems.CursorLocation = adUseClient
    temprstItems.CursorType = adOpenStatic
    temprstItems.LockType = adLockReadOnly
    temprstItems.Open strsql, connCurrent
    
    If Not (temprstItems.BOF And temprstItems.EOF) Then
        Set rstItems = temprstItems.Clone
        fillItemList
    End If

    temprstItems.Close
       
       
    Exit Sub
Err_Handler:
    logGeneralError "mdlMain.retItemClasses", CStr(Err.Number), Err.Description
    
    RetryCount = RetryCount + 1
    If RetryCount < 6 Then retItemClasses
End Sub

Public Sub retParts()
    Dim strsql As String, temprstColours As New ADODB.Recordset
    
    On Error GoTo Err_Handler
        
    Select Case ClientIsRemote
        Case False
            strsql = "SELECT tbl1.ItemClass, tbl1.PartDescription, tbl1.PartNumber FROM SQLSERVER.PolyconDW.dbo.tblParts tbl1 where left(itemclass,1)='1'"
        Case True
            strsql = "SELECT tbl1.ItemClass, tbl1.PartDescription, tbl1.PartNumber FROM PolyconDWRemote.dbo.tblParts tbl1"
    End Select
    
    temprstColours.CursorLocation = adUseClient
    temprstColours.CursorType = adOpenStatic
    temprstColours.LockType = adLockReadOnly
    temprstColours.Open strsql, connCurrent
    
    If Not (temprstColours.BOF And temprstColours.EOF) Then
        Set rstColours = temprstColours.Clone
        fillPartList
    End If
    
    temprstColours.Close
          
          
    Exit Sub
Err_Handler:
    logGeneralError "mdlMain.retParts", CStr(Err.Number), Err.Description
    
    RetryCount = RetryCount + 1
    If RetryCount < 6 Then retParts
End Sub

Public Sub retDefectProfile()
    Dim strsql As String, temprstDefectProfile As New ADODB.Recordset, strDefProf As String
    
    On Error GoTo Err_Handler
      
    strDefProf = frmDefectEntry.DefectProfile
    ProfileString = UCase(strDefProf)
    
    strsql = "select * from tblDefectProfiles where ProfileDescription = '" & strDefProf & "'"
    temprstDefectProfile.CursorLocation = adUseClient
    temprstDefectProfile.CursorType = adOpenStatic
    temprstDefectProfile.LockType = adLockReadOnly
    temprstDefectProfile.Open strsql, connCurrent
    
    If Not (temprstDefectProfile.BOF And temprstDefectProfile.EOF) Then
        DefProfile = temprstDefectProfile("DefectProfileID")
    End If
    
    If DefProfile = 0 Then DefProfile = 1
    
    temprstDefectProfile.Close
        
        
    Exit Sub
Err_Handler:
    logGeneralError "mdlMain.retDefectProfile", CStr(Err.Number), Err.Description
    
    RetryCount = RetryCount + 1
    If RetryCount < 6 Then retDefectProfile
End Sub

Public Sub retDefectTypes()
    Dim strsql As String, temprstDefectTypes As New ADODB.Recordset
    
    On Error GoTo Err_Handler
          
    strsql = "select distinct dt.DefectTypeID, dt.DefectType from tblDefectProfileItems dpi " _
            & "inner join tblDefectDescriptions dd on dpi.DefectDescriptionID = dd.DefectDescriptionID " _
            & "inner join tblDefectTypes dt on dd.DefectTypeID = dt.DefectTypeID " _
            & "where dpi.DefectProfileID = " & DefProfile
    temprstDefectTypes.CursorLocation = adUseClient
    temprstDefectTypes.CursorType = adOpenStatic
    temprstDefectTypes.LockType = adLockReadOnly
    temprstDefectTypes.Open strsql, connCurrent
    
    If Not (temprstDefectTypes.BOF And temprstDefectTypes.EOF) Then
        Set rstDefectTypes = temprstDefectTypes.Clone
    End If
    
    temprstDefectTypes.Close
        
        
    Exit Sub
Err_Handler:
    logGeneralError "mdlMain.retDefectTypes", CStr(Err.Number), Err.Description
    
    RetryCount = RetryCount + 1
    If RetryCount < 6 Then retDefectTypes
End Sub

Public Sub retDefects()
    Dim strsql As String, temprstDefectDescriptions As New ADODB.Recordset
    
    On Error GoTo Err_Handler
    
    strsql = "select dt.DefectTypeID as DTypeID, dt.defecttype as DefTypedescrip,dg.defectGroupDescription DefGrpDescrip, dd.DefectDescriptionID DefID,"
    strsql = strsql & "dd.DefectDescription DefDescrip from tbldefectdescriptions dd, tblDefecttypes dt, tblDefectGroup dg where dt.defecttypeid=dd.defecttypeid and "
    strsql = strsql & "dd.include=1 and dg.defectgroupid=dd.defectgroup and defectGroupID =1 and dd.include=1 and defectType='" & ProfileString & "' order by 5"

    temprstDefectDescriptions.CursorLocation = adUseClient
    temprstDefectDescriptions.CursorType = adOpenStatic
    temprstDefectDescriptions.LockType = adLockReadOnly
    temprstDefectDescriptions.Open strsql, connCurrent
    
    If Not (temprstDefectDescriptions.BOF And temprstDefectDescriptions.EOF) Then
        Set rstDefectDescriptions = temprstDefectDescriptions.Clone
    End If
    
    temprstDefectDescriptions.Close
    
    
    Exit Sub
Err_Handler:
    logGeneralError "mdlMain.retDefects", CStr(Err.Number), Err.Description
    
    RetryCount = RetryCount + 1
    If RetryCount < 6 Then retDefects
End Sub

Private Sub fillItemList()
    Dim xcntr As Integer

    On Error GoTo Err_Handler
    
    If ItemLst.Count > 0 Then
        Set ItemLst = Nothing
    End If

    rstItems.MoveFirst
    Do Until rstItems.EOF
        With ItemLst.Add
            .ItemClass = rstItems("ItemClass")
            .ItemClassIDX = xcntr
            .ItemDescription = rstItems("ItemDescription")
            rstItems.MoveNext
            xcntr = xcntr + 1
        End With
    Loop
        
        
    Exit Sub
Err_Handler:
    logGeneralError "mdlMain.General.fillItemList", CStr(Err.Number), Err.Description
End Sub

Private Sub fillPartList()
    Dim xcntr As Integer

    On Error GoTo Err_Handler

    If PartLst.Count > 0 Then
        Set PartLst = Nothing
    End If

    rstColours.MoveFirst
    Do Until rstColours.EOF
        With PartLst.Add
            .ItemClass = RTrim(rstColours("ItemClass"))
            .PartIDX = xcntr
            .ColourDescription = RTrim(rstColours("PartDescription"))
            .PartNumber = RTrim(rstColours("PartNumber"))
            rstColours.MoveNext
            xcntr = xcntr + 1
        End With
    Loop
        
        
    Exit Sub
Err_Handler:
    logGeneralError "mdlMain.General.fillPartList", CStr(Err.Number), Err.Description
End Sub

Public Sub bldConnections()
    Dim connErr As ADODB.Error, connTemp As New ADODB.Connection
    
    On Error GoTo Err_Connect
    
    connPartsID.ConnectionString = connStr2
    
    If ClientIsRemote Then
        connTemp.ConnectionTimeout = connTO
        connTemp.Open connStr
        Set connMain(1) = connTemp
        Set connCurrent = connMain(1)
        connState = 1
    Else
        If IsNetConnectViaLAN = True Then
            connTemp.ConnectionTimeout = connTO
            connTemp.Open connStr
            Set connMain(1) = connTemp
            Set connCurrent = connMain(1)
            connState = 1
        End If
    End If
    
    
    Exit Sub
Err_Connect:
    logGeneralError "mdlMain.General.bldConnections", CStr(Err.Number), Err.Description
    For Each connErr In connTemp.Errors
        If connErr.Number = -2147467259 Then
            connState = 2
            Exit Sub
        End If
    Next connErr
    If MsgBox("There is a problem connecting to a datasource, please contact Systems Administrator", vbOKOnly, "Cannot Connect") = vbOK Then
        End
    End If
End Sub

Public Sub RebuildConnToSQL()
    Dim connErr As ADODB.Error
    
    On Error GoTo conn_err
    
    frmDefectEntry.ctlStatBar.Panels(2) = "Attempting to connect to " _
            & "Data Server... Please Wait."
    Set connMain(1) = Nothing
    connMain(1).ConnectionTimeout = connTO
    connMain(1).Open connStr
    Set connCurrent = connMain(1)
    connState = 1
    RetryCount = 0
    frmDefectEntry.ctlTimerConn.Interval = 60000
    frmDefectEntry.ctlStatBar.Panels(2) = "Connecting to Server... Please Wait."
    
    CheckConnectionStatus
        
        
    Exit Sub
conn_err:
    logGeneralError "mdlMain.General.RebuildConnToSQL", CStr(Err.Number), Err.Description
    For Each connErr In connMain(1).Errors
        If connErr.Number = -2147467259 Then
            frmDefectEntry.ctlStatBar.Panels(2) = "Connection to Server " _
                    & "unavailable... Please Wait."
            connState = 2
            frmDefectEntry.ctlStatBar.Panels(2) = "No Connection to Server"
            frmDefectEntry.ctlTimerConn.Interval = 5000
            Exit Sub
        End If
    Next connErr
    If MsgBox("There is a problem connecting to a datasource, please contact Systems Administrator", vbOKOnly, "Cannot Connect") = vbOK Then
        End
    End If
End Sub

Public Sub CheckConnectionStatus()
    On Error GoTo Err_Handler

    If ClientIsRemote = False Then
        If IsNetConnectViaLAN = False And connState = 2 Then
            frmDefectEntry.ctlStatBar.Panels(1).Picture = LoadResPicture("RED", vbResBitmap)
            frmDefectEntry.ctlStatBar.Panels(2).Text = "No LAN connection."
        End If
        If IsNetConnectViaLAN = True And connState = 2 Then
            frmDefectEntry.ctlStatBar.Panels(1).Picture = LoadResPicture("YELLOW", vbResBitmap)
            frmDefectEntry.ctlStatBar.Panels(2).Text = "Connected to LAN.  No connection to datastore."
        End If
        If IsNetConnectViaLAN = True And connState = 1 Then
            frmDefectEntry.ctlStatBar.Panels(1).Picture = LoadResPicture("GREEN", vbResBitmap)
            frmDefectEntry.ctlStatBar.Panels(2).Text = "Connected to LAN, using connected datastore."
        End If
        If IsNetConnectViaLAN = True And (connState = 2 Or connState = 0) And frmDefectEntry.ctlPartSN = "" Then
            RebuildConnToSQL
            retItemClasses
            retParts
            retDefectTypes
            retDefects
            bldDefectTypes
            bldDefects
            stInspectionPoints
            FillctllbDisposition
            ctlDefectTypeRefresh
            mdlFillDefButtons.FillDefectButtons

        End If
        If IsNetConnectViaLAN = False And connState = 1 And frmDefectEntry.ctlPartSN = "" Then
            frmDefectEntry.ctlStatBar.Panels(1).Picture = LoadResPicture("RED", vbResBitmap)
            frmDefectEntry.ctlStatBar.Panels(2).Text = "No LAN connection."
            RebuildConnToSQL
        End If
    Else
        If connState = 1 Then
            frmDefectEntry.ctlStatBar.Panels(1).Picture = LoadResPicture("GREEN", vbResBitmap)
            frmDefectEntry.ctlStatBar.Panels(2).Text = "Disconnected from Lan, using local datastore."
        End If
    End If
    
    
    Exit Sub
Err_Handler:
    logGeneralError "mdlMain.CheckConnectionStatus", CStr(Err.Number), Err.Description
End Sub

Public Sub RefreshPartsAndDefects()
    On Error GoTo Err_Handler
    
    If IsNetConnectViaLAN = True And connState = 1 And RefreshOK = True Then
    
        frmDefectEntry.ctlScan.Enabled = False
    
        frmDefectEntry.ctlStatBar.Panels(1).Picture = LoadResPicture("YELLOW", vbResBitmap)
        frmDefectEntry.ctlStatBar.Panels(2).Text = "Updating Part and Defect Information"
        
        
        retItemClasses

        retParts

        retDefectTypes
        bldDefectTypes

        ctlDefectTypeRefresh

        retDefects
        bldDefects
        
        mdlFillDefButtons.FillDefectButtons

        frmDefectEntry.ctlStatBar.Panels(1).Picture = LoadResPicture("GREEN", vbResBitmap)
        frmDefectEntry.ctlStatBar.Panels(2).Text = "Connected to LAN, using connected datastore."
        
        RefreshOK = False
        
        RetryCount = 0
        
        frmDefectEntry.ctlScan.Enabled = True
    
    End If

    Exit Sub
    
Err_Handler:
    logGeneralError "mdlMain.RefreshPartsAndDefects", CStr(Err.Number), Err.Description
    
End Sub

Public Sub bldDefectTypes()

    On Error GoTo Err_Handler
    
    Set oDefectTypes = Nothing

    rstDefectTypes.MoveFirst
    Do Until rstDefectTypes.EOF
        With oDefectTypes.Add
            .DefectTypeID = rstDefectTypes("DefectTypeID")
            .DefectTypeDescription = rstDefectTypes("DefectType")
            rstDefectTypes.MoveNext
        End With
    Loop
    
    
    Exit Sub
Err_Handler:
    logGeneralError "mdlMain.bldDefectTypes", CStr(Err.Number), Err.Description
End Sub

Public Sub ctlDefectTypeRefresh()
    Dim Item As DefectType, idx As Integer
    
    On Error GoTo Err_Handler
    idx = 0
    frmDefectEntry.ctlDefectType.Clear
    For Each Item In oDefectTypes
        With frmDefectEntry.ctlDefectType
            .AddItem Item.DefectTypeDescription, idx
            .ItemData(idx) = Item.DefectTypeID
        End With
        idx = idx + 1
    Next Item
        
        
    Exit Sub
Err_Handler:
    logGeneralError "mdlMain.ctlDefectTypeRefresh", CStr(Err.Number), Err.Description
End Sub

Public Sub bldDefects()

    On Error GoTo Err_Handler
    
    Set oDefectDescs = Nothing
    
    rstDefectDescriptions.MoveFirst
    Do Until rstDefectDescriptions.EOF
        With oDefectDescs.Add
            .DefectTypeID = rstDefectDescriptions("DTypeID")
            .DefectDescriptionID = rstDefectDescriptions("DefID")
            .DefectDescription = rstDefectDescriptions("DefDescrip")
            .DefectInfo = rstDefectDescriptions("DefGrpDescrip")
            
            rstDefectDescriptions.MoveNext
        End With
    Loop
    
    
    Exit Sub
Err_Handler:
    logGeneralError "mdlMain.bldDefects", CStr(Err.Number), Err.Description
End Sub

Public Sub GetMDBtbls()
    Dim strsqla(4), strsqlb(3), xcnt As Integer
    
    On Error GoTo Err_Handler
    
    strsqla(1) = "SELECT * FROM tblDefectMaster"
    strsqla(2) = "SELECT * FROM tblDefectSubItems"
    strsqla(3) = "SELECT * FROM tblInspectionTrans"
    strsqla(4) = "DELETE tblDefectMaster FROM tblDefectMaster"
    For xcnt = 1 To 4
        RunMDBQuery strsqla(xcnt), xcnt
    Next xcnt
    
    
    Exit Sub
Err_Handler:
    logGeneralError "mdlMain.General.GetMDBtbls", CStr(Err.Number), Err.Description
End Sub

Private Sub RunMDBQuery(ByVal strsqla As String, ByVal iter As Integer)
    Dim rst(2) As New ADODB.Recordset, strsqlc As String
    
    On Error GoTo Err_Handler
    
    rst(2).Open strsqla, connMain(2)
    If iter = 4 Then Exit Sub
    If (rst(2).EOF And rst(2).BOF) = False Then
        Do Until rst(2).EOF
            Select Case iter
                Case 1
                    strsqlc = "INSERT INTO tblDefectMasterBuffer (EventID, EventDateTime, " _
                            & "PartSN, Comment) VALUES ('" _
                            & rst(2)("EventID") & "', '" & rst(2)("EventDateTime") _
                            & "', '" & rst(2)("PartSN") & "', '" & rst(2)("Comment") & "')"
                Case 2
                    strsqlc = "INSERT INTO tblDefectSubItemsBuffer (EventID, DefectID, Xpos, " _
                            & "Ypos, DefectDescriptionID, DefectDateTime, IPID) " _
                            & "VALUES ('" & rst(2)("EventID") & "', '" & rst(2)("DefectID") _
                            & "', " & rst(2)("Xpos") & ", " & rst(2)("Ypos") _
                            & ", " & rst(2)("DefectDescriptionID") & ", '" _
                            & rst(2)("DefectDateTime") & "', " & rst(2)("IPID") & ")"
                Case 3
                    strsqlc = "INSERT INTO tblInspectionTransBuffer (EventID, PartSN, FromIPID, " _
                            & "ToIPID, DispositionID, TransactionDateTime) VALUES ('" & rst(2)("EventID") & "', '" _
                            & rst(2)("PartSN") & "', " & rst(2)("FromIPID") & ", " & rst(2)("ToIPID") _
                            & ", " & rst(2)("DispositionID") & ", '" & rst(2)("TransactionDateTime") _
                            & "')"
            End Select
            rst(1).Open strsqlc, connMain(1)
            rst(2).MoveNext
        Loop
    End If
    rst(1).Close
    rst(2).Close
    
    
    Exit Sub
Err_Handler:
    logGeneralError "mdlMain.General.RunMDBQuery", CStr(Err.Number), Err.Description
End Sub

Public Sub stInspectionPoints()
    Dim rst As New ADODB.Recordset, strsql As String, lstind As Long
    
    On Error GoTo Err_Connect
    
    If Not frmDefectEntry.ctlLocked = 1 Then
        frmDefectEntry.ctlInspectionPoint.Clear
        strsql = "SELECT * FROM tblInspectionPoints ORDER BY IPID"
        rst.Open strsql, connCurrent
        Do Until rst.EOF
            With frmDefectEntry.ctlInspectionPoint
                If Not rst("IPID") = 0 Then
                    .AddItem rst("InspectionPoint"), lstind     'LIST INDEX
                    .ItemData(lstind) = rst("IPID")
                    lstind = lstind + 1
                End If
            End With
            rst.MoveNext
        Loop
    End If
    
    rst.Close
    
    
    Exit Sub
Err_Connect:
    logGeneralError "frmDefectEntry.General.stInspectionPoints", CStr(Err.Number), Err.Description
    If connState = 2 Then Resume
End Sub

Public Sub FillctllbDisposition()
    Dim rst As New ADODB.Recordset, strsql As String

    On Error GoTo Err_Handler

    strsql = "select DispositionID, DispositionName from tblDispositions " _
            & "where MouldInclude=1 order by DispositionName"

    rst.Open strsql, connCurrent

    Do Until rst.EOF
        With frmDefectEntry.ctllbDisposition
            .AddItem rst("DispositionName")
            .ItemData(frmDefectEntry.ctllbDisposition.NewIndex) = rst("DispositionID")
        End With
        rst.MoveNext
    Loop

    rst.Close


    Exit Sub
Err_Handler:
    logGeneralError "frmDefectEntry.General.FillctllbDisposition", CStr(Err.Number), Err.Description
End Sub

Private Sub setLocalResources()
    Dim rst(2) As New ADODB.Recordset, strsql As String, itmDesc As String
    
    On Error GoTo Err_Handler
    
    strsql = "SELECT * FROM vwColours"
    rst(1).Open strsql, connMain(1)
    strsql = "DELETE tblColours FROM tblColours"
    rst(2).Open strsql, connMain(2)
    Do Until rst(1).EOF
        strsql = "INSERT INTO tblColours (ItemClass, PartNumber, " _
                & "ColourDescription) VALUES ('" & rst(1)("ItemClass") _
                & "', '" & rst(1)("PartNumber") & "', '" _
                & rst(1)("ColourDescription") & "')"
        rst(2).Open strsql, connMain(2)
        rst(1).MoveNext
    Loop
    rst(1).Close
    strsql = "SELECT * FROM vwItemClasses"
    rst(1).Open strsql, connMain(1)
    strsql = "DELETE tblItemClasses FROM tblItemClasses"
    rst(2).Open strsql, connMain(2)
    Do Until rst(1).EOF
        itmDesc = Replace(rst(1)("ItemDescription"), "'", "")
        strsql = "INSERT INTO tblItemClasses (ItemClass, ItemDescription) " _
                & "VALUES ('" & rst(1)("ItemClass") & "', '" _
                & itmDesc & "')"
        rst(2).Open strsql, connMain(2)
        rst(1).MoveNext
    Loop
    rst(1).Close
    strsql = "SELECT * FROM tblDispositions"
    rst(1).Open strsql, connMain(1)
    strsql = "DELETE tblDispositions FROM tblDispositions"
    rst(2).Open strsql, connMain(2)
    Do Until rst(1).EOF
        strsql = "INSERT INTO tblDispositions (DispositionID, DispositionName) " _
                & "VALUES (" & rst(1)("DispositionID") & ", '" & rst(1)("DispositionName") _
                & "')"
        rst(2).Open strsql, connMain(2)
        rst(1).MoveNext
    Loop
    rst(1).Close
    strsql = "SELECT * FROM tblDefectTypes"
    rst(1).Open strsql, connMain(1)
    strsql = "DELETE tblDefectTypes FROM tblDefectTypes"
    rst(2).Open strsql, connMain(2)
    Do Until rst(1).EOF
        strsql = "INSERT INTO tblDefectTypes (DefectTypeID, DefectType) " _
                & "VALUES (" & rst(1)("DefectTypeID") & ", '" & rst(1)("DefectType") & "')"
        rst(2).Open strsql, connMain(2)
        rst(1).MoveNext
    Loop
    rst(1).Close
    strsql = "SELECT * FROM tblDefectDescriptions"
    rst(1).Open strsql, connMain(1)
    strsql = "DELETE tblDefectDescriptions FROM tblDefectDescriptions"
    rst(2).Open strsql, connMain(2)
    Do Until rst(1).EOF
        strsql = "INSERT INTO tblDefectDescriptions (DefectTypeID, DefectDescriptionID, " _
                & "DefectDescription, DefectInfo) VALUES (" & rst(1)("DefectTypeID") & ", " _
                & rst(1)("DefectDescriptionID") & ", '" & rst(1)("DefectDescription") _
                & "', '" & rst(1)("DefectInfo") & "')"
        rst(2).Open strsql, connMain(2)
        rst(1).MoveNext
    Loop
    
    rst(2).Close
    
    
    Exit Sub
Err_Handler:
    logGeneralError "mdlMain.General.setLocalResources", CStr(Err.Number), Err.Description
End Sub

'**************************************************************************************************
Public Sub getPrint0QtyComponents(pressID As Integer)
    Dim rs As New ADODB.Recordset
    Dim cmd As New ADODB.Command
    Dim params As New ADODB.Parameter
    Dim conn As New ADODB.Connection
    Dim cnt As Integer
    Dim connStr As String
    Dim sqlStr As String

    On Error GoTo Err_Handler

    connStr = frmDefectEntry.DDCMouldConnStr
    conn.ConnectionString = connStr
    conn.Open
      
    cmd.CommandText = "getPrint0QtyComponents"
    cmd.CommandType = adCmdStoredProc
    cmd.ActiveConnection = conn
    cmd.Parameters.Append cmd.CreateParameter("pressID", adInteger, adParamInput, , pressID)
    
    'WITHOUT THE FOLLOWING 3 LINES, RECORDCOUNT = -1
    rs.CursorType = adOpenStatic
    rs.CursorLocation = adUseClient
    rs.LockType = adLockOptimistic
    rs.Open cmd
    cnt = rs.RecordCount
    If cnt < 1 Then
        MsgBox ("No Components Available For The Current Tool.")
        conn.Close
        Set cmd = Nothing
        Set rs = Nothing
        Set conn = Nothing
    End If
    
    Do Until rs.EOF
        With frmDefectEntry.lsComponents
            .AddItem rs("Components")
            '.List(frmDefectEntry.lsComponents.NewIndex) = rs("Components")
        End With
        rs.MoveNext
    Loop

    'MsgBox frmDefectEntry.lsComponents.ListIndex   -1
    frmDefectEntry.lsComponents.ListIndex = 0
    conn.Close
    Set cmd = Nothing
    Set rs = Nothing
    Set conn = Nothing

    'Set cmd.ActiveConnection = Nothing


    Exit Sub
Err_Handler:
    logGeneralError "frmDefectEntry.General.getPrint0QtyComponents", CStr(Err.Number), Err.Description
End Sub

'**************************************************************************************************
Public Sub transactComponents(pressName As String, pressID As Integer, ItemClass As String, PartNumber As String, qty As Integer)
    Dim cmd As New ADODB.Command, params As New ADODB.Parameter
    Dim conn As New ADODB.Connection, connStr As String

    On Error GoTo Err_Handler
    
    connStr = frmDefectEntry.DDCMouldConnStr
    conn.ConnectionString = connStr
    conn.Open
    

    cmd.ActiveConnection = conn
    cmd.CommandType = adCmdStoredProc
    cmd.CommandText = "sendComponents2INVTRANS"
    Set params = cmd.CreateParameter("@PressName", adVarChar, adParamInput, 25, pressName)
    cmd.Parameters.Append params
    Set params = cmd.CreateParameter("@PressID", adInteger, adParamInput, , pressID)
    cmd.Parameters.Append params
    Set params = cmd.CreateParameter("@ItemClass", adVarChar, adParamInput, 11, ItemClass)
    cmd.Parameters.Append params
    Set params = cmd.CreateParameter("@PartNumber", adVarChar, adParamInput, 25, PartNumber)
    cmd.Parameters.Append params
    Set params = cmd.CreateParameter("@PartSN", adInteger, adParamInput, , qty)
    cmd.Parameters.Append params
    
    cmd.Execute
    
    conn.Close
    Set cmd = Nothing

    
    Exit Sub
Err_Handler:
    logGeneralError "frmDefectEntry.General.transactComponents", CStr(Err.Number), Err.Description
End Sub
'**************************************************************************************************

