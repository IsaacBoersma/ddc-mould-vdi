VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "DefectParent"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Attribute VB_Ext_KEY = "Member0" ,"ItemClass"
Private mvarEventID As String 'local copy
Private mvarEventDateTime As Variant 'local copy
Private mvarLotNum As String ' local copy
Private mvarPartSN As String 'local copy
Private mvarItemClass As String 'local copy
Private mvarPartNumber As String 'local copy
Private mvarComment As String 'local copy
Private mvarLabelCount As Integer 'local copy
Private mvarPressID As Integer 'local copy
Private mvarFlag As Integer 'local copy
Private mvarPressDesc As String 'local copy

Public Property Let LabelCount(ByVal vData As Integer)
    mvarLabelCount = vData
End Property

Public Property Get LabelCount() As Integer
    LabelCount = mvarLabelCount
End Property

Public Property Let LotNum(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.LotNum = 5
    mvarLotNum = vData
End Property

Public Property Get LotNum() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.Lotnum
    LotNum = mvarLotNum
End Property

Public Property Let Comment(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.Comment = 5
    mvarComment = vData
End Property

Public Property Get Comment() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.Comment
    Comment = mvarComment
End Property

Public Property Let PartNumber(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.PartNumber = 5
    mvarPartNumber = vData
End Property

Public Property Get PartNumber() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.PartNumber
    PartNumber = mvarPartNumber
End Property

Public Property Let ItemClass(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.ItemClass = 5
    mvarItemClass = vData
End Property

Public Property Get ItemClass() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.ItemClass
    ItemClass = mvarItemClass
End Property

Public Property Let PartSN(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.PartSN = 5
    mvarPartSN = vData
End Property

Public Property Get PartSN() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.PartSN
    PartSN = mvarPartSN
End Property

Public Property Let EventDateTime(ByVal vData As Variant)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.EventDateTime = 5
    mvarEventDateTime = vData
End Property

Public Property Set EventDateTime(ByVal vData As Variant)
'used when assigning an Object to the property, on the left side of a Set statement.
'Syntax: Set x.EventDateTime = Form1
    Set mvarEventDateTime = vData
End Property

Public Property Get EventDateTime() As Variant
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.EventDateTime
    If IsObject(mvarEventDateTime) Then
        Set EventDateTime = mvarEventDateTime
    Else
        EventDateTime = mvarEventDateTime
    End If
End Property

Public Property Let EventID(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.EventID = 5
    mvarEventID = vData
End Property

Public Property Get EventID() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.EventID
    EventID = mvarEventID
End Property

Public Property Let pressID(ByVal vData As Integer)
    mvarPressID = vData
End Property

Public Property Get pressID() As Integer
    pressID = mvarPressID
End Property

Public Property Let pressDesc(ByVal vData As String)
    mvarPressDesc = vData
End Property

Public Property Get pressDesc() As String
    pressDesc = mvarPressDesc
End Property

Public Property Let Flag(ByVal vData As Integer)
    mvarFlag = vData
End Property

Public Property Get Flag() As Integer
    Flag = mvarFlag
End Property


