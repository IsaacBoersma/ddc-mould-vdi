VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "Part"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Member0" ,"ItemClass"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Private mvarItemClass As String 'local copy
Private mvarPartNumber As String 'local copy
Private mvarColourDescription As String 'local copy
Private mvarPartIDX As Integer 'local copy

Public Property Let PartIDX(ByVal vData As Integer)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.PartIDX = 5
    mvarPartIDX = vData
End Property

Public Property Get PartIDX() As Integer
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.PartIDX
    PartIDX = mvarPartIDX
End Property

Public Property Let ColourDescription(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.ColourDescription = 5
    mvarColourDescription = vData
End Property

Public Property Get ColourDescription() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.ColourDescription
    ColourDescription = mvarColourDescription
End Property

Public Property Let PartNumber(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.PartNumber = 5
    mvarPartNumber = vData
End Property

Public Property Get PartNumber() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.PartNumber
    PartNumber = mvarPartNumber
End Property

Public Property Let ItemClass(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.ItemClass = 5
    mvarItemClass = vData
End Property

Public Property Get ItemClass() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.ItemClass
    ItemClass = mvarItemClass
End Property



