VERSION 5.00
Begin VB.Form frmDataMask 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "DataMask Values"
   ClientHeight    =   4005
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   3915
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4005
   ScaleWidth      =   3915
   StartUpPosition =   3  'Windows Default
   Begin VB.CheckBox Check1 
      Caption         =   "Check1"
      Height          =   255
      Index           =   256
      Left            =   2160
      TabIndex        =   18
      Top             =   3120
      Width           =   255
   End
   Begin VB.CommandButton cmdOk 
      Caption         =   "Ok"
      Height          =   375
      Left            =   2640
      TabIndex        =   17
      Top             =   1920
      Width           =   1095
   End
   Begin VB.CheckBox Check1 
      Caption         =   "Check1"
      Height          =   255
      Index           =   128
      Left            =   2160
      TabIndex        =   16
      Top             =   2760
      Width           =   255
   End
   Begin VB.CheckBox Check1 
      Caption         =   "Check1"
      Height          =   255
      Index           =   64
      Left            =   2160
      TabIndex        =   15
      Top             =   2400
      Width           =   255
   End
   Begin VB.CheckBox Check1 
      Caption         =   "Check1"
      Height          =   255
      Index           =   32
      Left            =   2160
      TabIndex        =   14
      Top             =   2040
      Width           =   255
   End
   Begin VB.CheckBox Check1 
      Caption         =   "Check1"
      Height          =   255
      Index           =   16
      Left            =   2160
      TabIndex        =   13
      Top             =   1680
      Width           =   255
   End
   Begin VB.CheckBox Check1 
      Caption         =   "Check1"
      Height          =   255
      Index           =   8
      Left            =   2160
      TabIndex        =   12
      Top             =   1320
      Width           =   255
   End
   Begin VB.CheckBox Check1 
      Caption         =   "Check1"
      Height          =   255
      Index           =   4
      Left            =   2160
      TabIndex        =   11
      Top             =   960
      Width           =   255
   End
   Begin VB.CheckBox Check1 
      Caption         =   "Check1"
      Height          =   255
      Index           =   2
      Left            =   2160
      TabIndex        =   10
      Top             =   600
      Width           =   255
   End
   Begin VB.CheckBox Check1 
      Caption         =   "Check1"
      Height          =   255
      Index           =   1
      Left            =   2160
      TabIndex        =   9
      Top             =   240
      Width           =   255
   End
   Begin VB.TextBox ctlMaskVal 
      Height          =   285
      Left            =   2640
      TabIndex        =   8
      Top             =   1440
      Width           =   1095
   End
   Begin VB.Label Label1 
      Caption         =   "256 = Part Disposition"
      Height          =   255
      Index           =   8
      Left            =   0
      TabIndex        =   19
      Top             =   3120
      Width           =   1995
   End
   Begin VB.Label Label1 
      Caption         =   "128 = Part Serial Number"
      Height          =   255
      Index           =   7
      Left            =   0
      TabIndex        =   7
      Top             =   2760
      Width           =   1995
   End
   Begin VB.Label Label1 
      Caption         =   "64 = Part Number"
      Height          =   255
      Index           =   6
      Left            =   0
      TabIndex        =   6
      Top             =   2400
      Width           =   1995
   End
   Begin VB.Label Label1 
      Caption         =   "32 = Part Colour"
      Height          =   255
      Index           =   5
      Left            =   0
      TabIndex        =   5
      Top             =   2040
      Width           =   1995
   End
   Begin VB.Label Label1 
      Caption         =   "16 = Lot Number"
      Height          =   255
      Index           =   4
      Left            =   0
      TabIndex        =   4
      Top             =   1680
      Width           =   1995
   End
   Begin VB.Label Label1 
      Caption         =   "8 = Item Class"
      Height          =   255
      Index           =   3
      Left            =   0
      TabIndex        =   3
      Top             =   1320
      Width           =   1995
   End
   Begin VB.Label Label1 
      Caption         =   "4 = Inspection Point"
      Height          =   255
      Index           =   2
      Left            =   0
      TabIndex        =   2
      Top             =   960
      Width           =   1995
   End
   Begin VB.Label Label1 
      Caption         =   "2 = Event ID"
      Height          =   255
      Index           =   1
      Left            =   0
      TabIndex        =   1
      Top             =   600
      Width           =   1995
   End
   Begin VB.Label Label1 
      Caption         =   "1 = Event Date/Time"
      Height          =   255
      Index           =   0
      Left            =   0
      TabIndex        =   0
      Top             =   240
      Width           =   1995
   End
End
Attribute VB_Name = "frmDataMask"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private intMask As Integer

Private Sub Check1_Click(Index As Integer)

    On Error GoTo err_Handler

    Select Case Index
        Case 1
            intMask = intMask + IIf(Check1(Index) = 1, 1, -1)
        Case 2
            intMask = intMask + IIf(Check1(Index) = 1, 2, -2)
        Case 4
            intMask = intMask + IIf(Check1(Index) = 1, 4, -4)
        Case 8
            intMask = intMask + IIf(Check1(Index) = 1, 8, -8)
        Case 16
            intMask = intMask + IIf(Check1(Index) = 1, 16, -16)
        Case 32
            intMask = intMask + IIf(Check1(Index) = 1, 32, -32)
        Case 64
            intMask = intMask + IIf(Check1(Index) = 1, 64, -64)
        Case 128
            intMask = intMask + IIf(Check1(Index) = 1, 128, -128)
        Case 256
            intMask = intMask + IIf(Check1(Index) = 1, 256, -256)
    End Select
    ctlMaskVal = intMask
    
    Exit Sub
    
err_Handler:
    
    logGeneralError "frmDataMask.Check1_Click", CStr(Err.Number), Err.Description

End Sub

Private Sub cmdOk_Click()

    On Error GoTo err_Handler

    frmConfig.ctlDataMask = ctlMaskVal
    Unload frmDataMask
    
    Exit Sub
    
err_Handler:
    
    logGeneralError "frmDataMask.cmdOk_Click", CStr(Err.Number), Err.Description

End Sub

Private Sub Form_Load()
    Dim cntr As Integer, inttmp As Integer
    
    On Error GoTo err_Handler
    
    inttmp = frmConfig.ctlDataMask
    intMask = 0
    For cntr = 7 To 0 Step -1
        If (inttmp - (2 ^ cntr)) >= 0 Then
            Check1(2 ^ cntr) = 1
            inttmp = inttmp - (2 ^ cntr)
        End If
    Next cntr
    
    Exit Sub
    
err_Handler:
    
    logGeneralError "frmDataMask.Form_Load", CStr(Err.Number), Err.Description

End Sub
